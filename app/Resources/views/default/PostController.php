<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Post;
use AppBundle\Form\PostType;


class PostController extends Controller
{
    
	/**
	*@Route("post/ContactUs") 
	*/
	public function ContactUs()
	{
		return $this->render('AppBundle:Post:ContactUs.html.twig');
	}	

	/**
	*@Route("post/About") 
	*/
	public function About()
	{
		return $this->render('AppBundle:Post:About.html.twig');
	}

    /**
     * @Route("post/list", name="post_list")
     */
    public function listAction()
    {
    	
    	$em= $this->getDoctrine()->getManager();
    	$posts = $em->getRepository('AppBundle:Post')->findAll();
        return $this->render('AppBundle:Post:list.html.twig', ['Posts' => $posts]);
    }

    /**
    * @Route("post/add", name="post_add")
    */
	public function addAction(Request $request)
	{
		$post = new Post();
		$form = $this->createForm(PostType::class, $post);

		$form->HandleRequest($request);
		if($form->isSubmitted() && $form->isValid())
		{
			$em = $this->getDoctrine()->getManager();
			$em = persist($post);
			$em -> flush();
			return $this->RedirectToRoute("post_list");
		}

		return $this->render('AppBundle:Post:add.html.twig', ['form'=>$form->createView()]);

	}


    /**
    * @Route("post/{id}/update", name="post_update")
    */
	public function updateAction(Post $post, Request $request)
	{
		
		$form = $this->createForm(PostType::class, $post);

		$form->HandleRequest($request);
		if($form->isSubmitted() && $form->isValid())
		{
			$em = $this->getDoctrine()->getManager();
			$em->flush();
			return $this->RedirectToRoute("post_list");
		}

		return $this->render('AppBundle:Post:update.html.twig', ['form'=>$form->createView()]);

	}


    /**
    * @Route("post/{id}/remove", name="post_remove")
    */
	public function removeAction(Post $post, Request $request)
	{
		

		
			$em = $this->getDoctrine()->getManager();
			$em->remove($post);
			$em->flush();
			return $this->RedirectToRoute("post_list");

	}


}
