-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 21, 2017 at 09:58 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.24-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bestperience`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_users`
--

CREATE TABLE `app_users` (
  `id` int(11) NOT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `user_role` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8_unicode_ci,
  `birthdate` date DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `app_users`
--

INSERT INTO `app_users` (`id`, `username`, `password`, `email`, `is_active`, `user_role`, `img`, `address`, `birthdate`, `description`, `name`) VALUES
(1, 'admin', '$2y$13$2AR5nXIB7UHLVzAD7fMNieC9dqJ7wzyWNYv34.Cc95DRITWEzLM.2', 'admin@example.com', 1, 'ROLE_ADMIN', '52003047d54fcd9ea3543d5134696f9e.jpeg', '123123213', '2014-03-16', '123123123', 'Ron'),
(2, 'ron', '$2y$13$/zIl6yvSvrDJaBat3mXZ8OhCmTJ3gdm4dyQ5kJ/W5pi3dMvDh4NP6', 'cruzgerone@gmail.com', 1, 'ROLE_USER', 'aad42f0f074833c12e9d577353826a2f.jpeg', NULL, NULL, NULL, '1'),
(4, 'karl', '$2y$13$mxgAJTJ3flCuAaAS2Z9YA.tR2LeLWCb0/BttOAz5ZOBvgyIIU0REW', 'cruzgerone1@gmail.com', 1, 'ROLE_USER', '618b4ceb3fcf8b2d99c9bfeea94a99f0.jpeg', NULL, NULL, NULL, '1'),
(6, 'karl1', '$2y$13$Jn0M1ws14TlQ4b7/vvJxmONCvMOfBv8W9X38nrc0NY30r8ysG0rgK', 'karlkarl@gmail.com', 1, 'ROLE_USER', 'a3c3985c2f1ee2b47ad5aafdd69b0771.jpeg', NULL, NULL, NULL, '1'),
(7, '1', '$2y$13$hxMXwT9IO/8QgK5bwF/.HeUSNp5gyl1MkpZSHTCEPG8zK1JtRCiB2', '1@gmail.com', 1, 'ROLE_USER', 'e2ffcaae139891d9bb47cf53050de442.jpeg', 'asd', '2016-10-17', 'asdasd', '1'),
(8, '2', '$2y$13$3kPEmxPgz/l5zOvhoeceY.zal52k/r1ZEoQ0yPYvDrOIe6AwtuJyC', '2@gmail.com', 1, 'ROLE_USER', 'ff6780b884dda1c726d580979bab88ff.jpeg', NULL, NULL, NULL, '1'),
(9, '3', '$2y$13$wYzfER00C2bcZluz0Wd.C.OYngo0P1wM8.6gJNfP1Z23Aoz0ADSya', '3@gmail.com', 1, 'ROLE_USER', '4f6df28dcf8b7182734e568a809b5dfc.jpeg', NULL, NULL, NULL, '1'),
(10, '4', '$2y$13$M4cp4AEaSvxd6MFiWEPVkuikVjPFMZzURtduPNO2LRiLZ7hN6D1i2', '4@gmail.com', 1, 'ROLE_USER', '701da84c330bfcf4769e181ffc41bca0.jpeg', NULL, NULL, NULL, '1'),
(11, '5', '$2y$13$JwbNARJAAnWJ.h/9RbYQC.z9gS7y70No8fy6D/uxl4G3HM/4QNTHm', '5@gmail.com', 1, 'ROLE_USER', '6b5fcd61583de4f48613199964c1dc5e.jpeg', NULL, NULL, NULL, '1'),
(12, '6', '$2y$13$qTsp49mAVLBRCrWlFnfcye3PO/agj9b2o4Mlgoz5ysn505k2lG1om', '6@gmail.com', 1, 'ROLE_USER', 'e05ea5e50579d1e3ae1f18cd60ad158e.jpeg', NULL, NULL, NULL, '1'),
(13, '7', '$2y$13$QoLFKpHuQeb.9v0NUIU9s.GrD.TrMBglXUuua8norTLbCYx2imRe6', '7@gmail.com', 1, 'ROLE_USER', '6a8e6bad2c394cd4ab7dd3db94585697.jpeg', 'asd', '2013-03-03', 'asd', 'as'),
(14, '8', '$2y$13$IDFNm6Xh5XOuPFbuhcucgOSju8nqAOklBfBmSJ7qH/Xl4CsTOs67C', '8@gmail.com', 1, 'ROLE_USER', '02858d416648f90848169422433ebbac.jpeg', 'asd', '2014-02-05', 'asd', 'asd'),
(15, '9', '$2y$13$ijdAAOUI6NHFa3LOvsT2/OpjfYiL2mzMkGZ59GVaIzmzDcZdgRHTy', '9@gmail.com', 1, 'ROLE_USER', 'a3b04e5353aba8788b95bb89d8d1e461.jpeg', '9', '2014-02-15', '9', '9'),
(17, '10', '$2y$13$6wnmciopHuLU478PdRlI/ucb8KZdLcpbRbxNGI/VOZ750ZH7h464a', '10@gmail.com', 1, 'ROLE_USER', '90a22f5049efdd048854a77f1ea07d86.jpeg', NULL, NULL, NULL, '1'),
(18, 'asd', '$2y$13$bCUzTjaXBn0xc0Asgon5JeXt6PIEPdsV6cb6Dkx4Bzq4GlbCKm4v.', 'asd@gmail.com', 1, 'ROLE_USER', '6a2c4941b3d25cf43350f5dd1f21e2aa.jpeg', 'asd', NULL, NULL, 'asd'),
(19, 'aa', '$2y$13$o1uK9BKLpcFUhcNBos9v1.RVL/k3YjiqnESyVMM2/ZtRgXpSXeVgG', 'aa@gmail.com', 1, 'ROLE_USER', '90dd310cf5a7733b383f46bfd29c3dc7.jpeg', 'asfd', NULL, 'asdf', 'asdf');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` longtext COLLATE utf8_unicode_ci NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `post_id`, `user_id`, `comment`, `name`) VALUES
(10, 5, 1, 'asf', '1'),
(11, 5, 1, 'rew', '1'),
(12, 5, 1, 'Barts', '1'),
(23, 9, 1, 'HI :)', 'Ron'),
(24, 11, 1, 'hi :)', 'Ron'),
(25, 11, 1, 'hihi :D', 'Ron'),
(26, 15, 1, 'asdasdHello', 'Ron'),
(27, 6, 7, 'asdXD XD', '1'),
(28, 6, 7, 'asd', '1'),
(29, 2, 1, 'yea boy', 'Ron'),
(30, 2, 1, 'asasd', 'Ron');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `Title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `date`, `Title`, `content`, `user_id`, `img`) VALUES
(2, '2017-10-21 14:59:38', 'sdfasdf', 'asfaf', 7, 'KIM_59eaf05a98f9b.jpeg'),
(3, '2017-10-21 18:59:55', 'wow', 'wewoooooooooooooooooooo', 1, 'KIM_59eb28ab1c471.jpeg'),
(4, '2017-10-21 19:52:11', 'asdas', 'asdas', 1, 'KIM_59eb34ebebe26.jpeg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_users`
--
ALTER TABLE `app_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_C2502824F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_C2502824E7927C74` (`email`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9474526C4B89032C` (`post_id`),
  ADD KEY `IDX_9474526CA76ED395` (`user_id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5A8A6C8DA76ED395` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_users`
--
ALTER TABLE `app_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526C4B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `FK_9474526CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `app_users` (`id`);

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `FK_5A8A6C8DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `app_users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
