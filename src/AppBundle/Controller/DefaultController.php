<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/Home.html.twig');
    }

    /**
	*@Route("ContactUs", name="contactus") 
	*/
	public function ContactUs()
	{
		return $this->render('default/ContactUs.html.twig');
	}	

	/**
	*@Route("About", name="about") 
	*/
	public function About()
	{
		return $this->render('default/About.html.twig');
	}
}
