<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use AppBundle\Form\PostType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\File;
use AppBundle\Entity\Comment;
use AppBundle\Form\CommentType;


class PostController extends Controller
{
    /**
	*@Route("mail") 
	*/
	public function sendAction(\Swift_Mailer $mailer)
    {
    	$message = (new \Swift_Message( 'Welcome to BESTPERIENCE'))
    	->setFrom('cruzgerone@gmail.com', 'BESTPERIENCE')
    	->setTo( 'pangilinanjohnlex@gmail.com')
    	->setBody('This is just a test for BESTPERIENCE');

	     $mailer->send($message);
		 return new Response(
			'Hello'
        );
    }


	

    /**
     * @Route("home", name="user_post_list")
     */
    public function listAction()
    {
    	$em= $this->getDoctrine()->getManager();
    	$posts = $em->getRepository('AppBundle:Post')->findAll();
        return $this->render('AppBundle:Post:listPost.html.twig', ['Posts' => $posts]);
    }


    /**
    * @Route("user/post/add", name="user_post_add")
    */
	public function addAction(Request $request)
	{
		$user = new User();
		$user = $this->getUser();

		$post = new Post();
		$form = $this->createForm(PostType::class, $post);
		$post->setUser($user);

		$form->HandleRequest($request);
		if($form->isSubmitted() && $form->isValid())
		{
			$file = $post->getImg();
			$filename =  'KIM_'.uniqid().".".$file->guessExtension();
			$file->move($this->getParameter("profile_img_dir"), $filename);
			$post -> setImg($filename);

			$em = $this->getDoctrine()->getManager();
			$em -> persist($post);
			$em -> flush();
			// return $this->RedirectToRoute("user_post_list");
			return $this->RedirectToRoute("user_myaccount");
		}

		return $this->render('AppBundle:Post:addPost.html.twig', ['form'=>$form->createView()]);

	}




    /**
    * @Route("user/post/{id}/update", name="user_post_update")
    */
	public function updateAction(Post $post, Request $request)
	{
		
		$form = $this->createForm(PostType::class, $post);

		$fname = '';
        if($post->getImg()!= null or $post->getImg()!= '' )
        {
            $post->setImg(
                new File($this->getParameter('profile_img_dir').'/'.$post->getImg())
            );
            $fileimg = $post->getImg();
            $fname = $post->getImg()->getFileName();
        }

		$form->HandleRequest($request);
		if($form->isSubmitted() && $form->isValid())
		{
			if($file = $post->getImg())
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move(
                    $this->getParameter('profile_img_dir'),
                    $fileName
                );
                $post->setImg($fileName);  
            }
            else
            {
                $post->setImg($fname);  
            }
			$em = $this->getDoctrine()->getManager();
			$em->flush();
			return $this->RedirectToRoute("user_myaccount");
		}

		return $this->render('AppBundle:Post:updatePost.html.twig', ['form'=>$form->createView(), 'postimg' => $fname]);

	}


    /**
    * @Route("user/post/{id}/remove", name="user_post_remove")
    */
	public function removeAction(Post $post, Request $request)
	{
			$em = $this->getDoctrine()->getManager();
			$em->remove($post);
			$em->flush();
			// return $this->RedirectToRoute("user_post_list");
			return $this->RedirectToRoute("user_myaccount");
	}

	/**
    * @Route("user/post/{id}/view", name="user_post_view")
    */
	public function viewPostAction(Post $post, Request $request)
	{
		//get comments in the post
		$hasComment = $post->getComment();
		//see if the user who posted the post is the one who is viewing the post
		$checkUser= "false";
		$postUserID = $post->getUser()->getId();
		$loggedInUserID = $this->getUser()->getId();
		
		// dump($postUserID . ' ' .$loggedInUserID);
		// die();
		if($postUserID == $loggedInUserID)
		{
			$checkUser = "true";
		}
		//initialization
		$comment = new Comment();
		$user = new User();
		$form = $this->createForm(CommentType::class, $comment);
		$user = $this->getUser();

		//associate user and post to comment
		$comment->setUser($user);
		$comment->setPost($post);
		// dump($user->getName());
		// die();
		$comment->setName($user->getName());

		$form->HandleRequest($request);
		if($form->isSubmitted() && $form->isValid())
		{
			
			$em = $this->getDoctrine()->getManager();
			$em -> persist($comment);
			$em -> flush();
			$this->addFlash('notice', 'Comment added.');
			//para mawala ung value ng comment box
			unset($comment);
			unset($form);
			$comment = new Comment();
			$form = $this->createForm(CommentType::class, $comment);
		}
		
		return $this->render('AppBundle:Post:viewPost.html.twig', ['Post' => $post, 
																	'form'=>$form->createView(),
																	'comments' => $hasComment,
																	'checkUser' => $checkUser]);
	}



	/**
    * @Route("user/post/comment/{id}/remove", name="remove_comment")
    */
	public function removeCommentAction(Comment $comment, Request $request)
	{
			$request = new Request();
			$post = $comment->getPost();
			$em = $this->getDoctrine()->getManager();
			$em->remove($comment);
			$em->flush();

			return $this->RedirectToRoute("user_post_view", ['id'=>$post->getId()]);
	}

	/**
    * @Route("user/post/comment/{id}/edit", name="edit_comment")
    */
    public function editCommentAction(Request $request, Comment $comment)
    {
		$post = $comment->getPost();
		//get comments in the post
		$hasComment = $post->getComment();
		//see if the user who posted the post is the one who is viewing the post
		$checkUser= "false";
		$postUserID = $post->getUser()->getId();
		$loggedInUserID = $this->getUser()->getId();
		
		// dump($postUserID . ' ' .$loggedInUserID);
		// die();
		if($postUserID == $loggedInUserID)
		{
			$checkUser = "true";
		}
		//initialization
		//$comment = new Comment();
		$user = new User();
		$form = $this->createForm(CommentType::class, $comment);
		$user = $this->getUser();

		//associate user and post to comment
		$comment->setUser($user);
		$comment->setPost($post);

		$form->HandleRequest($request);
		if($form->isSubmitted() && $form->isValid())
		{
			$em = $this->getDoctrine()->getManager();
			
			$em -> flush();
			$this->addFlash('notice', 'Comment edited.');

			//para mawala ung value ng comment box
			unset($comment);
			unset($form);
			$comment = new Comment();
			$form = $this->createForm(CommentType::class, $comment);
		}
		
		return $this->render('AppBundle:Post:viewPost.html.twig', ['Post' => $post, 
																	'form'=>$form->createView(),
																	'comments' => $hasComment,
																	'checkUser' => $checkUser]);
	}
	
	/**
     * @Route("user/post/listbyId", name="user_post_list_byId")
     */
    public function listByUserAction()
    {
    	$em= $this->getDoctrine()->getManager();
    	$posts = $em->getRepository('AppBundle:Post')->findByUser($this->getUser()->getId());
        return $this->render('AppBundle:Post:listPost.html.twig', ['Posts' => $posts]);
    }
	

	/**
    * @Route("user/post/myArticles", name="post_myArticle")
    */
	public function myArticleaPostAction(Post $post, Request $request)
	{
		//$em= $this->getDoctrine()->getManager();
		$posts = $em->getRepository('AppBundle:Post')->findByUserId($this->getUser()->getId());
        return $this->render('AppBundle:Post:myArticles.html.twig', ['Post' => $post]);
	}






	/**
    * @Route("addpost", name="addpost")
    */
	public function createPostAction()
	{
	
		$user = new User();
		$user = $this->getUser();
		$post = new Post();
		$post->setTitle('sad');
		$post->setContent('sad');

		$post->setUser($user);
		// dump($post->getUser());
		// die();
		$em = $this->getDoctrine()->getManager();
        $em->persist($post);
        $em->flush();

		return new Response(
			'Saved new product with id: '.$post->getId()
			.' userID: '. $user->getId()
        );
		
	}


}