<?php

namespace AppBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request, AuthenticationUtils $authUtils)
    {
        $error = $authUtils->getLastAuthenticationError();
        $lastUsername = $authUtils->getLastUsername();

        return $this->render('AppBundle:Security:login.html.twig', [
            'last_username'=> $lastUsername,
            'error'=>$error
             ]);
    }

    /**
     * @Route("/security/checkRole", name="checkRole")
     */
    public function checkRoleAction(Request $request)
    {
        //dump($this->getUser());
        $route = '';
        if($this->getUser()->getRoles()[0]=='ROLE_ADMIN')
        {
            $route = 'user_post_list';
        }
        else
        {
            $route = 'user_post_list';
        }
        return $this->redirectToRoute($route);
    }
   
}