<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\File\File;

class UserController extends Controller
{
    /**
     * @Route(" /registration", name="registration")
     */
    public function addAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form

            ->remove('img')
            ->remove('birthdate')
            ->remove('name')
            ->remove('address')
            ->remove('description'); //fields remove form 


        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $var =$em->flush();
            $this->addFlash('notice', 'You have sucessfully registered your account. You may now login.');
            
            return $this->redirectToRoute('login');
        }

       return $this->render('AppBundle:User:registration.html.twig', ['form'=>$form->createView()]);
    }


    /**
     * @Route(" /{id}/changePassword", name="admin_changePassword")
     */
    public function userChangePassAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, User $user)
    {
        
        $form = $this->createForm(UserType::class, $user);
        $form
            ->remove('img')
            ->remove('birthdate')
            ->remove('name')
            ->remove('address')
            ->remove('description'); //fields remove form 


        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $this->addFlash('notice', 'You have sucessfully changed your password. You may now login.');
            
            return $this->redirectToRoute('login');
        }

       return $this->render('AppBundle:User:changePassword.html.twig', ['form'=>$form->createView()]);
    }


    /**
     * @Route(" /admin/listUser", name="admin_list_user")
     */
    public function adminListUserAction()
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('AppBundle:User')->findAll();

        $userId = $this->getUser()->getId();
        //$userId = $this->getUser()->getName();

       return $this->render('AppBundle:User:listUser.html.twig', ['users'=>$users, 'userid'=>$userId]);
    }

    /**
     * @Route("user/myaccount", name="user_myaccount")
     */
    public function userMyAccountAction()
    {
        $em = $this->getDoctrine()->getManager();
      
        $user = $this->getUser();
        $userId = $this->getUser()->getId();
        //$userId = $this->getUser()->getName();
        $userPost = $em->getRepository('AppBundle:Post')->findByUser($this->getUser()->getId());

       return $this->render('AppBundle:User:myAccount.html.twig', ['users'=>$user, 'userpost'=>$userPost, 'userid'=>$userId]);
    }


    /**
     * @Route(" /user/{id}/update", name="admin_update_userProfile")
     */
    public function adminUpdateUserAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, User $user)
    {
        //$user = new User();
        
        $form = $this->createForm(UserType::class, $user);
        $form
            ->remove('username')
            ->remove('plainPassword')
            ->remove('email');

        //set default values    
        $user->setPlainPassword('nulasdzdasdasl');
        //check wether there is a file in the db.
        $fname = '';
        if($user->getImg()!= null or $user->getImg()!= '' )
        {
            $user->setImg(
                new File($this->getParameter('profile_img_dir').'/'.$user->getImg())
            );
            $fileimg = $user->getImg();
            $fname = $user->getImg()->getFileName();
        }
        //end of checking
              

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            if($file = $user->getImg())
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move(
                    $this->getParameter('profile_img_dir'),
                    $fileName
                );
                $user->setImg($fileName);  
            }
            else
            {
                $user->setImg($fname);  
            }
            
            $em = $this->getDoctrine()->getManager();
            $var =$em->flush();
    
            $this->addFlash('notice', 'You have sucessfully updated '. $user->getUsername(). " account.");
        
            // return $this->redirectToRoute('admin_list_user');
            return $this->redirectToRoute('user_myaccount');
        }

       return $this->render('AppBundle:User:editprofile.html.twig', ['form'=>$form->createView(), 'userimg'=> $fname]);
    }





    






}
