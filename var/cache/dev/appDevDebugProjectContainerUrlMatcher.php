<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request;
        $requestMethod = $canonicalMethod = $context->getMethod();
        $scheme = $context->getScheme();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }


        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // contactus
        if ('/ContactUs' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::ContactUs',  '_route' => 'contactus',);
        }

        // about
        if ('/About' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::About',  '_route' => 'about',);
        }

        // app_post_send
        if ('/mail' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\PostController::sendAction',  '_route' => 'app_post_send',);
        }

        // user_post_list
        if ('/home' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\PostController::listAction',  '_route' => 'user_post_list',);
        }

        if (0 === strpos($pathinfo, '/user/post')) {
            // user_post_add
            if ('/user/post/add' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\PostController::addAction',  '_route' => 'user_post_add',);
            }

            // user_post_update
            if (preg_match('#^/user/post/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_post_update')), array (  '_controller' => 'AppBundle\\Controller\\PostController::updateAction',));
            }

            // user_post_remove
            if (preg_match('#^/user/post/(?P<id>[^/]++)/remove$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_post_remove')), array (  '_controller' => 'AppBundle\\Controller\\PostController::removeAction',));
            }

            // user_post_view
            if (preg_match('#^/user/post/(?P<id>[^/]++)/view$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_post_view')), array (  '_controller' => 'AppBundle\\Controller\\PostController::viewPostAction',));
            }

            if (0 === strpos($pathinfo, '/user/post/comment')) {
                // remove_comment
                if (preg_match('#^/user/post/comment/(?P<id>[^/]++)/remove$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'remove_comment')), array (  '_controller' => 'AppBundle\\Controller\\PostController::removeCommentAction',));
                }

                // edit_comment
                if (preg_match('#^/user/post/comment/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'edit_comment')), array (  '_controller' => 'AppBundle\\Controller\\PostController::editCommentAction',));
                }

            }

            // user_post_list_byId
            if ('/user/post/listbyId' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\PostController::listByUserAction',  '_route' => 'user_post_list_byId',);
            }

            // post_myArticle
            if ('/user/post/myArticles' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\PostController::myArticleaPostAction',  '_route' => 'post_myArticle',);
            }

        }

        // addpost
        if ('/addpost' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\PostController::createPostAction',  '_route' => 'addpost',);
        }

        // login
        if ('/login' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\SecurityController::loginAction',  '_route' => 'login',);
        }

        // checkRole
        if ('/security/checkRole' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\SecurityController::checkRoleAction',  '_route' => 'checkRole',);
        }

        // registration
        if ('/registration' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\UserController::addAction',  '_route' => 'registration',);
        }

        // admin_changePassword
        if (preg_match('#^/(?P<id>[^/]++)/changePassword$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_changePassword')), array (  '_controller' => 'AppBundle\\Controller\\UserController::userChangePassAction',));
        }

        // admin_list_user
        if ('/admin/listUser' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\UserController::adminListUserAction',  '_route' => 'admin_list_user',);
        }

        if (0 === strpos($pathinfo, '/user')) {
            // user_myaccount
            if ('/user/myaccount' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\UserController::userMyAccountAction',  '_route' => 'user_myaccount',);
            }

            // admin_update_userProfile
            if (preg_match('#^/user/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_update_userProfile')), array (  '_controller' => 'AppBundle\\Controller\\UserController::adminUpdateUserAction',));
            }

        }

        // logout
        if ('/logout' === $pathinfo) {
            return array('_route' => 'logout');
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
