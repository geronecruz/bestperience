<?php

/* AppBundle:Post:add.html.twig */
class __TwigTemplate_5574813781e8075dff1e7394dafb639f26db1e826b26d8664559cba844cad7d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:Post:add.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_df8303bb3934ac7a29ccae329267efe9ffa813512e6a00eb42e93051db117bfc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_df8303bb3934ac7a29ccae329267efe9ffa813512e6a00eb42e93051db117bfc->enter($__internal_df8303bb3934ac7a29ccae329267efe9ffa813512e6a00eb42e93051db117bfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:add.html.twig"));

        $__internal_e1f64239e84323befe0b373dcfca41d41c327adfb19fe082a641ad9f74e9dc39 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1f64239e84323befe0b373dcfca41d41c327adfb19fe082a641ad9f74e9dc39->enter($__internal_e1f64239e84323befe0b373dcfca41d41c327adfb19fe082a641ad9f74e9dc39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:add.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_df8303bb3934ac7a29ccae329267efe9ffa813512e6a00eb42e93051db117bfc->leave($__internal_df8303bb3934ac7a29ccae329267efe9ffa813512e6a00eb42e93051db117bfc_prof);

        
        $__internal_e1f64239e84323befe0b373dcfca41d41c327adfb19fe082a641ad9f74e9dc39->leave($__internal_e1f64239e84323befe0b373dcfca41d41c327adfb19fe082a641ad9f74e9dc39_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_4ecb9c20af58056bb25f451a76cfe81f6ba0dfdb685486b3bcf46cdf821a7aab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ecb9c20af58056bb25f451a76cfe81f6ba0dfdb685486b3bcf46cdf821a7aab->enter($__internal_4ecb9c20af58056bb25f451a76cfe81f6ba0dfdb685486b3bcf46cdf821a7aab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_1d786b5cbe1d2232b3c97cde463e326716da98d2d8abd3b0aa8587fe464a6539 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d786b5cbe1d2232b3c97cde463e326716da98d2d8abd3b0aa8587fe464a6539->enter($__internal_1d786b5cbe1d2232b3c97cde463e326716da98d2d8abd3b0aa8587fe464a6539_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Add Blog";
        
        $__internal_1d786b5cbe1d2232b3c97cde463e326716da98d2d8abd3b0aa8587fe464a6539->leave($__internal_1d786b5cbe1d2232b3c97cde463e326716da98d2d8abd3b0aa8587fe464a6539_prof);

        
        $__internal_4ecb9c20af58056bb25f451a76cfe81f6ba0dfdb685486b3bcf46cdf821a7aab->leave($__internal_4ecb9c20af58056bb25f451a76cfe81f6ba0dfdb685486b3bcf46cdf821a7aab_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_0dccac0ae905de38c0e9381bef0b293bc867ce0c46f5f261795ccf241df42afc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0dccac0ae905de38c0e9381bef0b293bc867ce0c46f5f261795ccf241df42afc->enter($__internal_0dccac0ae905de38c0e9381bef0b293bc867ce0c46f5f261795ccf241df42afc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_3080be6f64828733e16fe7dca7f76b7599a8b08107261117120c4fcbdd044043 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3080be6f64828733e16fe7dca7f76b7599a8b08107261117120c4fcbdd044043->enter($__internal_3080be6f64828733e16fe7dca7f76b7599a8b08107261117120c4fcbdd044043_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
\t";
        // line 7
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 7, $this->getSourceContext()); })()), 'form_start');
        echo "

\t\t<div class=\"alert alert-success\">
\t\t\t<center>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t";
        // line 12
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 12, $this->getSourceContext()); })()), "title", array()), 'row', array("attr" => array("class" => "form-control text-center")));
        echo "
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t";
        // line 15
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 15, $this->getSourceContext()); })()), "content", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t";
        // line 18
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 18, $this->getSourceContext()); })()), "img", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "
\t\t\t\t</div>
\t\t\t
\t\t\t<button class=\"btn btn-success\" type=\"submit\">share</button>\t\t

\t\t\t
\t\t\t</center>
\t\t</div>

\t";
        // line 27
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 27, $this->getSourceContext()); })()), 'form_end');
        echo "

";
        
        $__internal_3080be6f64828733e16fe7dca7f76b7599a8b08107261117120c4fcbdd044043->leave($__internal_3080be6f64828733e16fe7dca7f76b7599a8b08107261117120c4fcbdd044043_prof);

        
        $__internal_0dccac0ae905de38c0e9381bef0b293bc867ce0c46f5f261795ccf241df42afc->leave($__internal_0dccac0ae905de38c0e9381bef0b293bc867ce0c46f5f261795ccf241df42afc_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Post:add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 27,  91 => 18,  85 => 15,  79 => 12,  71 => 7,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}

{% block title %}Add Blog{% endblock %}

{% block body %}

\t{{form_start(form)}}

\t\t<div class=\"alert alert-success\">
\t\t\t<center>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t{{form_row(form.title, {'attr': {'class': 'form-control text-center'}} )}}
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t{{form_row(form.content, {'attr': {'class': 'form-control'}} )}}
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t{{form_row(form.img, {'attr': {'class': 'form-control'}} )}}
\t\t\t\t</div>
\t\t\t
\t\t\t<button class=\"btn btn-success\" type=\"submit\">share</button>\t\t

\t\t\t
\t\t\t</center>
\t\t</div>

\t{{form_end(form)}}

{% endblock %}", "AppBundle:Post:add.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/Post/add.html.twig");
    }
}
