<?php

/* AppBundle:Post:list.html.twig */
class __TwigTemplate_e369c663951fb00dd40ddaa223a759eacd86257dba0a819504dd1608be167647 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:Post:list.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1c1c3144a6a058e781634cdf5cd7eba3067f3668fb60cc1582493293ebb758c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c1c3144a6a058e781634cdf5cd7eba3067f3668fb60cc1582493293ebb758c1->enter($__internal_1c1c3144a6a058e781634cdf5cd7eba3067f3668fb60cc1582493293ebb758c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:list.html.twig"));

        $__internal_921c7301419c363ce3a519559d1f0a5d9aa1908687574ccc849b05466ea4acad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_921c7301419c363ce3a519559d1f0a5d9aa1908687574ccc849b05466ea4acad->enter($__internal_921c7301419c363ce3a519559d1f0a5d9aa1908687574ccc849b05466ea4acad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1c1c3144a6a058e781634cdf5cd7eba3067f3668fb60cc1582493293ebb758c1->leave($__internal_1c1c3144a6a058e781634cdf5cd7eba3067f3668fb60cc1582493293ebb758c1_prof);

        
        $__internal_921c7301419c363ce3a519559d1f0a5d9aa1908687574ccc849b05466ea4acad->leave($__internal_921c7301419c363ce3a519559d1f0a5d9aa1908687574ccc849b05466ea4acad_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_05a3ec454825df73770ab53735996bc39ab799640d336c600ffef287b0f8edfd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_05a3ec454825df73770ab53735996bc39ab799640d336c600ffef287b0f8edfd->enter($__internal_05a3ec454825df73770ab53735996bc39ab799640d336c600ffef287b0f8edfd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_dfab5de8aaa15193d3899f5651c71c0f932f5d12f38148fa9549176293f5206b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dfab5de8aaa15193d3899f5651c71c0f932f5d12f38148fa9549176293f5206b->enter($__internal_dfab5de8aaa15193d3899f5651c71c0f932f5d12f38148fa9549176293f5206b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Articles";
        
        $__internal_dfab5de8aaa15193d3899f5651c71c0f932f5d12f38148fa9549176293f5206b->leave($__internal_dfab5de8aaa15193d3899f5651c71c0f932f5d12f38148fa9549176293f5206b_prof);

        
        $__internal_05a3ec454825df73770ab53735996bc39ab799640d336c600ffef287b0f8edfd->leave($__internal_05a3ec454825df73770ab53735996bc39ab799640d336c600ffef287b0f8edfd_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_caec047ee67b894cc2e00145722883152a413550023242a22fd25fd01f97353a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_caec047ee67b894cc2e00145722883152a413550023242a22fd25fd01f97353a->enter($__internal_caec047ee67b894cc2e00145722883152a413550023242a22fd25fd01f97353a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_acffbf125b045e0ffc3abfc6d4b5c7065f9e34ae49407d37477f95f81a29c7bb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_acffbf125b045e0ffc3abfc6d4b5c7065f9e34ae49407d37477f95f81a29c7bb->enter($__internal_acffbf125b045e0ffc3abfc6d4b5c7065f9e34ae49407d37477f95f81a29c7bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "<h1>Welcome to the Post:list page</h1>

    <table class=\"table table-hover\">
        <thead>
            <tr>
                <th>Date</th>
                <th>Title</th>
                <th>Content</th>
                <th>Option</th>
            </tr>
        </thead>
        <tbody>
            ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["Posts"] ?? $this->getContext($context, "Posts")));
        foreach ($context['_seq'] as $context["_key"] => $context["Post"]) {
            // line 20
            echo "                <tr>
                    <td>";
            // line 21
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["Post"], "date", array()), "M/d/Y H:i"), "html", null, true);
            echo "</td>
                    <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["Post"], "title", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["Post"], "content", array()), "html", null, true);
            echo "</td>
                    <td><a href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_update", array("id" => $this->getAttribute($context["Post"], "id", array()))), "html", null, true);
            echo "\">update</a> <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_remove", array("id" => $this->getAttribute($context["Post"], "id", array()))), "html", null, true);
            echo "\">remove</a></td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['Post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "        </tbody>
    </table>
            
    
";
        
        $__internal_acffbf125b045e0ffc3abfc6d4b5c7065f9e34ae49407d37477f95f81a29c7bb->leave($__internal_acffbf125b045e0ffc3abfc6d4b5c7065f9e34ae49407d37477f95f81a29c7bb_prof);

        
        $__internal_caec047ee67b894cc2e00145722883152a413550023242a22fd25fd01f97353a->leave($__internal_caec047ee67b894cc2e00145722883152a413550023242a22fd25fd01f97353a_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Post:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 27,  101 => 24,  97 => 23,  93 => 22,  89 => 21,  86 => 20,  82 => 19,  68 => 7,  59 => 6,  41 => 4,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("
{% extends \"::base.html.twig\" %}

{% block title %}Articles{% endblock %}

{% block body %}
<h1>Welcome to the Post:list page</h1>

    <table class=\"table table-hover\">
        <thead>
            <tr>
                <th>Date</th>
                <th>Title</th>
                <th>Content</th>
                <th>Option</th>
            </tr>
        </thead>
        <tbody>
            {%for Post in Posts%}
                <tr>
                    <td>{{Post.date|date('M/d/Y H:i')}}</td>
                    <td>{{Post.title}}</td>
                    <td>{{Post.content}}</td>
                    <td><a href=\"{{path('post_update',{id:Post.id})}}\">update</a> <a href=\"{{path('post_remove',{id:Post.id})}}\">remove</a></td>
                </tr>
            {%endfor%}
        </tbody>
    </table>
            
    
{% endblock %}
", "AppBundle:Post:list.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/Post/list.html.twig");
    }
}
