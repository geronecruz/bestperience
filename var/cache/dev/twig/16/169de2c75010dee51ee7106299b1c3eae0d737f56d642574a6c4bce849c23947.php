<?php

/* AppBundle:Post:addPost.html.twig */
class __TwigTemplate_104166d0d7a8a0335c25a51b68894f3eebb39635f17c414c105284caa495e104 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::loggedin.html.twig", "AppBundle:Post:addPost.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::loggedin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_54a50228ebb86bc08359b2c8d27c068e5963432e519e2055b09abe45c566149f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_54a50228ebb86bc08359b2c8d27c068e5963432e519e2055b09abe45c566149f->enter($__internal_54a50228ebb86bc08359b2c8d27c068e5963432e519e2055b09abe45c566149f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:addPost.html.twig"));

        $__internal_ba58ee1c6b7550a978818a03d94682e455578da772b22d7978c2ac30c7cc703d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba58ee1c6b7550a978818a03d94682e455578da772b22d7978c2ac30c7cc703d->enter($__internal_ba58ee1c6b7550a978818a03d94682e455578da772b22d7978c2ac30c7cc703d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:addPost.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_54a50228ebb86bc08359b2c8d27c068e5963432e519e2055b09abe45c566149f->leave($__internal_54a50228ebb86bc08359b2c8d27c068e5963432e519e2055b09abe45c566149f_prof);

        
        $__internal_ba58ee1c6b7550a978818a03d94682e455578da772b22d7978c2ac30c7cc703d->leave($__internal_ba58ee1c6b7550a978818a03d94682e455578da772b22d7978c2ac30c7cc703d_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_505aa5af94586847d441bd8aaa65c42cba1b5f83d32451809450ccc8ba1bd777 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_505aa5af94586847d441bd8aaa65c42cba1b5f83d32451809450ccc8ba1bd777->enter($__internal_505aa5af94586847d441bd8aaa65c42cba1b5f83d32451809450ccc8ba1bd777_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_9b12e8c65a89eb35aa5ae757b3ee634e6432ba4025dda7d7527c3afb5863d8a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b12e8c65a89eb35aa5ae757b3ee634e6432ba4025dda7d7527c3afb5863d8a8->enter($__internal_9b12e8c65a89eb35aa5ae757b3ee634e6432ba4025dda7d7527c3afb5863d8a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Add Blog";
        
        $__internal_9b12e8c65a89eb35aa5ae757b3ee634e6432ba4025dda7d7527c3afb5863d8a8->leave($__internal_9b12e8c65a89eb35aa5ae757b3ee634e6432ba4025dda7d7527c3afb5863d8a8_prof);

        
        $__internal_505aa5af94586847d441bd8aaa65c42cba1b5f83d32451809450ccc8ba1bd777->leave($__internal_505aa5af94586847d441bd8aaa65c42cba1b5f83d32451809450ccc8ba1bd777_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_7d8a5569df81e8486e70c0dd33baad77406a4607f9e4b3508d2f1b0551311aa2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7d8a5569df81e8486e70c0dd33baad77406a4607f9e4b3508d2f1b0551311aa2->enter($__internal_7d8a5569df81e8486e70c0dd33baad77406a4607f9e4b3508d2f1b0551311aa2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_935a11ef923bdebf4fd27101ee75c8e9b0012a3dd2efab7b26b247fb60a421ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_935a11ef923bdebf4fd27101ee75c8e9b0012a3dd2efab7b26b247fb60a421ff->enter($__internal_935a11ef923bdebf4fd27101ee75c8e9b0012a3dd2efab7b26b247fb60a421ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo " <section id=\"home-section\" class=\"line\">                 
\t";
        // line 7
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 7, $this->getSourceContext()); })()), 'form_start');
        echo "

\t\t<div class=\"\" style=\"padding-left: 100px; padding-right:100px; padding-top:50;padding-bottom:50; background-color: #19334d; margin: 10px\">
\t\t\t<div class=\"panel-heading\" style=\"margin-bottom: 10px; font-size: 20px;\">Add Article</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t";
        // line 12
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 12, $this->getSourceContext()); })()), "img", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t";
        // line 15
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 15, $this->getSourceContext()); })()), "title", array()), 'row', array("attr" => array("class" => "form-control text-center", "placeholder" => "Title")));
        echo "
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t";
        // line 18
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 18, $this->getSourceContext()); })()), "content", array()), 'row', array("attr" => array("class" => "form-control", "placeholder" => "Content")));
        echo "
\t\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t<button class=\"btn btn-primary\" type=\"submit\">Post</button>\t\t

\t\t\t
\t\t</div>

\t";
        // line 27
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 27, $this->getSourceContext()); })()), 'form_end');
        echo "
</section>
";
        
        $__internal_935a11ef923bdebf4fd27101ee75c8e9b0012a3dd2efab7b26b247fb60a421ff->leave($__internal_935a11ef923bdebf4fd27101ee75c8e9b0012a3dd2efab7b26b247fb60a421ff_prof);

        
        $__internal_7d8a5569df81e8486e70c0dd33baad77406a4607f9e4b3508d2f1b0551311aa2->leave($__internal_7d8a5569df81e8486e70c0dd33baad77406a4607f9e4b3508d2f1b0551311aa2_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Post:addPost.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 27,  91 => 18,  85 => 15,  79 => 12,  71 => 7,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::loggedin.html.twig\" %}

{% block title %}Add Blog{% endblock %}

{% block body %}
 <section id=\"home-section\" class=\"line\">                 
\t{{form_start(form)}}

\t\t<div class=\"\" style=\"padding-left: 100px; padding-right:100px; padding-top:50;padding-bottom:50; background-color: #19334d; margin: 10px\">
\t\t\t<div class=\"panel-heading\" style=\"margin-bottom: 10px; font-size: 20px;\">Add Article</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t{{form_row(form.img, {'attr': {'class': 'form-control'}} )}}
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t{{form_row(form.title, {'attr': {'class': 'form-control text-center', 'placeholder': 'Title'}} )}}
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t{{form_row(form.content, {'attr': {'class': 'form-control' \t , 'placeholder': 'Content'}} )}}
\t\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t<button class=\"btn btn-primary\" type=\"submit\">Post</button>\t\t

\t\t\t
\t\t</div>

\t{{form_end(form)}}
</section>
{% endblock %}", "AppBundle:Post:addPost.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/Post/addPost.html.twig");
    }
}
