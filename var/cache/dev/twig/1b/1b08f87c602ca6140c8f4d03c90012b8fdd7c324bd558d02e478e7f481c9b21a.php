<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_5ace0514ac51dddf7015e278581d1ccbc34e9ce9c04fa0c5adac0bc55a3fad5b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fa54b0fb47cc07f88a76dfd7a2edb1923c840c99913ed16119fa1e1a02585313 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa54b0fb47cc07f88a76dfd7a2edb1923c840c99913ed16119fa1e1a02585313->enter($__internal_fa54b0fb47cc07f88a76dfd7a2edb1923c840c99913ed16119fa1e1a02585313_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_b5241fd1b9da9c15e641a31a30f06c9b12b8cd606f59d4f3cbe7fc061f01a0ba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b5241fd1b9da9c15e641a31a30f06c9b12b8cd606f59d4f3cbe7fc061f01a0ba->enter($__internal_b5241fd1b9da9c15e641a31a30f06c9b12b8cd606f59d4f3cbe7fc061f01a0ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fa54b0fb47cc07f88a76dfd7a2edb1923c840c99913ed16119fa1e1a02585313->leave($__internal_fa54b0fb47cc07f88a76dfd7a2edb1923c840c99913ed16119fa1e1a02585313_prof);

        
        $__internal_b5241fd1b9da9c15e641a31a30f06c9b12b8cd606f59d4f3cbe7fc061f01a0ba->leave($__internal_b5241fd1b9da9c15e641a31a30f06c9b12b8cd606f59d4f3cbe7fc061f01a0ba_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6a67c3d8dcdd4d6f3ecb5221dea71bcf3accfd7b42161c21880ef178048deb46 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6a67c3d8dcdd4d6f3ecb5221dea71bcf3accfd7b42161c21880ef178048deb46->enter($__internal_6a67c3d8dcdd4d6f3ecb5221dea71bcf3accfd7b42161c21880ef178048deb46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_291d1004a8a1945328b9736c0395b6abb81320ab3c34f3acf993662cb48cc464 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_291d1004a8a1945328b9736c0395b6abb81320ab3c34f3acf993662cb48cc464->enter($__internal_291d1004a8a1945328b9736c0395b6abb81320ab3c34f3acf993662cb48cc464_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_291d1004a8a1945328b9736c0395b6abb81320ab3c34f3acf993662cb48cc464->leave($__internal_291d1004a8a1945328b9736c0395b6abb81320ab3c34f3acf993662cb48cc464_prof);

        
        $__internal_6a67c3d8dcdd4d6f3ecb5221dea71bcf3accfd7b42161c21880ef178048deb46->leave($__internal_6a67c3d8dcdd4d6f3ecb5221dea71bcf3accfd7b42161c21880ef178048deb46_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_7c2faad7847327dbabb7cab2c94cbf0b5556bf5aafac993f0c51703e751f907a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c2faad7847327dbabb7cab2c94cbf0b5556bf5aafac993f0c51703e751f907a->enter($__internal_7c2faad7847327dbabb7cab2c94cbf0b5556bf5aafac993f0c51703e751f907a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_32dde50a87486b235ae991cb3eaa37ceb294606622a9258234ce249c4ab49b87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_32dde50a87486b235ae991cb3eaa37ceb294606622a9258234ce249c4ab49b87->enter($__internal_32dde50a87486b235ae991cb3eaa37ceb294606622a9258234ce249c4ab49b87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_32dde50a87486b235ae991cb3eaa37ceb294606622a9258234ce249c4ab49b87->leave($__internal_32dde50a87486b235ae991cb3eaa37ceb294606622a9258234ce249c4ab49b87_prof);

        
        $__internal_7c2faad7847327dbabb7cab2c94cbf0b5556bf5aafac993f0c51703e751f907a->leave($__internal_7c2faad7847327dbabb7cab2c94cbf0b5556bf5aafac993f0c51703e751f907a_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_2cc95d637e99ac37285f90c167f88408173195eced30ba98478addf3f6cdc6a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2cc95d637e99ac37285f90c167f88408173195eced30ba98478addf3f6cdc6a7->enter($__internal_2cc95d637e99ac37285f90c167f88408173195eced30ba98478addf3f6cdc6a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f7de77df4c17f5dc2a5162300d342cfa4e65fbb2efd3d9706bb4c76ba7f1e6fc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f7de77df4c17f5dc2a5162300d342cfa4e65fbb2efd3d9706bb4c76ba7f1e6fc->enter($__internal_f7de77df4c17f5dc2a5162300d342cfa4e65fbb2efd3d9706bb4c76ba7f1e6fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_f7de77df4c17f5dc2a5162300d342cfa4e65fbb2efd3d9706bb4c76ba7f1e6fc->leave($__internal_f7de77df4c17f5dc2a5162300d342cfa4e65fbb2efd3d9706bb4c76ba7f1e6fc_prof);

        
        $__internal_2cc95d637e99ac37285f90c167f88408173195eced30ba98478addf3f6cdc6a7->leave($__internal_2cc95d637e99ac37285f90c167f88408173195eced30ba98478addf3f6cdc6a7_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/ron/bestperience/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
