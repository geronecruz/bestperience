<?php

/* default/ContactUs.html.twig */
class __TwigTemplate_87a46445cf4e6b3ef4f910a2cc4a11a51de1f2cfe524f029a3c1173566bb6bf5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::loggedin.html.twig", "default/ContactUs.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::loggedin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7bff170f74e2a57b4fbf23445842c1533471f32287d2f2faa9c2829a81501f71 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7bff170f74e2a57b4fbf23445842c1533471f32287d2f2faa9c2829a81501f71->enter($__internal_7bff170f74e2a57b4fbf23445842c1533471f32287d2f2faa9c2829a81501f71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/ContactUs.html.twig"));

        $__internal_8bce36d0d990b244e88589b764e1f78057fd0f975deee8d76014cd4c461b77a2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8bce36d0d990b244e88589b764e1f78057fd0f975deee8d76014cd4c461b77a2->enter($__internal_8bce36d0d990b244e88589b764e1f78057fd0f975deee8d76014cd4c461b77a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/ContactUs.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7bff170f74e2a57b4fbf23445842c1533471f32287d2f2faa9c2829a81501f71->leave($__internal_7bff170f74e2a57b4fbf23445842c1533471f32287d2f2faa9c2829a81501f71_prof);

        
        $__internal_8bce36d0d990b244e88589b764e1f78057fd0f975deee8d76014cd4c461b77a2->leave($__internal_8bce36d0d990b244e88589b764e1f78057fd0f975deee8d76014cd4c461b77a2_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_cdfc9bc086b301b63d3b96694cd56ee1e6f2c976d7f617b02d60a662410ea04f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cdfc9bc086b301b63d3b96694cd56ee1e6f2c976d7f617b02d60a662410ea04f->enter($__internal_cdfc9bc086b301b63d3b96694cd56ee1e6f2c976d7f617b02d60a662410ea04f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_d2f4717e453ec7e01f18fdfb306427797b1073788d0e3829beaea91f58432336 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d2f4717e453ec7e01f18fdfb306427797b1073788d0e3829beaea91f58432336->enter($__internal_d2f4717e453ec7e01f18fdfb306427797b1073788d0e3829beaea91f58432336_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Contact Us";
        
        $__internal_d2f4717e453ec7e01f18fdfb306427797b1073788d0e3829beaea91f58432336->leave($__internal_d2f4717e453ec7e01f18fdfb306427797b1073788d0e3829beaea91f58432336_prof);

        
        $__internal_cdfc9bc086b301b63d3b96694cd56ee1e6f2c976d7f617b02d60a662410ea04f->leave($__internal_cdfc9bc086b301b63d3b96694cd56ee1e6f2c976d7f617b02d60a662410ea04f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_a7dbdd5c6b807741da10271499a929ff9ccd16782a85a3774800647b8e3f658d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a7dbdd5c6b807741da10271499a929ff9ccd16782a85a3774800647b8e3f658d->enter($__internal_a7dbdd5c6b807741da10271499a929ff9ccd16782a85a3774800647b8e3f658d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_adc6d8842da70a7b54d1ce53e087ce56f6c5ef75aa068d354b9f235ac9b4cd66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_adc6d8842da70a7b54d1ce53e087ce56f6c5ef75aa068d354b9f235ac9b4cd66->enter($__internal_adc6d8842da70a7b54d1ce53e087ce56f6c5ef75aa068d354b9f235ac9b4cd66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
\t\t


<table class=\"table table-hover\">
<thead>
\t<td>Contact us @ EmailUs@yahoo.com.ph !</td>
\t

</thead>
</table>
<div class=\"contactus\">
<h2>Contact Us</h2>
<i class=\"fa fa-envelope\"></i>
<br>
<i class=\"fa fa-map-marker\"></i>
</div>
</div>




<div class=\"form \">
<form >
\t<input type=\"text\" class=\"input\" id=\"name\" name=\"Fullname\" placeholder=\"Full Name\">


\t<input type=\"text\" class=\"input\" id=\"email\" name=\"emailadd\" placeholder=\"Email Address\">


\t<input type=\"text\" class=\"input\" id=\"message\" name=\"message\" placeholder=\"Your Message\">

\t<br>
\t<input type=\"submit\" class=\"submitbutton\" value=\"Submit\">
\t</center>
</form>
</div>





";
        
        $__internal_adc6d8842da70a7b54d1ce53e087ce56f6c5ef75aa068d354b9f235ac9b4cd66->leave($__internal_adc6d8842da70a7b54d1ce53e087ce56f6c5ef75aa068d354b9f235ac9b4cd66_prof);

        
        $__internal_a7dbdd5c6b807741da10271499a929ff9ccd16782a85a3774800647b8e3f658d->leave($__internal_a7dbdd5c6b807741da10271499a929ff9ccd16782a85a3774800647b8e3f658d_prof);

    }

    public function getTemplateName()
    {
        return "default/ContactUs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::loggedin.html.twig\" %}

{% block title %}Contact Us{% endblock %}

{% block body %}

\t\t


<table class=\"table table-hover\">
<thead>
\t<td>Contact us @ EmailUs@yahoo.com.ph !</td>
\t

</thead>
</table>
<div class=\"contactus\">
<h2>Contact Us</h2>
<i class=\"fa fa-envelope\"></i>
<br>
<i class=\"fa fa-map-marker\"></i>
</div>
</div>




<div class=\"form \">
<form >
\t<input type=\"text\" class=\"input\" id=\"name\" name=\"Fullname\" placeholder=\"Full Name\">


\t<input type=\"text\" class=\"input\" id=\"email\" name=\"emailadd\" placeholder=\"Email Address\">


\t<input type=\"text\" class=\"input\" id=\"message\" name=\"message\" placeholder=\"Your Message\">

\t<br>
\t<input type=\"submit\" class=\"submitbutton\" value=\"Submit\">
\t</center>
</form>
</div>





{% endblock %}
", "default/ContactUs.html.twig", "/home/ron/bestperience/app/Resources/views/default/ContactUs.html.twig");
    }
}
