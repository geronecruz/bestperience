<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_8006b7f8fc2624f01b3de6d52decc5ff3e8fa8050fc8435cce232d4e154540ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8bbf5b53e6507c942f58a70f2f984cd526bdde7b18f4fb270ac9d3dd5e8e22ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8bbf5b53e6507c942f58a70f2f984cd526bdde7b18f4fb270ac9d3dd5e8e22ee->enter($__internal_8bbf5b53e6507c942f58a70f2f984cd526bdde7b18f4fb270ac9d3dd5e8e22ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_cb8ba816e3d64f648fb60789f33dfcbb4aa227d3d0b337e5c755d2af7fe46b57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb8ba816e3d64f648fb60789f33dfcbb4aa227d3d0b337e5c755d2af7fe46b57->enter($__internal_cb8ba816e3d64f648fb60789f33dfcbb4aa227d3d0b337e5c755d2af7fe46b57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8bbf5b53e6507c942f58a70f2f984cd526bdde7b18f4fb270ac9d3dd5e8e22ee->leave($__internal_8bbf5b53e6507c942f58a70f2f984cd526bdde7b18f4fb270ac9d3dd5e8e22ee_prof);

        
        $__internal_cb8ba816e3d64f648fb60789f33dfcbb4aa227d3d0b337e5c755d2af7fe46b57->leave($__internal_cb8ba816e3d64f648fb60789f33dfcbb4aa227d3d0b337e5c755d2af7fe46b57_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_1f8475f597e0481aa7c03f35d4a8d76c524504bd880c7704a96fe64f76bcef01 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1f8475f597e0481aa7c03f35d4a8d76c524504bd880c7704a96fe64f76bcef01->enter($__internal_1f8475f597e0481aa7c03f35d4a8d76c524504bd880c7704a96fe64f76bcef01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_09bd6d08e2faf6c47e91f05fc00634cbf5d5b0b76edc2f9c0bbc5f5ceb733bee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09bd6d08e2faf6c47e91f05fc00634cbf5d5b0b76edc2f9c0bbc5f5ceb733bee->enter($__internal_09bd6d08e2faf6c47e91f05fc00634cbf5d5b0b76edc2f9c0bbc5f5ceb733bee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 4, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 6, $this->getSourceContext()); })()))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_09bd6d08e2faf6c47e91f05fc00634cbf5d5b0b76edc2f9c0bbc5f5ceb733bee->leave($__internal_09bd6d08e2faf6c47e91f05fc00634cbf5d5b0b76edc2f9c0bbc5f5ceb733bee_prof);

        
        $__internal_1f8475f597e0481aa7c03f35d4a8d76c524504bd880c7704a96fe64f76bcef01->leave($__internal_1f8475f597e0481aa7c03f35d4a8d76c524504bd880c7704a96fe64f76bcef01_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_8ac1c782015bda99778708e71af63daa507c72e720b15440afec314333808f9e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ac1c782015bda99778708e71af63daa507c72e720b15440afec314333808f9e->enter($__internal_8ac1c782015bda99778708e71af63daa507c72e720b15440afec314333808f9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_2474c93ba188c5be5ceb01da7f35dbd803e818a14acc77acdbd2b71ab5696dbf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2474c93ba188c5be5ceb01da7f35dbd803e818a14acc77acdbd2b71ab5696dbf->enter($__internal_2474c93ba188c5be5ceb01da7f35dbd803e818a14acc77acdbd2b71ab5696dbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 13, $this->getSourceContext()); })()), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 16, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_2474c93ba188c5be5ceb01da7f35dbd803e818a14acc77acdbd2b71ab5696dbf->leave($__internal_2474c93ba188c5be5ceb01da7f35dbd803e818a14acc77acdbd2b71ab5696dbf_prof);

        
        $__internal_8ac1c782015bda99778708e71af63daa507c72e720b15440afec314333808f9e->leave($__internal_8ac1c782015bda99778708e71af63daa507c72e720b15440afec314333808f9e_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_882d49cf7bd2642e24a5a3317f83240b7e1f0431df0730f8466b989d3cb92184 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_882d49cf7bd2642e24a5a3317f83240b7e1f0431df0730f8466b989d3cb92184->enter($__internal_882d49cf7bd2642e24a5a3317f83240b7e1f0431df0730f8466b989d3cb92184_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_8b6995f645c970c815162bc0b66ea4c0a49cf8e624706166aa594f1c8f59abe7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b6995f645c970c815162bc0b66ea4c0a49cf8e624706166aa594f1c8f59abe7->enter($__internal_8b6995f645c970c815162bc0b66ea4c0a49cf8e624706166aa594f1c8f59abe7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 27, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 33, $this->getSourceContext()); })()))));
            echo "
        </div>
    ";
        }
        
        $__internal_8b6995f645c970c815162bc0b66ea4c0a49cf8e624706166aa594f1c8f59abe7->leave($__internal_8b6995f645c970c815162bc0b66ea4c0a49cf8e624706166aa594f1c8f59abe7_prof);

        
        $__internal_882d49cf7bd2642e24a5a3317f83240b7e1f0431df0730f8466b989d3cb92184->leave($__internal_882d49cf7bd2642e24a5a3317f83240b7e1f0431df0730f8466b989d3cb92184_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/ron/bestperience/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
