<?php

/* default/Home.html.twig */
class __TwigTemplate_b7404317907b54af1e182c973967059ce358e60ad096fdfe1f319c9f8f689498 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "default/Home.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8a1b4dbfc59a0b0e3a5a1dab5f41fdc23714799c272b0d97e7043367372bf4ed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8a1b4dbfc59a0b0e3a5a1dab5f41fdc23714799c272b0d97e7043367372bf4ed->enter($__internal_8a1b4dbfc59a0b0e3a5a1dab5f41fdc23714799c272b0d97e7043367372bf4ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/Home.html.twig"));

        $__internal_952ec446af26a5183165812aa67d365bc887168ef0dfac4f56c5950023a0fbc4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_952ec446af26a5183165812aa67d365bc887168ef0dfac4f56c5950023a0fbc4->enter($__internal_952ec446af26a5183165812aa67d365bc887168ef0dfac4f56c5950023a0fbc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/Home.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8a1b4dbfc59a0b0e3a5a1dab5f41fdc23714799c272b0d97e7043367372bf4ed->leave($__internal_8a1b4dbfc59a0b0e3a5a1dab5f41fdc23714799c272b0d97e7043367372bf4ed_prof);

        
        $__internal_952ec446af26a5183165812aa67d365bc887168ef0dfac4f56c5950023a0fbc4->leave($__internal_952ec446af26a5183165812aa67d365bc887168ef0dfac4f56c5950023a0fbc4_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_6148b2861a01f63f097e87421bd6387000908a10e40f1047176ea68bc7cd24eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6148b2861a01f63f097e87421bd6387000908a10e40f1047176ea68bc7cd24eb->enter($__internal_6148b2861a01f63f097e87421bd6387000908a10e40f1047176ea68bc7cd24eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_09d348dfa796ee41dad50c164acbb5aa1432da7fb7a27bda27d3aba4584dbd44 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09d348dfa796ee41dad50c164acbb5aa1432da7fb7a27bda27d3aba4584dbd44->enter($__internal_09d348dfa796ee41dad50c164acbb5aa1432da7fb7a27bda27d3aba4584dbd44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Contact Us";
        
        $__internal_09d348dfa796ee41dad50c164acbb5aa1432da7fb7a27bda27d3aba4584dbd44->leave($__internal_09d348dfa796ee41dad50c164acbb5aa1432da7fb7a27bda27d3aba4584dbd44_prof);

        
        $__internal_6148b2861a01f63f097e87421bd6387000908a10e40f1047176ea68bc7cd24eb->leave($__internal_6148b2861a01f63f097e87421bd6387000908a10e40f1047176ea68bc7cd24eb_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_7974c1ca972936ed4cfb052cd839fc641aa73871f59d75ac07df13b1d961cad2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7974c1ca972936ed4cfb052cd839fc641aa73871f59d75ac07df13b1d961cad2->enter($__internal_7974c1ca972936ed4cfb052cd839fc641aa73871f59d75ac07df13b1d961cad2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e5e19c3bee755c2df83f87091da833a51a532138eb0ad3cfc5d7522d1bfb6edc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5e19c3bee755c2df83f87091da833a51a532138eb0ad3cfc5d7522d1bfb6edc->enter($__internal_e5e19c3bee755c2df83f87091da833a51a532138eb0ad3cfc5d7522d1bfb6edc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
<section id=\"home-sectiodn\" class=\"line\">
         <div class=\"margin\">
            <!-- ARTICLES -->             
            <div class=\"s-12 l-9\">
            
               <!-- ARTICLE 1 -->               
               <article class=\"post-1 line\">
                  <!-- image -->                 
                  <div class=\"s-12 l-6 post-image\">                   
                     <a href=\"post-1.php\">
                     <img src=\"img/post1.jpg\" alt=\"Fashion 1\">
                     </a>                
                  </div>
                  <!-- text -->                 
                  <div class=\"s-12 l-5 post-text\">
                     <a href=\"post-1.php\">
                        <h2>Free responsive template</h2>
                     </a>
                     <p>Lorem ipsum dolor sit amet, conse ctetuer. Duis autem vemeu iriure dolor adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim.             
                     </p>
                  </div>
                  <!-- date -->                 
                  <div class=\"s-12 l-1 post-date\">
                     <p class=\"date\">07</p>
                     <p class=\"month\">mar</p>
                  </div>
               </article>
              
            </div>
           
         </div>
      </section>


";
        
        $__internal_e5e19c3bee755c2df83f87091da833a51a532138eb0ad3cfc5d7522d1bfb6edc->leave($__internal_e5e19c3bee755c2df83f87091da833a51a532138eb0ad3cfc5d7522d1bfb6edc_prof);

        
        $__internal_7974c1ca972936ed4cfb052cd839fc641aa73871f59d75ac07df13b1d961cad2->leave($__internal_7974c1ca972936ed4cfb052cd839fc641aa73871f59d75ac07df13b1d961cad2_prof);

    }

    public function getTemplateName()
    {
        return "default/Home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}

{% block title %}Contact Us{% endblock %}

{% block body %}

<section id=\"home-sectiodn\" class=\"line\">
         <div class=\"margin\">
            <!-- ARTICLES -->             
            <div class=\"s-12 l-9\">
            
               <!-- ARTICLE 1 -->               
               <article class=\"post-1 line\">
                  <!-- image -->                 
                  <div class=\"s-12 l-6 post-image\">                   
                     <a href=\"post-1.php\">
                     <img src=\"img/post1.jpg\" alt=\"Fashion 1\">
                     </a>                
                  </div>
                  <!-- text -->                 
                  <div class=\"s-12 l-5 post-text\">
                     <a href=\"post-1.php\">
                        <h2>Free responsive template</h2>
                     </a>
                     <p>Lorem ipsum dolor sit amet, conse ctetuer. Duis autem vemeu iriure dolor adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim.             
                     </p>
                  </div>
                  <!-- date -->                 
                  <div class=\"s-12 l-1 post-date\">
                     <p class=\"date\">07</p>
                     <p class=\"month\">mar</p>
                  </div>
               </article>
              
            </div>
           
         </div>
      </section>


{% endblock%}
\t\t

", "default/Home.html.twig", "/home/ron/bestperience/app/Resources/views/default/Home.html.twig");
    }
}
