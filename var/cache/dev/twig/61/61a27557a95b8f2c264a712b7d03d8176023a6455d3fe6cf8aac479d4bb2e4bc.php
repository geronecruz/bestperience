<?php

/* default/About.html.twig */
class __TwigTemplate_66fff5691d862069916492cf1936081c8f6b544720196934f7cda4016f861ed7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "default/About.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f8c7adf163d6ebbaf347c5fa94f0ac89427a9234866542786c6f621c47216477 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f8c7adf163d6ebbaf347c5fa94f0ac89427a9234866542786c6f621c47216477->enter($__internal_f8c7adf163d6ebbaf347c5fa94f0ac89427a9234866542786c6f621c47216477_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/About.html.twig"));

        $__internal_efb2e28ce97de53a4d84eb2f58f2656e5d973e7f73dc594326f3d0c0f1206d2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_efb2e28ce97de53a4d84eb2f58f2656e5d973e7f73dc594326f3d0c0f1206d2d->enter($__internal_efb2e28ce97de53a4d84eb2f58f2656e5d973e7f73dc594326f3d0c0f1206d2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/About.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f8c7adf163d6ebbaf347c5fa94f0ac89427a9234866542786c6f621c47216477->leave($__internal_f8c7adf163d6ebbaf347c5fa94f0ac89427a9234866542786c6f621c47216477_prof);

        
        $__internal_efb2e28ce97de53a4d84eb2f58f2656e5d973e7f73dc594326f3d0c0f1206d2d->leave($__internal_efb2e28ce97de53a4d84eb2f58f2656e5d973e7f73dc594326f3d0c0f1206d2d_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_e4d3effb5806ba656e80c31649a75f2a62bed2fe74eaf391f4f5f22f85c43d3e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e4d3effb5806ba656e80c31649a75f2a62bed2fe74eaf391f4f5f22f85c43d3e->enter($__internal_e4d3effb5806ba656e80c31649a75f2a62bed2fe74eaf391f4f5f22f85c43d3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_d7df190573986be47630de74f14af77e842350fadb09cd0cb30f07a7eae00704 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7df190573986be47630de74f14af77e842350fadb09cd0cb30f07a7eae00704->enter($__internal_d7df190573986be47630de74f14af77e842350fadb09cd0cb30f07a7eae00704_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Contact Us";
        
        $__internal_d7df190573986be47630de74f14af77e842350fadb09cd0cb30f07a7eae00704->leave($__internal_d7df190573986be47630de74f14af77e842350fadb09cd0cb30f07a7eae00704_prof);

        
        $__internal_e4d3effb5806ba656e80c31649a75f2a62bed2fe74eaf391f4f5f22f85c43d3e->leave($__internal_e4d3effb5806ba656e80c31649a75f2a62bed2fe74eaf391f4f5f22f85c43d3e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_3721fecb383852edda870054d52198219774a6ea7f1a4baa98d5f48c8045d3e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3721fecb383852edda870054d52198219774a6ea7f1a4baa98d5f48c8045d3e3->enter($__internal_3721fecb383852edda870054d52198219774a6ea7f1a4baa98d5f48c8045d3e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_6ee1e6417d804bd0b0e623664bfe13d69db1f5da4c3f443a947726251ef3a948 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6ee1e6417d804bd0b0e623664bfe13d69db1f5da4c3f443a947726251ef3a948->enter($__internal_6ee1e6417d804bd0b0e623664bfe13d69db1f5da4c3f443a947726251ef3a948_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
\t\t
<hr>
<div>

\t<div class=\"info\">
  \t<a href=\"https://www.facebook.com/RonCruz28\" target=\"_blank\" src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/Books/ron.jpg"), "html", null, true);
        echo "\">
    <center>
    <img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/Books/ron.jpg"), "html", null, true);
        echo "\" style=\"max-height: 500px; max-width: 500px;\" >
    </img>
  </a>
\t <div class=\"infodesc\"><b> Gerone Cruz </b></div>
  \t<div class=\"number\">09369948215</div>



  <a href=\"https://www.facebook.com/kakarlka\" target=\"_blank\" src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/Books/karl1.jpg"), "html", null, true);
        echo "\">
    <center>
    <img src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/Books/karl1.jpg"), "html", null, true);
        echo "\"  style=\"max-height: 500px; max-width: 500px;\" >
    </img>
    </center>
  </a>
  <div class=\"infodesc\"><b> Karl Augustine Relovasa </b></div>
   <div class=\"number\">09367842951</div>



\t  <a href=\"https://www.facebook.com/pvqkeem125\" target=\"_blank\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/Books/kim.jpg"), "html", null, true);
        echo "\">
    <center>
    <img src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/Books/kim.jpg"), "html", null, true);
        echo "\" style=\"max-height: 500px; max-width: 500px;\">
  \t</img>
    </center>
    </a>
  \t<div class=\"infodesc\"><b> Kim Phillip Quinones </b></div>
     <div class=\"number\">09367701976</div><br>
\t</div>

\t<div class=\"info\">
  \t<a href=\"https://www.facebook.com/profile.php?id=100010502681906\" target=\"_blank\" src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/Books/lex.jpg"), "html", null, true);
        echo "\">
    \t<center>
      <img src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/Books/lex.jpg"), "html", null, true);
        echo "\" style=\"max-height: 500px; max-width: 500px;\">
  \t  </img>
      
    </a>
  \t<div class=\"infodesc\"><b> John Lex Pangilinan </b></div>
    \t <div class=\"number\">09453377989</div><br>

       </center>
\t</div>

</div>
<br>





<div class=\"info\">
  <a href=\"https://www.facebook.com/ButskiButski\" target=\"_blank\" src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/Books/buts.jpg"), "html", null, true);
        echo "\">
    \t<center>
    <img src=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/Books/buts.jpg"), "html", null, true);
        echo "\"style=\"max-height: 500px; max-width: 500px;\">
    </img>
  </a>
  <div class=\"infodesc\"><b> Meryll Butay </b></div>
     <div class=\"number\">09174584671</div><br>
     </center>
</div>

<div class=\"info\">
  <a href=\"https://www.facebook.com/judeyahhh\" target=\"_blank\" src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/Books/barts.jpg"), "html", null, true);
        echo "\">
    \t<center>
    <img src=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/Books/barts.jpg"), "html", null, true);
        echo "\" style=\"max-height: 500px; max-width: 500px;\">
    </img>
  </a>
  <div class=\"infodesc\"><b> Maria Judea Bartolome </b></div>
   <div class=\"number\">09751364897</div><br>
   </center>
   
</div>
</div>



";
        
        $__internal_6ee1e6417d804bd0b0e623664bfe13d69db1f5da4c3f443a947726251ef3a948->leave($__internal_6ee1e6417d804bd0b0e623664bfe13d69db1f5da4c3f443a947726251ef3a948_prof);

        
        $__internal_3721fecb383852edda870054d52198219774a6ea7f1a4baa98d5f48c8045d3e3->leave($__internal_3721fecb383852edda870054d52198219774a6ea7f1a4baa98d5f48c8045d3e3_prof);

    }

    public function getTemplateName()
    {
        return "default/About.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 77,  169 => 75,  157 => 66,  152 => 64,  131 => 46,  126 => 44,  114 => 35,  109 => 33,  97 => 24,  92 => 22,  81 => 14,  76 => 12,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}

{% block title %}Contact Us{% endblock %}

{% block body %}

\t\t
<hr>
<div>

\t<div class=\"info\">
  \t<a href=\"https://www.facebook.com/RonCruz28\" target=\"_blank\" src=\"{{asset('assets/image/Books/ron.jpg')}}\">
    <center>
    <img src=\"{{asset('assets/image/Books/ron.jpg')}}\" style=\"max-height: 500px; max-width: 500px;\" >
    </img>
  </a>
\t <div class=\"infodesc\"><b> Gerone Cruz </b></div>
  \t<div class=\"number\">09369948215</div>



  <a href=\"https://www.facebook.com/kakarlka\" target=\"_blank\" src=\"{{asset('assets/image/Books/karl1.jpg')}}\">
    <center>
    <img src=\"{{asset('assets/image/Books/karl1.jpg')}}\"  style=\"max-height: 500px; max-width: 500px;\" >
    </img>
    </center>
  </a>
  <div class=\"infodesc\"><b> Karl Augustine Relovasa </b></div>
   <div class=\"number\">09367842951</div>



\t  <a href=\"https://www.facebook.com/pvqkeem125\" target=\"_blank\" src=\"{{asset('assets/image/Books/kim.jpg')}}\">
    <center>
    <img src=\"{{asset('assets/image/Books/kim.jpg')}}\" style=\"max-height: 500px; max-width: 500px;\">
  \t</img>
    </center>
    </a>
  \t<div class=\"infodesc\"><b> Kim Phillip Quinones </b></div>
     <div class=\"number\">09367701976</div><br>
\t</div>

\t<div class=\"info\">
  \t<a href=\"https://www.facebook.com/profile.php?id=100010502681906\" target=\"_blank\" src=\"{{asset('assets/image/Books/lex.jpg')}}\">
    \t<center>
      <img src=\"{{asset('assets/image/Books/lex.jpg')}}\" style=\"max-height: 500px; max-width: 500px;\">
  \t  </img>
      
    </a>
  \t<div class=\"infodesc\"><b> John Lex Pangilinan </b></div>
    \t <div class=\"number\">09453377989</div><br>

       </center>
\t</div>

</div>
<br>





<div class=\"info\">
  <a href=\"https://www.facebook.com/ButskiButski\" target=\"_blank\" src=\"{{asset('assets/image/Books/buts.jpg')}}\">
    \t<center>
    <img src=\"{{asset('assets/image/Books/buts.jpg')}}\"style=\"max-height: 500px; max-width: 500px;\">
    </img>
  </a>
  <div class=\"infodesc\"><b> Meryll Butay </b></div>
     <div class=\"number\">09174584671</div><br>
     </center>
</div>

<div class=\"info\">
  <a href=\"https://www.facebook.com/judeyahhh\" target=\"_blank\" src=\"{{asset('assets/image/Books/barts.jpg')}}\">
    \t<center>
    <img src=\"{{asset('assets/image/Books/barts.jpg')}}\" style=\"max-height: 500px; max-width: 500px;\">
    </img>
  </a>
  <div class=\"infodesc\"><b> Maria Judea Bartolome </b></div>
   <div class=\"number\">09751364897</div><br>
   </center>
   
</div>
</div>



{% endblock %}

", "default/About.html.twig", "/home/ron/bestperience/app/Resources/views/default/About.html.twig");
    }
}
