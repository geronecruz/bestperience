<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_6c775e4c4ccdb3af11f3aa2031a382d5c0ed87f26f326c1f292fdbc62fcd4060 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d1b280a3bd5ce221ac129756610d82d07af8dc64ae3bb3aa38564b6e92e20f7e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1b280a3bd5ce221ac129756610d82d07af8dc64ae3bb3aa38564b6e92e20f7e->enter($__internal_d1b280a3bd5ce221ac129756610d82d07af8dc64ae3bb3aa38564b6e92e20f7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_f14b423d70452267ef1cb6ceda7af1a9f36250248899f8641601cd3e2ba9beb9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f14b423d70452267ef1cb6ceda7af1a9f36250248899f8641601cd3e2ba9beb9->enter($__internal_f14b423d70452267ef1cb6ceda7af1a9f36250248899f8641601cd3e2ba9beb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d1b280a3bd5ce221ac129756610d82d07af8dc64ae3bb3aa38564b6e92e20f7e->leave($__internal_d1b280a3bd5ce221ac129756610d82d07af8dc64ae3bb3aa38564b6e92e20f7e_prof);

        
        $__internal_f14b423d70452267ef1cb6ceda7af1a9f36250248899f8641601cd3e2ba9beb9->leave($__internal_f14b423d70452267ef1cb6ceda7af1a9f36250248899f8641601cd3e2ba9beb9_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_a1909cbb5b596a1fad65592f629726d7603afede003ce6edf0cb9bb40563c45a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a1909cbb5b596a1fad65592f629726d7603afede003ce6edf0cb9bb40563c45a->enter($__internal_a1909cbb5b596a1fad65592f629726d7603afede003ce6edf0cb9bb40563c45a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_c4266daa2ff91e7f514c00ab799b93443bfa3d326a52ef7d052361a45310646a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4266daa2ff91e7f514c00ab799b93443bfa3d326a52ef7d052361a45310646a->enter($__internal_c4266daa2ff91e7f514c00ab799b93443bfa3d326a52ef7d052361a45310646a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_c4266daa2ff91e7f514c00ab799b93443bfa3d326a52ef7d052361a45310646a->leave($__internal_c4266daa2ff91e7f514c00ab799b93443bfa3d326a52ef7d052361a45310646a_prof);

        
        $__internal_a1909cbb5b596a1fad65592f629726d7603afede003ce6edf0cb9bb40563c45a->leave($__internal_a1909cbb5b596a1fad65592f629726d7603afede003ce6edf0cb9bb40563c45a_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_d890aa45299b2d387cc87736f1d8ad143c318098e58f1f3ccb872deff93250c9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d890aa45299b2d387cc87736f1d8ad143c318098e58f1f3ccb872deff93250c9->enter($__internal_d890aa45299b2d387cc87736f1d8ad143c318098e58f1f3ccb872deff93250c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_34b4ca98f37383f9eb961fb487c64b3c19f47dacbfb1c93ba13312736dfcfbe4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_34b4ca98f37383f9eb961fb487c64b3c19f47dacbfb1c93ba13312736dfcfbe4->enter($__internal_34b4ca98f37383f9eb961fb487c64b3c19f47dacbfb1c93ba13312736dfcfbe4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_34b4ca98f37383f9eb961fb487c64b3c19f47dacbfb1c93ba13312736dfcfbe4->leave($__internal_34b4ca98f37383f9eb961fb487c64b3c19f47dacbfb1c93ba13312736dfcfbe4_prof);

        
        $__internal_d890aa45299b2d387cc87736f1d8ad143c318098e58f1f3ccb872deff93250c9->leave($__internal_d890aa45299b2d387cc87736f1d8ad143c318098e58f1f3ccb872deff93250c9_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_9277f4f71b9c4226f405155dfa4ad0d4089c7761b653fd2cd81d98132ca0087c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9277f4f71b9c4226f405155dfa4ad0d4089c7761b653fd2cd81d98132ca0087c->enter($__internal_9277f4f71b9c4226f405155dfa4ad0d4089c7761b653fd2cd81d98132ca0087c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_391cbefdfe8f5b4ecf848b705dc6ba398bd9c918fd4bac4a96732fd3108390ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_391cbefdfe8f5b4ecf848b705dc6ba398bd9c918fd4bac4a96732fd3108390ac->enter($__internal_391cbefdfe8f5b4ecf848b705dc6ba398bd9c918fd4bac4a96732fd3108390ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_391cbefdfe8f5b4ecf848b705dc6ba398bd9c918fd4bac4a96732fd3108390ac->leave($__internal_391cbefdfe8f5b4ecf848b705dc6ba398bd9c918fd4bac4a96732fd3108390ac_prof);

        
        $__internal_9277f4f71b9c4226f405155dfa4ad0d4089c7761b653fd2cd81d98132ca0087c->leave($__internal_9277f4f71b9c4226f405155dfa4ad0d4089c7761b653fd2cd81d98132ca0087c_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/ron/bestperience/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
