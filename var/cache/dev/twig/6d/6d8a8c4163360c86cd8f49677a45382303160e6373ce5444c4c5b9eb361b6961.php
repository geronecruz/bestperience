<?php

/* ::base.html.twig */
class __TwigTemplate_ef6a598e82a5011af52d5b328f0cd2f8830bea5f4c88506ad9e23671aec9697a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b111fc57d24757d0ea685053811a85c8d4429a4d01b13eb96d806812df0a83b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b111fc57d24757d0ea685053811a85c8d4429a4d01b13eb96d806812df0a83b0->enter($__internal_b111fc57d24757d0ea685053811a85c8d4429a4d01b13eb96d806812df0a83b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        $__internal_07c2f36fdee3477c5dfecff70658360d916faafdebd40745636a46ec4acb9f96 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_07c2f36fdee3477c5dfecff70658360d916faafdebd40745636a46ec4acb9f96->enter($__internal_07c2f36fdee3477c5dfecff70658360d916faafdebd40745636a46ec4acb9f96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
  \t <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link href=\"https://fonts.googleapis.com/css?family=Oswald\" rel=\"stylesheet\">
\t\t<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">
    \t<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/bootstrap.min.css"), "html", null, true);
        echo "\" />
\t\t<link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/Css/style.css"), "html", null, true);
        echo "\" />

        ";
        // line 11
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 15
        echo "
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        
    \t

    </head>
    
    <body >

\t    <header>
\t\t\t<div>
\t\t\t\t<ul class=\"navbar\">
\t\t\t\t<li><a href=\"Home\" class=\"active\">Book.Ly</a></li>
\t\t\t\t<li><a href=\"Home\">Home</a></li>
\t\t\t\t<li><a href=\"Product\">Our Products</a></li>
\t\t\t\t<li><a href=\"Testimony\">Testimonies</a></li>
\t\t\t\t<li><a href=\"ContactUs\">Contact Us</a></li>
\t\t\t\t<li><a href=\"About\">About</a></li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</header>

    \t";
        // line 37
        $this->displayBlock('body', $context, $blocks);
        // line 43
        echo "


    
 
        
\t\t

\t\t";
        // line 51
        $this->displayBlock('footer', $context, $blocks);
        // line 65
        echo "
\t\t<script src=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/bootstrap.js"), "html", null, true);
        echo "\"></script>        
\t\t";
        // line 68
        $this->displayBlock('javascripts', $context, $blocks);
        // line 69
        echo "        
    </body>
</html>
";
        
        $__internal_b111fc57d24757d0ea685053811a85c8d4429a4d01b13eb96d806812df0a83b0->leave($__internal_b111fc57d24757d0ea685053811a85c8d4429a4d01b13eb96d806812df0a83b0_prof);

        
        $__internal_07c2f36fdee3477c5dfecff70658360d916faafdebd40745636a46ec4acb9f96->leave($__internal_07c2f36fdee3477c5dfecff70658360d916faafdebd40745636a46ec4acb9f96_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_faeb257df5fda868ef594459bbeb9a52c4c7114a75e6f77cceb1f7499b1b9f88 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_faeb257df5fda868ef594459bbeb9a52c4c7114a75e6f77cceb1f7499b1b9f88->enter($__internal_faeb257df5fda868ef594459bbeb9a52c4c7114a75e6f77cceb1f7499b1b9f88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_34581325807a4faf6663bdb09c3188bcdf8ab68693be1957b644199ba75a30c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_34581325807a4faf6663bdb09c3188bcdf8ab68693be1957b644199ba75a30c9->enter($__internal_34581325807a4faf6663bdb09c3188bcdf8ab68693be1957b644199ba75a30c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Book.Ly";
        
        $__internal_34581325807a4faf6663bdb09c3188bcdf8ab68693be1957b644199ba75a30c9->leave($__internal_34581325807a4faf6663bdb09c3188bcdf8ab68693be1957b644199ba75a30c9_prof);

        
        $__internal_faeb257df5fda868ef594459bbeb9a52c4c7114a75e6f77cceb1f7499b1b9f88->leave($__internal_faeb257df5fda868ef594459bbeb9a52c4c7114a75e6f77cceb1f7499b1b9f88_prof);

    }

    // line 11
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_04de73adb4a0668f9e9f7bfd3df2657fd935a79c04aa6df5031d1e9b2c9752ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_04de73adb4a0668f9e9f7bfd3df2657fd935a79c04aa6df5031d1e9b2c9752ec->enter($__internal_04de73adb4a0668f9e9f7bfd3df2657fd935a79c04aa6df5031d1e9b2c9752ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_f27688c564a51c97c03d09a6d589fb1a0178b17e5bcfe7e370da7d8912ca9205 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f27688c564a51c97c03d09a6d589fb1a0178b17e5bcfe7e370da7d8912ca9205->enter($__internal_f27688c564a51c97c03d09a6d589fb1a0178b17e5bcfe7e370da7d8912ca9205_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 12
        echo "
    \t\t
        ";
        
        $__internal_f27688c564a51c97c03d09a6d589fb1a0178b17e5bcfe7e370da7d8912ca9205->leave($__internal_f27688c564a51c97c03d09a6d589fb1a0178b17e5bcfe7e370da7d8912ca9205_prof);

        
        $__internal_04de73adb4a0668f9e9f7bfd3df2657fd935a79c04aa6df5031d1e9b2c9752ec->leave($__internal_04de73adb4a0668f9e9f7bfd3df2657fd935a79c04aa6df5031d1e9b2c9752ec_prof);

    }

    // line 37
    public function block_body($context, array $blocks = array())
    {
        $__internal_835aea28794532b6289318ffe7e99939a6d51383b40e40f977edcdfedf40df26 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_835aea28794532b6289318ffe7e99939a6d51383b40e40f977edcdfedf40df26->enter($__internal_835aea28794532b6289318ffe7e99939a6d51383b40e40f977edcdfedf40df26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_debf884d5bb16e0f7cc11db5a387c120127133de39c27cabaf07e070b3f30e4b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_debf884d5bb16e0f7cc11db5a387c120127133de39c27cabaf07e070b3f30e4b->enter($__internal_debf884d5bb16e0f7cc11db5a387c120127133de39c27cabaf07e070b3f30e4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 38
        echo "    \t

    \t
  
        ";
        
        $__internal_debf884d5bb16e0f7cc11db5a387c120127133de39c27cabaf07e070b3f30e4b->leave($__internal_debf884d5bb16e0f7cc11db5a387c120127133de39c27cabaf07e070b3f30e4b_prof);

        
        $__internal_835aea28794532b6289318ffe7e99939a6d51383b40e40f977edcdfedf40df26->leave($__internal_835aea28794532b6289318ffe7e99939a6d51383b40e40f977edcdfedf40df26_prof);

    }

    // line 51
    public function block_footer($context, array $blocks = array())
    {
        $__internal_2cbce5a0cd8c82250352a4918499306c352e6528d18427e4e52c9b68c9e033dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2cbce5a0cd8c82250352a4918499306c352e6528d18427e4e52c9b68c9e033dc->enter($__internal_2cbce5a0cd8c82250352a4918499306c352e6528d18427e4e52c9b68c9e033dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_a7c15f555b5f0cef65700ca9a9f690cc6547d971afa0598299e2f9c2263f0189 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a7c15f555b5f0cef65700ca9a9f690cc6547d971afa0598299e2f9c2263f0189->enter($__internal_a7c15f555b5f0cef65700ca9a9f690cc6547d971afa0598299e2f9c2263f0189_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 52
        echo "\t\t    <footer class=\"footer\">
\t\t\t<div class=\"icons\">
\t\t\t<i class=\"fa fa-facebook-square \"></i>
\t\t\t<i class=\"fa fa-instagram \"></i>
\t\t\t<i class=\"fa fa-twitter\"></i>
\t\t\t<i class=\"fa fa-google-plus\"></i>
\t\t\t<i class=\"fa fa-pinterest\"></i>
\t\t\t</div>
\t\t\t<br>
\t\t\tCopyright © 2017 Book.Ly Inc. All rights reserved.

\t\t\t</footer>
        ";
        
        $__internal_a7c15f555b5f0cef65700ca9a9f690cc6547d971afa0598299e2f9c2263f0189->leave($__internal_a7c15f555b5f0cef65700ca9a9f690cc6547d971afa0598299e2f9c2263f0189_prof);

        
        $__internal_2cbce5a0cd8c82250352a4918499306c352e6528d18427e4e52c9b68c9e033dc->leave($__internal_2cbce5a0cd8c82250352a4918499306c352e6528d18427e4e52c9b68c9e033dc_prof);

    }

    // line 68
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_e884d18078e576a0d8910c28f22d04cbf8c23fa2f68a12e5f3cb9f4b5f2a7bc5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e884d18078e576a0d8910c28f22d04cbf8c23fa2f68a12e5f3cb9f4b5f2a7bc5->enter($__internal_e884d18078e576a0d8910c28f22d04cbf8c23fa2f68a12e5f3cb9f4b5f2a7bc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_5e0e89bea327def277593ebc68d7d53a5a30ebc04f7531ea74bc260337305762 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e0e89bea327def277593ebc68d7d53a5a30ebc04f7531ea74bc260337305762->enter($__internal_5e0e89bea327def277593ebc68d7d53a5a30ebc04f7531ea74bc260337305762_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_5e0e89bea327def277593ebc68d7d53a5a30ebc04f7531ea74bc260337305762->leave($__internal_5e0e89bea327def277593ebc68d7d53a5a30ebc04f7531ea74bc260337305762_prof);

        
        $__internal_e884d18078e576a0d8910c28f22d04cbf8c23fa2f68a12e5f3cb9f4b5f2a7bc5->leave($__internal_e884d18078e576a0d8910c28f22d04cbf8c23fa2f68a12e5f3cb9f4b5f2a7bc5_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 68,  191 => 52,  182 => 51,  168 => 38,  159 => 37,  147 => 12,  138 => 11,  120 => 5,  107 => 69,  105 => 68,  101 => 67,  97 => 66,  94 => 65,  92 => 51,  82 => 43,  80 => 37,  56 => 16,  53 => 15,  51 => 11,  46 => 9,  42 => 8,  36 => 5,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
  \t <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Book.Ly{% endblock %}</title>
        <link href=\"https://fonts.googleapis.com/css?family=Oswald\" rel=\"stylesheet\">
\t\t<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">
    \t<link rel=\"stylesheet\" href=\"{{asset('assets/css/bootstrap.min.css')}}\" />
\t\t<link rel=\"stylesheet\" href=\"{{asset('assets/Css/style.css')}}\" />

        {% block stylesheets %}

    \t\t
        {% endblock %}

        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        
    \t

    </head>
    
    <body >

\t    <header>
\t\t\t<div>
\t\t\t\t<ul class=\"navbar\">
\t\t\t\t<li><a href=\"Home\" class=\"active\">Book.Ly</a></li>
\t\t\t\t<li><a href=\"Home\">Home</a></li>
\t\t\t\t<li><a href=\"Product\">Our Products</a></li>
\t\t\t\t<li><a href=\"Testimony\">Testimonies</a></li>
\t\t\t\t<li><a href=\"ContactUs\">Contact Us</a></li>
\t\t\t\t<li><a href=\"About\">About</a></li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</header>

    \t{% block body %}
    \t

    \t
  
        {% endblock %}



    
 
        
\t\t

\t\t{% block footer %}
\t\t    <footer class=\"footer\">
\t\t\t<div class=\"icons\">
\t\t\t<i class=\"fa fa-facebook-square \"></i>
\t\t\t<i class=\"fa fa-instagram \"></i>
\t\t\t<i class=\"fa fa-twitter\"></i>
\t\t\t<i class=\"fa fa-google-plus\"></i>
\t\t\t<i class=\"fa fa-pinterest\"></i>
\t\t\t</div>
\t\t\t<br>
\t\t\tCopyright © 2017 Book.Ly Inc. All rights reserved.

\t\t\t</footer>
        {% endblock %}

\t\t<script src=\"{{asset('assets/js/jquery.min.js')}}\"></script>
\t\t<script src=\"{{asset('assets/js/bootstrap.js')}}\"></script>        
\t\t{% block javascripts %}{% endblock %}
        
    </body>
</html>
", "::base.html.twig", "/home/ron/bestperience/app/Resources/views/base.html.twig");
    }
}
