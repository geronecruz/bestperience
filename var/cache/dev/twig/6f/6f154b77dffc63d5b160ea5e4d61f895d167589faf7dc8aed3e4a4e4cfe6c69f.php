<?php

/* @Twig/images/icon-plus-square.svg */
class __TwigTemplate_ff57c058cda98d9aabd1793267914c8b62bcb0f7f38ea3b96bf9702002515345 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d214ee59b21fc4165816ef16980fd3ff4cd27dc0f26dbf87d752da84b432528 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d214ee59b21fc4165816ef16980fd3ff4cd27dc0f26dbf87d752da84b432528->enter($__internal_5d214ee59b21fc4165816ef16980fd3ff4cd27dc0f26dbf87d752da84b432528_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square.svg"));

        $__internal_78385da03956032843fd45fed60d71247ba807b6faaf5738ed4c8760c4f11ece = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78385da03956032843fd45fed60d71247ba807b6faaf5738ed4c8760c4f11ece->enter($__internal_78385da03956032843fd45fed60d71247ba807b6faaf5738ed4c8760c4f11ece_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960v-128q0-26-19-45t-45-19h-320v-320q0-26-19-45t-45-19h-128q-26 0-45 19t-19 45v320h-320q-26 0-45 19t-19 45v128q0 26 19 45t45 19h320v320q0 26 19 45t45 19h128q26 0 45-19t19-45v-320h320q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z\"/></svg>
";
        
        $__internal_5d214ee59b21fc4165816ef16980fd3ff4cd27dc0f26dbf87d752da84b432528->leave($__internal_5d214ee59b21fc4165816ef16980fd3ff4cd27dc0f26dbf87d752da84b432528_prof);

        
        $__internal_78385da03956032843fd45fed60d71247ba807b6faaf5738ed4c8760c4f11ece->leave($__internal_78385da03956032843fd45fed60d71247ba807b6faaf5738ed4c8760c4f11ece_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-plus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960v-128q0-26-19-45t-45-19h-320v-320q0-26-19-45t-45-19h-128q-26 0-45 19t-19 45v320h-320q-26 0-45 19t-19 45v128q0 26 19 45t45 19h320v320q0 26 19 45t45 19h128q26 0 45-19t19-45v-320h320q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z\"/></svg>
", "@Twig/images/icon-plus-square.svg", "/home/ron/bestperience/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-plus-square.svg");
    }
}
