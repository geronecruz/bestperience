<?php

/* AppBundle:Post:update.html.twig */
class __TwigTemplate_9e3feb1ca711e05d24bea46a1d59e624868e02fc612da13b3a7ae25bd7af422e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:Post:update.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1d5e2e0923c48fa6b5bfc2408ecb558a67fe53be1017c0ac430bf55336d057bd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d5e2e0923c48fa6b5bfc2408ecb558a67fe53be1017c0ac430bf55336d057bd->enter($__internal_1d5e2e0923c48fa6b5bfc2408ecb558a67fe53be1017c0ac430bf55336d057bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:update.html.twig"));

        $__internal_057ab89bae5984bf36d3538ed8abeb2c141a198d13a52f94a3a499420730ff0a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_057ab89bae5984bf36d3538ed8abeb2c141a198d13a52f94a3a499420730ff0a->enter($__internal_057ab89bae5984bf36d3538ed8abeb2c141a198d13a52f94a3a499420730ff0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:update.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1d5e2e0923c48fa6b5bfc2408ecb558a67fe53be1017c0ac430bf55336d057bd->leave($__internal_1d5e2e0923c48fa6b5bfc2408ecb558a67fe53be1017c0ac430bf55336d057bd_prof);

        
        $__internal_057ab89bae5984bf36d3538ed8abeb2c141a198d13a52f94a3a499420730ff0a->leave($__internal_057ab89bae5984bf36d3538ed8abeb2c141a198d13a52f94a3a499420730ff0a_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_b3343343ee8f11fa282c49a2ba9998a3345c75f338e8c86e866e5c6018ae63d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b3343343ee8f11fa282c49a2ba9998a3345c75f338e8c86e866e5c6018ae63d3->enter($__internal_b3343343ee8f11fa282c49a2ba9998a3345c75f338e8c86e866e5c6018ae63d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_523d5bc24e4cfec325eb5e7e4299d0412c3b882ecee70e2f4ef9a4ab3020401b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_523d5bc24e4cfec325eb5e7e4299d0412c3b882ecee70e2f4ef9a4ab3020401b->enter($__internal_523d5bc24e4cfec325eb5e7e4299d0412c3b882ecee70e2f4ef9a4ab3020401b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " Update Article ";
        
        $__internal_523d5bc24e4cfec325eb5e7e4299d0412c3b882ecee70e2f4ef9a4ab3020401b->leave($__internal_523d5bc24e4cfec325eb5e7e4299d0412c3b882ecee70e2f4ef9a4ab3020401b_prof);

        
        $__internal_b3343343ee8f11fa282c49a2ba9998a3345c75f338e8c86e866e5c6018ae63d3->leave($__internal_b3343343ee8f11fa282c49a2ba9998a3345c75f338e8c86e866e5c6018ae63d3_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_befe276e646d53310a535b5316d6544ecfff007c3cdc54aeaef9a78c1a046264 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_befe276e646d53310a535b5316d6544ecfff007c3cdc54aeaef9a78c1a046264->enter($__internal_befe276e646d53310a535b5316d6544ecfff007c3cdc54aeaef9a78c1a046264_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_587f09e2b458a3b5b0c500da2e9fa21dada89aba6551ea5da11f5771eac997b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_587f09e2b458a3b5b0c500da2e9fa21dada89aba6551ea5da11f5771eac997b1->enter($__internal_587f09e2b458a3b5b0c500da2e9fa21dada89aba6551ea5da11f5771eac997b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "

";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
";
        // line 9
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
<button type=\"submit\">Update</button>
";
        // line 11
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "




";
        
        $__internal_587f09e2b458a3b5b0c500da2e9fa21dada89aba6551ea5da11f5771eac997b1->leave($__internal_587f09e2b458a3b5b0c500da2e9fa21dada89aba6551ea5da11f5771eac997b1_prof);

        
        $__internal_befe276e646d53310a535b5316d6544ecfff007c3cdc54aeaef9a78c1a046264->leave($__internal_befe276e646d53310a535b5316d6544ecfff007c3cdc54aeaef9a78c1a046264_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Post:update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 11,  76 => 9,  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}

{% block title%} Update Article {% endblock %}

{% block body %}


{{ form_start(form) }}
{{ form_widget(form) }}
<button type=\"submit\">Update</button>
{{ form_end(form) }}




{% endblock %}", "AppBundle:Post:update.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/Post/update.html.twig");
    }
}
