<?php

/* form_table_layout.html.twig */
class __TwigTemplate_017b2828e5fe2c06c449846c961b8579e5b17ff47a35f2480cceb38a9c6bf36a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("form_div_layout.html.twig", "form_table_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."form_div_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_row' => array($this, 'block_form_row'),
                'button_row' => array($this, 'block_button_row'),
                'hidden_row' => array($this, 'block_hidden_row'),
                'form_widget_compound' => array($this, 'block_form_widget_compound'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d6f469bd03025986837fc4f9733e06ca70b82026d31fd5cf593e25341157ecdb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d6f469bd03025986837fc4f9733e06ca70b82026d31fd5cf593e25341157ecdb->enter($__internal_d6f469bd03025986837fc4f9733e06ca70b82026d31fd5cf593e25341157ecdb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form_table_layout.html.twig"));

        $__internal_8bf804832625274319884ed38001c66c35e31dc6738bc4125ecdb198c597f119 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8bf804832625274319884ed38001c66c35e31dc6738bc4125ecdb198c597f119->enter($__internal_8bf804832625274319884ed38001c66c35e31dc6738bc4125ecdb198c597f119_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form_table_layout.html.twig"));

        // line 3
        $this->displayBlock('form_row', $context, $blocks);
        // line 15
        $this->displayBlock('button_row', $context, $blocks);
        // line 24
        $this->displayBlock('hidden_row', $context, $blocks);
        // line 32
        $this->displayBlock('form_widget_compound', $context, $blocks);
        
        $__internal_d6f469bd03025986837fc4f9733e06ca70b82026d31fd5cf593e25341157ecdb->leave($__internal_d6f469bd03025986837fc4f9733e06ca70b82026d31fd5cf593e25341157ecdb_prof);

        
        $__internal_8bf804832625274319884ed38001c66c35e31dc6738bc4125ecdb198c597f119->leave($__internal_8bf804832625274319884ed38001c66c35e31dc6738bc4125ecdb198c597f119_prof);

    }

    // line 3
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_0c02921d96f5f93bf3ffd8a3ee89d4cba32602245369e96f688af724cba021e7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c02921d96f5f93bf3ffd8a3ee89d4cba32602245369e96f688af724cba021e7->enter($__internal_0c02921d96f5f93bf3ffd8a3ee89d4cba32602245369e96f688af724cba021e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_f814c97e710168bfab2e65dd9c776dd8abe55d7520ac3a6bc562a5d9263618f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f814c97e710168bfab2e65dd9c776dd8abe55d7520ac3a6bc562a5d9263618f6->enter($__internal_f814c97e710168bfab2e65dd9c776dd8abe55d7520ac3a6bc562a5d9263618f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 4
        echo "<tr>
        <td>";
        // line 6
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 6, $this->getSourceContext()); })()), 'label');
        // line 7
        echo "</td>
        <td>";
        // line 9
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 9, $this->getSourceContext()); })()), 'errors');
        // line 10
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 10, $this->getSourceContext()); })()), 'widget');
        // line 11
        echo "</td>
    </tr>";
        
        $__internal_f814c97e710168bfab2e65dd9c776dd8abe55d7520ac3a6bc562a5d9263618f6->leave($__internal_f814c97e710168bfab2e65dd9c776dd8abe55d7520ac3a6bc562a5d9263618f6_prof);

        
        $__internal_0c02921d96f5f93bf3ffd8a3ee89d4cba32602245369e96f688af724cba021e7->leave($__internal_0c02921d96f5f93bf3ffd8a3ee89d4cba32602245369e96f688af724cba021e7_prof);

    }

    // line 15
    public function block_button_row($context, array $blocks = array())
    {
        $__internal_a06a95bb735c340b1d044dbf73e71f942a65209a40231dc59c38bb592dbfa7e0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a06a95bb735c340b1d044dbf73e71f942a65209a40231dc59c38bb592dbfa7e0->enter($__internal_a06a95bb735c340b1d044dbf73e71f942a65209a40231dc59c38bb592dbfa7e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        $__internal_3f94dedbc96ea4768b4ea570b37e97968045f66c499f9de1deec2448fdbabe51 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f94dedbc96ea4768b4ea570b37e97968045f66c499f9de1deec2448fdbabe51->enter($__internal_3f94dedbc96ea4768b4ea570b37e97968045f66c499f9de1deec2448fdbabe51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        // line 16
        echo "<tr>
        <td></td>
        <td>";
        // line 19
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 19, $this->getSourceContext()); })()), 'widget');
        // line 20
        echo "</td>
    </tr>";
        
        $__internal_3f94dedbc96ea4768b4ea570b37e97968045f66c499f9de1deec2448fdbabe51->leave($__internal_3f94dedbc96ea4768b4ea570b37e97968045f66c499f9de1deec2448fdbabe51_prof);

        
        $__internal_a06a95bb735c340b1d044dbf73e71f942a65209a40231dc59c38bb592dbfa7e0->leave($__internal_a06a95bb735c340b1d044dbf73e71f942a65209a40231dc59c38bb592dbfa7e0_prof);

    }

    // line 24
    public function block_hidden_row($context, array $blocks = array())
    {
        $__internal_1ea266a82919f7aee9468c886d1178871016c75339be68f86f2d513282c22e26 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ea266a82919f7aee9468c886d1178871016c75339be68f86f2d513282c22e26->enter($__internal_1ea266a82919f7aee9468c886d1178871016c75339be68f86f2d513282c22e26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_row"));

        $__internal_9f0493645b31d9a855c7c0e6db2f3eb3dd212ed1bfbd56fc584f0690ece0fabd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f0493645b31d9a855c7c0e6db2f3eb3dd212ed1bfbd56fc584f0690ece0fabd->enter($__internal_9f0493645b31d9a855c7c0e6db2f3eb3dd212ed1bfbd56fc584f0690ece0fabd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_row"));

        // line 25
        echo "<tr style=\"display: none\">
        <td colspan=\"2\">";
        // line 27
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 27, $this->getSourceContext()); })()), 'widget');
        // line 28
        echo "</td>
    </tr>";
        
        $__internal_9f0493645b31d9a855c7c0e6db2f3eb3dd212ed1bfbd56fc584f0690ece0fabd->leave($__internal_9f0493645b31d9a855c7c0e6db2f3eb3dd212ed1bfbd56fc584f0690ece0fabd_prof);

        
        $__internal_1ea266a82919f7aee9468c886d1178871016c75339be68f86f2d513282c22e26->leave($__internal_1ea266a82919f7aee9468c886d1178871016c75339be68f86f2d513282c22e26_prof);

    }

    // line 32
    public function block_form_widget_compound($context, array $blocks = array())
    {
        $__internal_05895ff041331654e528436f5512cc03a003496ef391250e2a4e1cc69ef56878 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_05895ff041331654e528436f5512cc03a003496ef391250e2a4e1cc69ef56878->enter($__internal_05895ff041331654e528436f5512cc03a003496ef391250e2a4e1cc69ef56878_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_compound"));

        $__internal_7f68cded720a0a6308167059ebe902286faf987d165860c721a7f617905ebf4d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7f68cded720a0a6308167059ebe902286faf987d165860c721a7f617905ebf4d->enter($__internal_7f68cded720a0a6308167059ebe902286faf987d165860c721a7f617905ebf4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_compound"));

        // line 33
        echo "<table ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">";
        // line 34
        if ((twig_test_empty(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 34, $this->getSourceContext()); })()), "parent", array())) && (twig_length_filter($this->env, (isset($context["errors"]) || array_key_exists("errors", $context) ? $context["errors"] : (function () { throw new Twig_Error_Runtime('Variable "errors" does not exist.', 34, $this->getSourceContext()); })())) > 0))) {
            // line 35
            echo "<tr>
            <td colspan=\"2\">";
            // line 37
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 37, $this->getSourceContext()); })()), 'errors');
            // line 38
            echo "</td>
        </tr>";
        }
        // line 41
        $this->displayBlock("form_rows", $context, $blocks);
        // line 42
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 42, $this->getSourceContext()); })()), 'rest');
        // line 43
        echo "</table>";
        
        $__internal_7f68cded720a0a6308167059ebe902286faf987d165860c721a7f617905ebf4d->leave($__internal_7f68cded720a0a6308167059ebe902286faf987d165860c721a7f617905ebf4d_prof);

        
        $__internal_05895ff041331654e528436f5512cc03a003496ef391250e2a4e1cc69ef56878->leave($__internal_05895ff041331654e528436f5512cc03a003496ef391250e2a4e1cc69ef56878_prof);

    }

    public function getTemplateName()
    {
        return "form_table_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  168 => 43,  166 => 42,  164 => 41,  160 => 38,  158 => 37,  155 => 35,  153 => 34,  149 => 33,  140 => 32,  129 => 28,  127 => 27,  124 => 25,  115 => 24,  104 => 20,  102 => 19,  98 => 16,  89 => 15,  78 => 11,  76 => 10,  74 => 9,  71 => 7,  69 => 6,  66 => 4,  57 => 3,  47 => 32,  45 => 24,  43 => 15,  41 => 3,  14 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"form_div_layout.html.twig\" %}

{%- block form_row -%}
    <tr>
        <td>
            {{- form_label(form) -}}
        </td>
        <td>
            {{- form_errors(form) -}}
            {{- form_widget(form) -}}
        </td>
    </tr>
{%- endblock form_row -%}

{%- block button_row -%}
    <tr>
        <td></td>
        <td>
            {{- form_widget(form) -}}
        </td>
    </tr>
{%- endblock button_row -%}

{%- block hidden_row -%}
    <tr style=\"display: none\">
        <td colspan=\"2\">
            {{- form_widget(form) -}}
        </td>
    </tr>
{%- endblock hidden_row -%}

{%- block form_widget_compound -%}
    <table {{ block('widget_container_attributes') }}>
        {%- if form.parent is empty and errors|length > 0 -%}
        <tr>
            <td colspan=\"2\">
                {{- form_errors(form) -}}
            </td>
        </tr>
        {%- endif -%}
        {{- block('form_rows') -}}
        {{- form_rest(form) -}}
    </table>
{%- endblock form_widget_compound -%}
", "form_table_layout.html.twig", "/home/ron/bestperience/vendor/symfony/symfony/src/Symfony/Bridge/Twig/Resources/views/Form/form_table_layout.html.twig");
    }
}
