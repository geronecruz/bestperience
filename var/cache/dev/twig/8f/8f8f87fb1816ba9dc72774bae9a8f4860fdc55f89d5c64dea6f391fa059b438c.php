<?php

/* @WebProfiler/Profiler/open.html.twig */
class __TwigTemplate_59376f8f17caee83a6c462eebaa666862a05768aae5e8a1e520390be4274954d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "@WebProfiler/Profiler/open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c6027765b61e2f2533dbfc6d39ea0c0c38204a8eb16e28f99bf18174ef6080fa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6027765b61e2f2533dbfc6d39ea0c0c38204a8eb16e28f99bf18174ef6080fa->enter($__internal_c6027765b61e2f2533dbfc6d39ea0c0c38204a8eb16e28f99bf18174ef6080fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $__internal_93b6fe069430d9242a65b40ec8cd9b3b7f173aa32e4d5474aa5c9fa303603c0d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93b6fe069430d9242a65b40ec8cd9b3b7f173aa32e4d5474aa5c9fa303603c0d->enter($__internal_93b6fe069430d9242a65b40ec8cd9b3b7f173aa32e4d5474aa5c9fa303603c0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c6027765b61e2f2533dbfc6d39ea0c0c38204a8eb16e28f99bf18174ef6080fa->leave($__internal_c6027765b61e2f2533dbfc6d39ea0c0c38204a8eb16e28f99bf18174ef6080fa_prof);

        
        $__internal_93b6fe069430d9242a65b40ec8cd9b3b7f173aa32e4d5474aa5c9fa303603c0d->leave($__internal_93b6fe069430d9242a65b40ec8cd9b3b7f173aa32e4d5474aa5c9fa303603c0d_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_34989ba8129cd442441cf740641af434590bb89de363577d4c9716671b53bc0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34989ba8129cd442441cf740641af434590bb89de363577d4c9716671b53bc0d->enter($__internal_34989ba8129cd442441cf740641af434590bb89de363577d4c9716671b53bc0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_4fbdae12fb44fd10858ba658eb5ce8c69945a868f314794ac3a00ba41fef2271 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4fbdae12fb44fd10858ba658eb5ce8c69945a868f314794ac3a00ba41fef2271->enter($__internal_4fbdae12fb44fd10858ba658eb5ce8c69945a868f314794ac3a00ba41fef2271_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_4fbdae12fb44fd10858ba658eb5ce8c69945a868f314794ac3a00ba41fef2271->leave($__internal_4fbdae12fb44fd10858ba658eb5ce8c69945a868f314794ac3a00ba41fef2271_prof);

        
        $__internal_34989ba8129cd442441cf740641af434590bb89de363577d4c9716671b53bc0d->leave($__internal_34989ba8129cd442441cf740641af434590bb89de363577d4c9716671b53bc0d_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_16e765c187d0d702c9361492fb8ef3443689b28715bf5257a16117320d3ed553 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_16e765c187d0d702c9361492fb8ef3443689b28715bf5257a16117320d3ed553->enter($__internal_16e765c187d0d702c9361492fb8ef3443689b28715bf5257a16117320d3ed553_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e87fe2e4e287d470b73279248e26df6540acb09631819bf7107527f66ff72fc7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e87fe2e4e287d470b73279248e26df6540acb09631819bf7107527f66ff72fc7->enter($__internal_e87fe2e4e287d470b73279248e26df6540acb09631819bf7107527f66ff72fc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["file"]) || array_key_exists("file", $context) ? $context["file"] : (function () { throw new Twig_Error_Runtime('Variable "file" does not exist.', 11, $this->getSourceContext()); })()), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, (isset($context["line"]) || array_key_exists("line", $context) ? $context["line"] : (function () { throw new Twig_Error_Runtime('Variable "line" does not exist.', 11, $this->getSourceContext()); })()), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt((isset($context["filename"]) || array_key_exists("filename", $context) ? $context["filename"] : (function () { throw new Twig_Error_Runtime('Variable "filename" does not exist.', 15, $this->getSourceContext()); })()), (isset($context["line"]) || array_key_exists("line", $context) ? $context["line"] : (function () { throw new Twig_Error_Runtime('Variable "line" does not exist.', 15, $this->getSourceContext()); })()),  -1);
        echo "
</div>
";
        
        $__internal_e87fe2e4e287d470b73279248e26df6540acb09631819bf7107527f66ff72fc7->leave($__internal_e87fe2e4e287d470b73279248e26df6540acb09631819bf7107527f66ff72fc7_prof);

        
        $__internal_16e765c187d0d702c9361492fb8ef3443689b28715bf5257a16117320d3ed553->leave($__internal_16e765c187d0d702c9361492fb8ef3443689b28715bf5257a16117320d3ed553_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "@WebProfiler/Profiler/open.html.twig", "/home/ron/bestperience/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
