<?php

/* AppBundle:Post:add.html.twig */
class __TwigTemplate_c3dcdc6f6e6fb2bbc08ac4e509cf52ad06fb54dbcf3819edef0b2f50887fcaa0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:Post:add.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ecb76ed5e88713118333fcde4535407d5963995052864686ecafe3a8f99ffbf6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ecb76ed5e88713118333fcde4535407d5963995052864686ecafe3a8f99ffbf6->enter($__internal_ecb76ed5e88713118333fcde4535407d5963995052864686ecafe3a8f99ffbf6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:add.html.twig"));

        $__internal_f985ad4f59132de278581f40231982138344adf271a28d68e0403d7be522af37 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f985ad4f59132de278581f40231982138344adf271a28d68e0403d7be522af37->enter($__internal_f985ad4f59132de278581f40231982138344adf271a28d68e0403d7be522af37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:add.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ecb76ed5e88713118333fcde4535407d5963995052864686ecafe3a8f99ffbf6->leave($__internal_ecb76ed5e88713118333fcde4535407d5963995052864686ecafe3a8f99ffbf6_prof);

        
        $__internal_f985ad4f59132de278581f40231982138344adf271a28d68e0403d7be522af37->leave($__internal_f985ad4f59132de278581f40231982138344adf271a28d68e0403d7be522af37_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_b19bd7d51d7f793f0466e7e5a433b652e20eb2e45e1e9b17135a18a4a4d291eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b19bd7d51d7f793f0466e7e5a433b652e20eb2e45e1e9b17135a18a4a4d291eb->enter($__internal_b19bd7d51d7f793f0466e7e5a433b652e20eb2e45e1e9b17135a18a4a4d291eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_b733c1bb6ab9c016696a20f5fd42df27bbcccf3864f1912c67c50e942db4ef8b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b733c1bb6ab9c016696a20f5fd42df27bbcccf3864f1912c67c50e942db4ef8b->enter($__internal_b733c1bb6ab9c016696a20f5fd42df27bbcccf3864f1912c67c50e942db4ef8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " Add Article ";
        
        $__internal_b733c1bb6ab9c016696a20f5fd42df27bbcccf3864f1912c67c50e942db4ef8b->leave($__internal_b733c1bb6ab9c016696a20f5fd42df27bbcccf3864f1912c67c50e942db4ef8b_prof);

        
        $__internal_b19bd7d51d7f793f0466e7e5a433b652e20eb2e45e1e9b17135a18a4a4d291eb->leave($__internal_b19bd7d51d7f793f0466e7e5a433b652e20eb2e45e1e9b17135a18a4a4d291eb_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_5743e9cce715065f3a5a2ebc332f8ab1e1f2221b4ac41371059ca609dc3e8200 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5743e9cce715065f3a5a2ebc332f8ab1e1f2221b4ac41371059ca609dc3e8200->enter($__internal_5743e9cce715065f3a5a2ebc332f8ab1e1f2221b4ac41371059ca609dc3e8200_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d2c53bbde89955275dc972768728feb24afe2cf0fc889f8476e6a6a039ded88e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d2c53bbde89955275dc972768728feb24afe2cf0fc889f8476e6a6a039ded88e->enter($__internal_d2c53bbde89955275dc972768728feb24afe2cf0fc889f8476e6a6a039ded88e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "

";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
";
        // line 9
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
<button type=\"submit\">Add</button>
";
        // line 11
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "




";
        
        $__internal_d2c53bbde89955275dc972768728feb24afe2cf0fc889f8476e6a6a039ded88e->leave($__internal_d2c53bbde89955275dc972768728feb24afe2cf0fc889f8476e6a6a039ded88e_prof);

        
        $__internal_5743e9cce715065f3a5a2ebc332f8ab1e1f2221b4ac41371059ca609dc3e8200->leave($__internal_5743e9cce715065f3a5a2ebc332f8ab1e1f2221b4ac41371059ca609dc3e8200_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Post:add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 11,  76 => 9,  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}

{% block title%} Add Article {% endblock %}

{% block body %}


{{ form_start(form) }}
{{ form_widget(form) }}
<button type=\"submit\">Add</button>
{{ form_end(form) }}




{% endblock %}", "AppBundle:Post:add.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/Post/add.html.twig");
    }
}
