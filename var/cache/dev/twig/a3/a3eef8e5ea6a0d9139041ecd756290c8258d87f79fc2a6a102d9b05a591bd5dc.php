<?php

/* AppBundle:Post:updatePost.html.twig */
class __TwigTemplate_58d64c83c44f96fafa2ff779ea5c5dd2d71f8258e14a43137de6bf0ba815808b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::loggedin.html.twig", "AppBundle:Post:updatePost.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::loggedin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_39effc7c143efb11f8ec9d258510eae5d7b052b6bcee6ba2f896bf661081efe8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_39effc7c143efb11f8ec9d258510eae5d7b052b6bcee6ba2f896bf661081efe8->enter($__internal_39effc7c143efb11f8ec9d258510eae5d7b052b6bcee6ba2f896bf661081efe8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:updatePost.html.twig"));

        $__internal_4d8f9119fd75363ff7115254da1e1004dbbefdaac3c50fe88373cc34320194c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d8f9119fd75363ff7115254da1e1004dbbefdaac3c50fe88373cc34320194c9->enter($__internal_4d8f9119fd75363ff7115254da1e1004dbbefdaac3c50fe88373cc34320194c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:updatePost.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_39effc7c143efb11f8ec9d258510eae5d7b052b6bcee6ba2f896bf661081efe8->leave($__internal_39effc7c143efb11f8ec9d258510eae5d7b052b6bcee6ba2f896bf661081efe8_prof);

        
        $__internal_4d8f9119fd75363ff7115254da1e1004dbbefdaac3c50fe88373cc34320194c9->leave($__internal_4d8f9119fd75363ff7115254da1e1004dbbefdaac3c50fe88373cc34320194c9_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_366811c99bd6d3ae09118070f87b62c778581d6a573aa3cf42f08700a89a59ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_366811c99bd6d3ae09118070f87b62c778581d6a573aa3cf42f08700a89a59ec->enter($__internal_366811c99bd6d3ae09118070f87b62c778581d6a573aa3cf42f08700a89a59ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_2117dc9f05f8c5f71fa84095f3e6fa8f8ee07b33bb0b7d3e67dac4f73f3ff557 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2117dc9f05f8c5f71fa84095f3e6fa8f8ee07b33bb0b7d3e67dac4f73f3ff557->enter($__internal_2117dc9f05f8c5f71fa84095f3e6fa8f8ee07b33bb0b7d3e67dac4f73f3ff557_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " Update Article ";
        
        $__internal_2117dc9f05f8c5f71fa84095f3e6fa8f8ee07b33bb0b7d3e67dac4f73f3ff557->leave($__internal_2117dc9f05f8c5f71fa84095f3e6fa8f8ee07b33bb0b7d3e67dac4f73f3ff557_prof);

        
        $__internal_366811c99bd6d3ae09118070f87b62c778581d6a573aa3cf42f08700a89a59ec->leave($__internal_366811c99bd6d3ae09118070f87b62c778581d6a573aa3cf42f08700a89a59ec_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_d3e9fd17ca46074c9e3054ff39f4c5d7f335543dcd321bcdc088df92ca2dcf4a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d3e9fd17ca46074c9e3054ff39f4c5d7f335543dcd321bcdc088df92ca2dcf4a->enter($__internal_d3e9fd17ca46074c9e3054ff39f4c5d7f335543dcd321bcdc088df92ca2dcf4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_04a6869510042f79e2a4471366daddc5c5d45b6d0d551974faa33da14518c219 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04a6869510042f79e2a4471366daddc5c5d45b6d0d551974faa33da14518c219->enter($__internal_04a6869510042f79e2a4471366daddc5c5d45b6d0d551974faa33da14518c219_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "

";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 8, $this->getSourceContext()); })()), 'form_start');
        echo "
    ";
        // line 13
        echo "
    <center>
    <img src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("uploads/image/"), "html", null, true);
        echo twig_escape_filter($this->env, (isset($context["postimg"]) || array_key_exists("postimg", $context) ? $context["postimg"] : (function () { throw new Twig_Error_Runtime('Variable "postimg" does not exist.', 15, $this->getSourceContext()); })()), "html", null, true);
        echo "\" style=\"max-height:500px; max-width:500px;\"/>
    </img>
    </center>
    <div class=\"row \">
        <div class = \"\" style=\"margin-left: 300px; margin-right:300px; margin-top:50; background-color: #19334d; padding: 10px\">
    ";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 20, $this->getSourceContext()); })()), "img", array()), 'row', array("attr" => array("requirsed" => "false")));
        echo "
    ";
        // line 21
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 21, $this->getSourceContext()); })()), "title", array()), 'row', array("attr" => array("requidred" => "false")));
        echo "
    
    
    ";
        // line 24
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 24, $this->getSourceContext()); })()), "content", array()), 'row', array("attr" => array("requdired" => "false")));
        echo "
    
    <button class=\"btn btn-primary\" type=\"submit\">Update</button>

    </div>
    </div>
";
        // line 30
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 30, $this->getSourceContext()); })()), 'form_end');
        echo "





";
        
        $__internal_04a6869510042f79e2a4471366daddc5c5d45b6d0d551974faa33da14518c219->leave($__internal_04a6869510042f79e2a4471366daddc5c5d45b6d0d551974faa33da14518c219_prof);

        
        $__internal_d3e9fd17ca46074c9e3054ff39f4c5d7f335543dcd321bcdc088df92ca2dcf4a->leave($__internal_d3e9fd17ca46074c9e3054ff39f4c5d7f335543dcd321bcdc088df92ca2dcf4a_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Post:updatePost.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 30,  99 => 24,  93 => 21,  89 => 20,  80 => 15,  76 => 13,  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::loggedin.html.twig\" %}

{% block title%} Update Article {% endblock %}

{% block body %}


{{ form_start(form) }}
    {#{{ form_row(form.username) }}
    {{ form_row(form.plainPassword.first) }}
    {{ form_row(form.plainPassword.second) }}
    {{ form_row(form.email) }}#}

    <center>
    <img src=\"{{asset('uploads/image/')}}{{postimg}}\" style=\"max-height:500px; max-width:500px;\"/>
    </img>
    </center>
    <div class=\"row \">
        <div class = \"\" style=\"margin-left: 300px; margin-right:300px; margin-top:50; background-color: #19334d; padding: 10px\">
    {{ form_row(form.img,  {'attr': {'requirsed': 'false'}}) }}
    {{ form_row(form.title,  {'attr': {'requidred': 'false'}}) }}
    
    
    {{ form_row(form.content,  {'attr': {'requdired': 'false'}}) }}
    
    <button class=\"btn btn-primary\" type=\"submit\">Update</button>

    </div>
    </div>
{{ form_end(form) }}





{% endblock %}", "AppBundle:Post:updatePost.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/Post/updatePost.html.twig");
    }
}
