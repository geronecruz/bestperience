<?php

/* AppBundle:User:listUser.html.twig */
class __TwigTemplate_7a91b47e9c723e33b420ed1bbe9845417400b2ab62415a28d7379e5b1a09b8a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("::loggedin.html.twig", "AppBundle:User:listUser.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::loggedin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0a7b069543f8b49129bf114d1b48930e39530bb7c008c7ebc42eabd29499aa46 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a7b069543f8b49129bf114d1b48930e39530bb7c008c7ebc42eabd29499aa46->enter($__internal_0a7b069543f8b49129bf114d1b48930e39530bb7c008c7ebc42eabd29499aa46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:listUser.html.twig"));

        $__internal_1525e6f64dfcba141d53bf5a5fb6fae77b52664a9d1bc30081c7c351ea3edc83 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1525e6f64dfcba141d53bf5a5fb6fae77b52664a9d1bc30081c7c351ea3edc83->enter($__internal_1525e6f64dfcba141d53bf5a5fb6fae77b52664a9d1bc30081c7c351ea3edc83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:listUser.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0a7b069543f8b49129bf114d1b48930e39530bb7c008c7ebc42eabd29499aa46->leave($__internal_0a7b069543f8b49129bf114d1b48930e39530bb7c008c7ebc42eabd29499aa46_prof);

        
        $__internal_1525e6f64dfcba141d53bf5a5fb6fae77b52664a9d1bc30081c7c351ea3edc83->leave($__internal_1525e6f64dfcba141d53bf5a5fb6fae77b52664a9d1bc30081c7c351ea3edc83_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_3448e675495e590602a37b59999fd5f0fb448e53ef7998d17020857fea115090 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3448e675495e590602a37b59999fd5f0fb448e53ef7998d17020857fea115090->enter($__internal_3448e675495e590602a37b59999fd5f0fb448e53ef7998d17020857fea115090_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_5b80c86c932ccef7e8fe1eb80e312e5015ca4eb192b60b0a44a65930757123fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b80c86c932ccef7e8fe1eb80e312e5015ca4eb192b60b0a44a65930757123fe->enter($__internal_5b80c86c932ccef7e8fe1eb80e312e5015ca4eb192b60b0a44a65930757123fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Articles";
        
        $__internal_5b80c86c932ccef7e8fe1eb80e312e5015ca4eb192b60b0a44a65930757123fe->leave($__internal_5b80c86c932ccef7e8fe1eb80e312e5015ca4eb192b60b0a44a65930757123fe_prof);

        
        $__internal_3448e675495e590602a37b59999fd5f0fb448e53ef7998d17020857fea115090->leave($__internal_3448e675495e590602a37b59999fd5f0fb448e53ef7998d17020857fea115090_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_c6838e80b92bdf6dac52c7a180aef547caae4f30e8f256c28ba314225599b4ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6838e80b92bdf6dac52c7a180aef547caae4f30e8f256c28ba314225599b4ef->enter($__internal_c6838e80b92bdf6dac52c7a180aef547caae4f30e8f256c28ba314225599b4ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e8676dc195d397d862ad69ee5a5da0e15d0c02bca0abf66ac790e9c2fff69910 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e8676dc195d397d862ad69ee5a5da0e15d0c02bca0abf66ac790e9c2fff69910->enter($__internal_e8676dc195d397d862ad69ee5a5da0e15d0c02bca0abf66ac790e9c2fff69910_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "<h1>Welcome to the Post:list page</h1>

    <a href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_changePassword", array("id" => (isset($context["userid"]) || array_key_exists("userid", $context) ? $context["userid"] : (function () { throw new Twig_Error_Runtime('Variable "userid" does not exist.', 9, $this->getSourceContext()); })()))), "html", null, true);
        echo "\">Change Password</a>
    <table class=\"table table-hover\">
        <thead>
            <tr>
                <th width=\"20%\">Image</th>
                <th width=\"20%\">username</th>
                <th  width=\"20%\">Name</th>
                <th  width=\"20%\">Email</th>
                <th  width=\"20%\">Action</th>
                ";
        // line 21
        echo "            </tr>
        </thead>
        <tbody>
            ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 24, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 25
            echo "                <tr>
                    <td><center><img src=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("uploads/image/"), "html", null, true);
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "img", array()), "html", null, true);
            echo "\" style=\"max-height:100px; max-width:100px;\"/></center> </td>
                    <td>";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "username", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "name", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 29
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "email", array()), "html", null, true);
            echo "</td>
                    
                    <td><a href=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_update_userProfile", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["user"], "id", array()))), "html", null, true);
            echo "\">update</a> <a href=\"\">remove</a></td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "        </tbody>
    </table>

            
    
";
        
        $__internal_e8676dc195d397d862ad69ee5a5da0e15d0c02bca0abf66ac790e9c2fff69910->leave($__internal_e8676dc195d397d862ad69ee5a5da0e15d0c02bca0abf66ac790e9c2fff69910_prof);

        
        $__internal_c6838e80b92bdf6dac52c7a180aef547caae4f30e8f256c28ba314225599b4ef->leave($__internal_c6838e80b92bdf6dac52c7a180aef547caae4f30e8f256c28ba314225599b4ef_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:User:listUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 34,  114 => 31,  109 => 29,  105 => 28,  101 => 27,  96 => 26,  93 => 25,  89 => 24,  84 => 21,  72 => 9,  68 => 7,  59 => 6,  41 => 4,  11 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("
{% extends \"::loggedin.html.twig\" %}

{% block title %}Articles{% endblock %}

{% block body %}
<h1>Welcome to the Post:list page</h1>

    <a href=\"{{path('admin_changePassword',{id:userid})}}\">Change Password</a>
    <table class=\"table table-hover\">
        <thead>
            <tr>
                <th width=\"20%\">Image</th>
                <th width=\"20%\">username</th>
                <th  width=\"20%\">Name</th>
                <th  width=\"20%\">Email</th>
                <th  width=\"20%\">Action</th>
                {#<th>Address</th>
                <th>Birthday</th>
                 <th>Description</th>#}
            </tr>
        </thead>
        <tbody>
            {%for user in users%}
                <tr>
                    <td><center><img src=\"{{asset('uploads/image/')}}{{user.img}}\" style=\"max-height:100px; max-width:100px;\"/></center> </td>
                    <td>{{user.username}}</td>
                    <td>{{user.name}}</td>
                    <td>{{user.email}}</td>
                    
                    <td><a href=\"{{path('admin_update_userProfile',{id:user.id})}}\">update</a> <a href=\"\">remove</a></td>
                </tr>
            {%endfor%}
        </tbody>
    </table>

            
    
{% endblock %}
", "AppBundle:User:listUser.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/User/listUser.html.twig");
    }
}
