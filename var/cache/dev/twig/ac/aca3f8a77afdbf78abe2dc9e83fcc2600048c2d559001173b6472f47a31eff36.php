<?php

/* AppBundle:Post:update.html.twig */
class __TwigTemplate_4e6eab24f3760448d351ed76278945960c9502bd73676e31d72d02f4a0137d5e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:Post:update.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9b403713bef0be64491b5750d580514f4b98ed350b1c1d4085614c7720447c5e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9b403713bef0be64491b5750d580514f4b98ed350b1c1d4085614c7720447c5e->enter($__internal_9b403713bef0be64491b5750d580514f4b98ed350b1c1d4085614c7720447c5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:update.html.twig"));

        $__internal_0c0c9a38d2a94e977db29899b8b79210af32225578d6e92a8221f78851c4d183 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0c0c9a38d2a94e977db29899b8b79210af32225578d6e92a8221f78851c4d183->enter($__internal_0c0c9a38d2a94e977db29899b8b79210af32225578d6e92a8221f78851c4d183_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:update.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9b403713bef0be64491b5750d580514f4b98ed350b1c1d4085614c7720447c5e->leave($__internal_9b403713bef0be64491b5750d580514f4b98ed350b1c1d4085614c7720447c5e_prof);

        
        $__internal_0c0c9a38d2a94e977db29899b8b79210af32225578d6e92a8221f78851c4d183->leave($__internal_0c0c9a38d2a94e977db29899b8b79210af32225578d6e92a8221f78851c4d183_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_ed903de0675ee39d9537966ce6712faf1a0e8d0b1467e92901a2958a76f316f0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed903de0675ee39d9537966ce6712faf1a0e8d0b1467e92901a2958a76f316f0->enter($__internal_ed903de0675ee39d9537966ce6712faf1a0e8d0b1467e92901a2958a76f316f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_28199231474273bb30fda1a0c812f107093cdeed7e00ba6e0f9274d1dc134af2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28199231474273bb30fda1a0c812f107093cdeed7e00ba6e0f9274d1dc134af2->enter($__internal_28199231474273bb30fda1a0c812f107093cdeed7e00ba6e0f9274d1dc134af2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " Update Article ";
        
        $__internal_28199231474273bb30fda1a0c812f107093cdeed7e00ba6e0f9274d1dc134af2->leave($__internal_28199231474273bb30fda1a0c812f107093cdeed7e00ba6e0f9274d1dc134af2_prof);

        
        $__internal_ed903de0675ee39d9537966ce6712faf1a0e8d0b1467e92901a2958a76f316f0->leave($__internal_ed903de0675ee39d9537966ce6712faf1a0e8d0b1467e92901a2958a76f316f0_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_4c0976b8d8e74b90cd6a3344a6b31c2ddb87d9be58a886985b1a4cc220097a41 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4c0976b8d8e74b90cd6a3344a6b31c2ddb87d9be58a886985b1a4cc220097a41->enter($__internal_4c0976b8d8e74b90cd6a3344a6b31c2ddb87d9be58a886985b1a4cc220097a41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_664ab9a3036f963ac31ec7379a4e03eade1f59f3c3bc9b1fcf81c5873b001795 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_664ab9a3036f963ac31ec7379a4e03eade1f59f3c3bc9b1fcf81c5873b001795->enter($__internal_664ab9a3036f963ac31ec7379a4e03eade1f59f3c3bc9b1fcf81c5873b001795_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "

";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 8, $this->getSourceContext()); })()), 'form_start');
        echo "
    ";
        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 13, $this->getSourceContext()); })()), "title", array()), 'row', array("attr" => array("requidred" => "false")));
        echo "
    <img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("uploads/image/"), "html", null, true);
        echo twig_escape_filter($this->env, (isset($context["postimg"]) || array_key_exists("postimg", $context) ? $context["postimg"] : (function () { throw new Twig_Error_Runtime('Variable "postimg" does not exist.', 14, $this->getSourceContext()); })()), "html", null, true);
        echo "\" style=\"max-height:100px; max-width:100px;\"/>
    ";
        // line 15
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 15, $this->getSourceContext()); })()), "img", array()), 'row', array("attr" => array("requirsed" => "false")));
        echo "
    ";
        // line 16
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 16, $this->getSourceContext()); })()), "content", array()), 'row', array("attr" => array("requdired" => "false")));
        echo "
    
    <button type=\"submit\">update!</button>
";
        // line 19
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 19, $this->getSourceContext()); })()), 'form_end');
        echo "





";
        
        $__internal_664ab9a3036f963ac31ec7379a4e03eade1f59f3c3bc9b1fcf81c5873b001795->leave($__internal_664ab9a3036f963ac31ec7379a4e03eade1f59f3c3bc9b1fcf81c5873b001795_prof);

        
        $__internal_4c0976b8d8e74b90cd6a3344a6b31c2ddb87d9be58a886985b1a4cc220097a41->leave($__internal_4c0976b8d8e74b90cd6a3344a6b31c2ddb87d9be58a886985b1a4cc220097a41_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Post:update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 19,  90 => 16,  86 => 15,  81 => 14,  76 => 13,  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}

{% block title%} Update Article {% endblock %}

{% block body %}


{{ form_start(form) }}
    {#{{ form_row(form.username) }}
    {{ form_row(form.plainPassword.first) }}
    {{ form_row(form.plainPassword.second) }}
    {{ form_row(form.email) }}#}
    {{ form_row(form.title,  {'attr': {'requidred': 'false'}}) }}
    <img src=\"{{asset('uploads/image/')}}{{postimg}}\" style=\"max-height:100px; max-width:100px;\"/>
    {{ form_row(form.img,  {'attr': {'requirsed': 'false'}}) }}
    {{ form_row(form.content,  {'attr': {'requdired': 'false'}}) }}
    
    <button type=\"submit\">update!</button>
{{ form_end(form) }}





{% endblock %}", "AppBundle:Post:update.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/Post/update.html.twig");
    }
}
