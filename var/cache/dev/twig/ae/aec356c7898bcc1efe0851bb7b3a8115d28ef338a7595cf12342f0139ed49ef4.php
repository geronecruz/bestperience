<?php

/* @Twig/images/icon-minus-square.svg */
class __TwigTemplate_65b81e4b7d4534a815607ad42801b3aae4f5d8f04efc7ef069aec8685a551451 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d3db2d05b715fdb9264f1dda6d4a45f48c328e0fe422c99ed68a469b907cb371 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d3db2d05b715fdb9264f1dda6d4a45f48c328e0fe422c99ed68a469b907cb371->enter($__internal_d3db2d05b715fdb9264f1dda6d4a45f48c328e0fe422c99ed68a469b907cb371_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        $__internal_17937f6e6264f8e50db6987cad5f62408a0b3c7d9902807d5a48725795877e46 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_17937f6e6264f8e50db6987cad5f62408a0b3c7d9902807d5a48725795877e46->enter($__internal_17937f6e6264f8e50db6987cad5f62408a0b3c7d9902807d5a48725795877e46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
";
        
        $__internal_d3db2d05b715fdb9264f1dda6d4a45f48c328e0fe422c99ed68a469b907cb371->leave($__internal_d3db2d05b715fdb9264f1dda6d4a45f48c328e0fe422c99ed68a469b907cb371_prof);

        
        $__internal_17937f6e6264f8e50db6987cad5f62408a0b3c7d9902807d5a48725795877e46->leave($__internal_17937f6e6264f8e50db6987cad5f62408a0b3c7d9902807d5a48725795877e46_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-minus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
", "@Twig/images/icon-minus-square.svg", "/home/ron/bestperience/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-minus-square.svg");
    }
}
