<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_504b4c769556d0dec363c5a1ba46cd1ccd966e1af8d1a9aa5a5f438d60d29ae7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9ca14b6c59662f9c9dc8de83556cd4612c5fa1aead79173c2c01d13731aa9723 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ca14b6c59662f9c9dc8de83556cd4612c5fa1aead79173c2c01d13731aa9723->enter($__internal_9ca14b6c59662f9c9dc8de83556cd4612c5fa1aead79173c2c01d13731aa9723_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_7f490d21a33c1006aa3d19182e1e4dbd0306284bbe5705486fba44b5b57820d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7f490d21a33c1006aa3d19182e1e4dbd0306284bbe5705486fba44b5b57820d9->enter($__internal_7f490d21a33c1006aa3d19182e1e4dbd0306284bbe5705486fba44b5b57820d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9ca14b6c59662f9c9dc8de83556cd4612c5fa1aead79173c2c01d13731aa9723->leave($__internal_9ca14b6c59662f9c9dc8de83556cd4612c5fa1aead79173c2c01d13731aa9723_prof);

        
        $__internal_7f490d21a33c1006aa3d19182e1e4dbd0306284bbe5705486fba44b5b57820d9->leave($__internal_7f490d21a33c1006aa3d19182e1e4dbd0306284bbe5705486fba44b5b57820d9_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_5ce76ef56655156f644d2f46c2231ffda074bb65af9df357dbf6d8ea0d9c5513 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5ce76ef56655156f644d2f46c2231ffda074bb65af9df357dbf6d8ea0d9c5513->enter($__internal_5ce76ef56655156f644d2f46c2231ffda074bb65af9df357dbf6d8ea0d9c5513_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_cad448a1a3c9d842a0e99f36b1ba04b6e1848975897cebc9b2e6746b5d697d50 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cad448a1a3c9d842a0e99f36b1ba04b6e1848975897cebc9b2e6746b5d697d50->enter($__internal_cad448a1a3c9d842a0e99f36b1ba04b6e1848975897cebc9b2e6746b5d697d50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_cad448a1a3c9d842a0e99f36b1ba04b6e1848975897cebc9b2e6746b5d697d50->leave($__internal_cad448a1a3c9d842a0e99f36b1ba04b6e1848975897cebc9b2e6746b5d697d50_prof);

        
        $__internal_5ce76ef56655156f644d2f46c2231ffda074bb65af9df357dbf6d8ea0d9c5513->leave($__internal_5ce76ef56655156f644d2f46c2231ffda074bb65af9df357dbf6d8ea0d9c5513_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_36d8cca01e47be520c61bd0b3fec0f736332a757783d695fdb03ce784f0ae4d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_36d8cca01e47be520c61bd0b3fec0f736332a757783d695fdb03ce784f0ae4d3->enter($__internal_36d8cca01e47be520c61bd0b3fec0f736332a757783d695fdb03ce784f0ae4d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_2520d8da580bfd1de2dea260891492a880da12ce39b31a67dc2af2b275a1bc65 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2520d8da580bfd1de2dea260891492a880da12ce39b31a67dc2af2b275a1bc65->enter($__internal_2520d8da580bfd1de2dea260891492a880da12ce39b31a67dc2af2b275a1bc65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_2520d8da580bfd1de2dea260891492a880da12ce39b31a67dc2af2b275a1bc65->leave($__internal_2520d8da580bfd1de2dea260891492a880da12ce39b31a67dc2af2b275a1bc65_prof);

        
        $__internal_36d8cca01e47be520c61bd0b3fec0f736332a757783d695fdb03ce784f0ae4d3->leave($__internal_36d8cca01e47be520c61bd0b3fec0f736332a757783d695fdb03ce784f0ae4d3_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_50f62139a936901458b0e60758f3e1b10105e84b0ad84a373340a5a9250a288d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_50f62139a936901458b0e60758f3e1b10105e84b0ad84a373340a5a9250a288d->enter($__internal_50f62139a936901458b0e60758f3e1b10105e84b0ad84a373340a5a9250a288d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_1b4a9a59804a2b85c0ab0a69f1b38577c429ddc50ce2b221f33c8833bbf45c7a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b4a9a59804a2b85c0ab0a69f1b38577c429ddc50ce2b221f33c8833bbf45c7a->enter($__internal_1b4a9a59804a2b85c0ab0a69f1b38577c429ddc50ce2b221f33c8833bbf45c7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 13, $this->getSourceContext()); })()))));
        echo "
";
        
        $__internal_1b4a9a59804a2b85c0ab0a69f1b38577c429ddc50ce2b221f33c8833bbf45c7a->leave($__internal_1b4a9a59804a2b85c0ab0a69f1b38577c429ddc50ce2b221f33c8833bbf45c7a_prof);

        
        $__internal_50f62139a936901458b0e60758f3e1b10105e84b0ad84a373340a5a9250a288d->leave($__internal_50f62139a936901458b0e60758f3e1b10105e84b0ad84a373340a5a9250a288d_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/ron/bestperience/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
