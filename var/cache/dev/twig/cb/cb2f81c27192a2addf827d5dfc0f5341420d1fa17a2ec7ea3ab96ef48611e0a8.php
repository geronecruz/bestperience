<?php

/* AppBundle:User:registration.html.twig */
class __TwigTemplate_318048ca3b1322886012c9206018a39ececbc4137caeb73433a21422f9f1e02c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:User:registration.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'style' => array($this, 'block_style'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e03d5004ec9ad99f1fcd2e407224581905697d13dbf785db3ffe9efcc631b18f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e03d5004ec9ad99f1fcd2e407224581905697d13dbf785db3ffe9efcc631b18f->enter($__internal_e03d5004ec9ad99f1fcd2e407224581905697d13dbf785db3ffe9efcc631b18f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:registration.html.twig"));

        $__internal_3521ac83497604932089bfb63815f269b31f2ee9ee6731173e61fd825d37f106 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3521ac83497604932089bfb63815f269b31f2ee9ee6731173e61fd825d37f106->enter($__internal_3521ac83497604932089bfb63815f269b31f2ee9ee6731173e61fd825d37f106_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:registration.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e03d5004ec9ad99f1fcd2e407224581905697d13dbf785db3ffe9efcc631b18f->leave($__internal_e03d5004ec9ad99f1fcd2e407224581905697d13dbf785db3ffe9efcc631b18f_prof);

        
        $__internal_3521ac83497604932089bfb63815f269b31f2ee9ee6731173e61fd825d37f106->leave($__internal_3521ac83497604932089bfb63815f269b31f2ee9ee6731173e61fd825d37f106_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_6cc9a683c2dcf202ce34936818f95a4fc5ca4bf2df5c7bf41649aaa195ee034f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6cc9a683c2dcf202ce34936818f95a4fc5ca4bf2df5c7bf41649aaa195ee034f->enter($__internal_6cc9a683c2dcf202ce34936818f95a4fc5ca4bf2df5c7bf41649aaa195ee034f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_dae8979f2a86f0e3c66478089e36e413550273003387b27262d8baadf580eece = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dae8979f2a86f0e3c66478089e36e413550273003387b27262d8baadf580eece->enter($__internal_dae8979f2a86f0e3c66478089e36e413550273003387b27262d8baadf580eece_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Register";
        
        $__internal_dae8979f2a86f0e3c66478089e36e413550273003387b27262d8baadf580eece->leave($__internal_dae8979f2a86f0e3c66478089e36e413550273003387b27262d8baadf580eece_prof);

        
        $__internal_6cc9a683c2dcf202ce34936818f95a4fc5ca4bf2df5c7bf41649aaa195ee034f->leave($__internal_6cc9a683c2dcf202ce34936818f95a4fc5ca4bf2df5c7bf41649aaa195ee034f_prof);

    }

    // line 6
    public function block_style($context, array $blocks = array())
    {
        $__internal_a968e1aaea84842b88e422bf27da3eb4a8d6303a4a2ab4040ce6f8c135128752 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a968e1aaea84842b88e422bf27da3eb4a8d6303a4a2ab4040ce6f8c135128752->enter($__internal_a968e1aaea84842b88e422bf27da3eb4a8d6303a4a2ab4040ce6f8c135128752_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        $__internal_ea0f631f884521ab6b216d2d3edb8c33a378937d627d6a3da90a1311cbc650e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ea0f631f884521ab6b216d2d3edb8c33a378937d627d6a3da90a1311cbc650e5->enter($__internal_ea0f631f884521ab6b216d2d3edb8c33a378937d627d6a3da90a1311cbc650e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/signup.css"), "html", null, true);
        echo "\">";
        
        $__internal_ea0f631f884521ab6b216d2d3edb8c33a378937d627d6a3da90a1311cbc650e5->leave($__internal_ea0f631f884521ab6b216d2d3edb8c33a378937d627d6a3da90a1311cbc650e5_prof);

        
        $__internal_a968e1aaea84842b88e422bf27da3eb4a8d6303a4a2ab4040ce6f8c135128752->leave($__internal_a968e1aaea84842b88e422bf27da3eb4a8d6303a4a2ab4040ce6f8c135128752_prof);

    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        $__internal_ab69b23602465b6acff0d66c20be19116b97ed6f0ac0a8761572d86061a9c52b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab69b23602465b6acff0d66c20be19116b97ed6f0ac0a8761572d86061a9c52b->enter($__internal_ab69b23602465b6acff0d66c20be19116b97ed6f0ac0a8761572d86061a9c52b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a1fbda37562f63e1f66e7d709f863ed9a9f5cecd13b40c5e0359a4f9414ada30 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1fbda37562f63e1f66e7d709f863ed9a9f5cecd13b40c5e0359a4f9414ada30->enter($__internal_a1fbda37562f63e1f66e7d709f863ed9a9f5cecd13b40c5e0359a4f9414ada30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 9
        echo "

<div class=\"signup-text\" >
<h1>
    Bestperience
</h1>
<p>Share your best experience!</p>
</div>

<div class=\"signup-box\" style=\"background-color: \">
  <center>
<p style=\"font-family: 'Roboto',sans-serif;color: white;margin-top: 40px\">
  Sign Up now with BestPerience
</p>
</center>
";
        // line 24
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 24, $this->getSourceContext()); })()), 'form_start');
        echo "
<div class=\"signup-form\">
 <div class=\"form-group\">
 ";
        // line 27
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 27, $this->getSourceContext()); })()), "username", array()), 'errors');
        echo "
    ";
        // line 28
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 28, $this->getSourceContext()); })()), "username", array()), 'row');
        echo "
</div>
<div class=\"form-group\">
  
    ";
        // line 32
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 32, $this->getSourceContext()); })()), "email", array()), 'errors');
        echo "
    ";
        // line 33
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 33, $this->getSourceContext()); })()), "email", array()), 'row');
        echo "
</div>
<div class=\"form-group\">
  ";
        // line 36
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 36, $this->getSourceContext()); })()), "plainPassword", array()), "first", array()), 'errors');
        echo "
    ";
        // line 37
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 37, $this->getSourceContext()); })()), "plainPassword", array()), "first", array()), 'row');
        echo "
     ";
        // line 38
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 38, $this->getSourceContext()); })()), "plainPassword", array()), "second", array()), 'errors');
        echo "
    ";
        // line 39
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 39, $this->getSourceContext()); })()), "plainPassword", array()), "second", array()), 'row');
        echo "
     <button class=\"btn btn-primary btn-block\" type=\"submit\">Register!</button>


  <p style=\" color:white; font-size: 15px; margin-top: 10px; \">Already have an account? <a href=\"";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" style=\"color: white;\">Login</a></p>
</div> 
</div>






   
    
   
    

";
        // line 57
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 57, $this->getSourceContext()); })()), 'form_end');
        echo "
    
    

";
        
        $__internal_a1fbda37562f63e1f66e7d709f863ed9a9f5cecd13b40c5e0359a4f9414ada30->leave($__internal_a1fbda37562f63e1f66e7d709f863ed9a9f5cecd13b40c5e0359a4f9414ada30_prof);

        
        $__internal_ab69b23602465b6acff0d66c20be19116b97ed6f0ac0a8761572d86061a9c52b->leave($__internal_ab69b23602465b6acff0d66c20be19116b97ed6f0ac0a8761572d86061a9c52b_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:User:registration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 57,  152 => 43,  145 => 39,  141 => 38,  137 => 37,  133 => 36,  127 => 33,  123 => 32,  116 => 28,  112 => 27,  106 => 24,  89 => 9,  80 => 8,  60 => 6,  42 => 5,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}



{% block title %}Register{% endblock %}
{% block style %}<link rel=\"stylesheet\" href=\"{{asset('assets/css/signup.css')}}\">{% endblock %}

{% block body %}


<div class=\"signup-text\" >
<h1>
    Bestperience
</h1>
<p>Share your best experience!</p>
</div>

<div class=\"signup-box\" style=\"background-color: \">
  <center>
<p style=\"font-family: 'Roboto',sans-serif;color: white;margin-top: 40px\">
  Sign Up now with BestPerience
</p>
</center>
{{ form_start(form) }}
<div class=\"signup-form\">
 <div class=\"form-group\">
 {{form_errors(form.username )}}
    {{ form_row(form.username) }}
</div>
<div class=\"form-group\">
  
    {{form_errors(form.email)}}
    {{ form_row(form.email) }}
</div>
<div class=\"form-group\">
  {{form_errors(form.plainPassword.first)}}
    {{ form_row(form.plainPassword.first) }}
     {{form_errors(form.plainPassword.second)}}
    {{ form_row(form.plainPassword.second) }}
     <button class=\"btn btn-primary btn-block\" type=\"submit\">Register!</button>


  <p style=\" color:white; font-size: 15px; margin-top: 10px; \">Already have an account? <a href=\"{{path('login')}}\" style=\"color: white;\">Login</a></p>
</div> 
</div>






   
    
   
    

{{ form_end(form) }}
    
    

{% endblock %}
", "AppBundle:User:registration.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/User/registration.html.twig");
    }
}
