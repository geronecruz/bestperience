<?php

/* AppBundle:Post:list.html.twig */
class __TwigTemplate_6c34c6e0091b0da3fcbbf445b85a03375ebed0a0ad7d5b54cd3f7302b1601f54 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:Post:list.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_26ea2100aa4efe20816e245f3f1e288e7079aa6c64fe43f300b0405eddfe71a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_26ea2100aa4efe20816e245f3f1e288e7079aa6c64fe43f300b0405eddfe71a5->enter($__internal_26ea2100aa4efe20816e245f3f1e288e7079aa6c64fe43f300b0405eddfe71a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:list.html.twig"));

        $__internal_b6e96451c3e9186c89209df8ea987eee5892b2ecb1f67b590c3ba9d8d1323d84 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b6e96451c3e9186c89209df8ea987eee5892b2ecb1f67b590c3ba9d8d1323d84->enter($__internal_b6e96451c3e9186c89209df8ea987eee5892b2ecb1f67b590c3ba9d8d1323d84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_26ea2100aa4efe20816e245f3f1e288e7079aa6c64fe43f300b0405eddfe71a5->leave($__internal_26ea2100aa4efe20816e245f3f1e288e7079aa6c64fe43f300b0405eddfe71a5_prof);

        
        $__internal_b6e96451c3e9186c89209df8ea987eee5892b2ecb1f67b590c3ba9d8d1323d84->leave($__internal_b6e96451c3e9186c89209df8ea987eee5892b2ecb1f67b590c3ba9d8d1323d84_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_ed339ceaff96f60575fe0919c53d3880d30453442d94cd4773ef7e8e886eb488 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed339ceaff96f60575fe0919c53d3880d30453442d94cd4773ef7e8e886eb488->enter($__internal_ed339ceaff96f60575fe0919c53d3880d30453442d94cd4773ef7e8e886eb488_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_6cd80494f1a1090c1dea437a20c705f69516fbd205d07f8df43e9b6cde95b974 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6cd80494f1a1090c1dea437a20c705f69516fbd205d07f8df43e9b6cde95b974->enter($__internal_6cd80494f1a1090c1dea437a20c705f69516fbd205d07f8df43e9b6cde95b974_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Home";
        
        $__internal_6cd80494f1a1090c1dea437a20c705f69516fbd205d07f8df43e9b6cde95b974->leave($__internal_6cd80494f1a1090c1dea437a20c705f69516fbd205d07f8df43e9b6cde95b974_prof);

        
        $__internal_ed339ceaff96f60575fe0919c53d3880d30453442d94cd4773ef7e8e886eb488->leave($__internal_ed339ceaff96f60575fe0919c53d3880d30453442d94cd4773ef7e8e886eb488_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_5790df0917c2c2341e8856002a715ab56aa613a233d004b73815b9730879b788 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5790df0917c2c2341e8856002a715ab56aa613a233d004b73815b9730879b788->enter($__internal_5790df0917c2c2341e8856002a715ab56aa613a233d004b73815b9730879b788_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_23b82f8d4fae6e316a51a32572883cf1cd2101d6520c568c042b8cf6938b1037 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_23b82f8d4fae6e316a51a32572883cf1cd2101d6520c568c042b8cf6938b1037->enter($__internal_23b82f8d4fae6e316a51a32572883cf1cd2101d6520c568c042b8cf6938b1037_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
<table class=\" table table-hover\">
\t<thead>
\t\t<tr>
\t\t\t<th>Date</th>
\t\t\t<th>Title</th>
\t\t\t<th>Content</th>
\t\t\t<th>Image</th>
\t\t\t<th>Option</th>
\t\t</tr>
\t</thead>
\t\t\t
\t<tbody>
\t\t";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["Posts"]) || array_key_exists("Posts", $context) ? $context["Posts"] : (function () { throw new Twig_Error_Runtime('Variable "Posts" does not exist.', 19, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["Post"]) {
            // line 20
            echo "\t\t<tr>
\t\t\t<td>";
            // line 21
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "date", array()), "M/d/Y H:i"), "html", null, true);
            echo "</td>
\t\t\t<td>";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "title", array()), "html", null, true);
            echo " <br/>
\t\t\t\t<a class=\"";
            // line 23
            echo "\" title=\"View\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_view", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "id", array()))), "html", null, true);
            echo "\">";
            echo "view</a> 
\t\t\t\t<a class=\"";
            // line 24
            echo "\" title=\"edit\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_post_update", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "id", array()))), "html", null, true);
            echo "\"> ";
            echo "edit</i></a> 
\t\t\t\t<a class=\"";
            // line 25
            echo "text-danger\" title=\"Remove\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_post_remove", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "id", array()))), "html", null, true);
            echo "\"> ";
            echo "remove</a>
\t\t\t</td>
\t\t\t<td>";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "Content", array()), "html", null, true);
            echo "</td>
\t\t\t<td> <img src=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("uploads/image/"), "html", null, true);
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "img", array()), "html", null, true);
            echo "\" style=\"max-height:100px; max-width: 150px;\"/></td>
\t\t\t<td></td>

\t\t</tr>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['Post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "\t</tbody>
</table>
\t\t\t





";
        
        $__internal_23b82f8d4fae6e316a51a32572883cf1cd2101d6520c568c042b8cf6938b1037->leave($__internal_23b82f8d4fae6e316a51a32572883cf1cd2101d6520c568c042b8cf6938b1037_prof);

        
        $__internal_5790df0917c2c2341e8856002a715ab56aa613a233d004b73815b9730879b788->leave($__internal_5790df0917c2c2341e8856002a715ab56aa613a233d004b73815b9730879b788_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Post:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 33,  121 => 28,  117 => 27,  110 => 25,  104 => 24,  98 => 23,  94 => 22,  90 => 21,  87 => 20,  83 => 19,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}

{% block title %}Home{% endblock %}

{% block body %}

<table class=\" table table-hover\">
\t<thead>
\t\t<tr>
\t\t\t<th>Date</th>
\t\t\t<th>Title</th>
\t\t\t<th>Content</th>
\t\t\t<th>Image</th>
\t\t\t<th>Option</th>
\t\t</tr>
\t</thead>
\t\t\t
\t<tbody>
\t\t{%for Post in Posts%}
\t\t<tr>
\t\t\t<td>{{Post.date|date('M/d/Y H:i')}}</td>
\t\t\t<td>{{Post.title}} <br/>
\t\t\t\t<a class=\"{#btn btn-success btn-xs#}\" title=\"View\" href=\"{{path('post_view',{id:Post.id})}}\">{#<i class=\"fa fa-eye\"></i>#}view</a> 
\t\t\t\t<a class=\"{#btn btn-warning btn-xs#}\" title=\"edit\" href=\"{{path('admin_post_update',{id:Post.id})}}\"> {#<i class=\"fa fa-pencil-square-o\">#}edit</i></a> 
\t\t\t\t<a class=\"{#btn btn-danger btn-xs#}text-danger\" title=\"Remove\" href=\"{{path('admin_post_remove',{id:Post.id})}}\"> {#<i class=\"fa fa-trash-o\"></i>#}remove</a>
\t\t\t</td>
\t\t\t<td>{{Post.Content}}</td>
\t\t\t<td> <img src=\"{{asset('uploads/image/')}}{{Post.img}}\" style=\"max-height:100px; max-width: 150px;\"/></td>
\t\t\t<td></td>

\t\t</tr>
\t\t{%endfor%}
\t</tbody>
</table>
\t\t\t





{% endblock %}", "AppBundle:Post:list.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/Post/list.html.twig");
    }
}
