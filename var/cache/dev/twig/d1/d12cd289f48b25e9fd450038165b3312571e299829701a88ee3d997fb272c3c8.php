<?php

/* AppBundle:Post:viewPost.html.twig */
class __TwigTemplate_120176f843174af790f106b65cfbf5931c8410cb4535f98c57016cfba1eb6104 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::loggedin.html.twig", "AppBundle:Post:viewPost.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::loggedin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f96a7647caa8471c682e1ee04d5d2cbf71335494baea88856da79c8ddd9356b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f96a7647caa8471c682e1ee04d5d2cbf71335494baea88856da79c8ddd9356b0->enter($__internal_f96a7647caa8471c682e1ee04d5d2cbf71335494baea88856da79c8ddd9356b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:viewPost.html.twig"));

        $__internal_a9821488cf172259e8aa46d608f903e898ff4df83945f7073838c5e57504fcf3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a9821488cf172259e8aa46d608f903e898ff4df83945f7073838c5e57504fcf3->enter($__internal_a9821488cf172259e8aa46d608f903e898ff4df83945f7073838c5e57504fcf3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:viewPost.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f96a7647caa8471c682e1ee04d5d2cbf71335494baea88856da79c8ddd9356b0->leave($__internal_f96a7647caa8471c682e1ee04d5d2cbf71335494baea88856da79c8ddd9356b0_prof);

        
        $__internal_a9821488cf172259e8aa46d608f903e898ff4df83945f7073838c5e57504fcf3->leave($__internal_a9821488cf172259e8aa46d608f903e898ff4df83945f7073838c5e57504fcf3_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_6dbb650fd1ddaba2ea710750d86eec4e848e027bd186fadf7f62d3fdb869ea7a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6dbb650fd1ddaba2ea710750d86eec4e848e027bd186fadf7f62d3fdb869ea7a->enter($__internal_6dbb650fd1ddaba2ea710750d86eec4e848e027bd186fadf7f62d3fdb869ea7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_aaa9f4347b16a66fdcf1295b5a87f10fb60531bdf4b0e1bae320c0adca7f2d46 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aaa9f4347b16a66fdcf1295b5a87f10fb60531bdf4b0e1bae320c0adca7f2d46->enter($__internal_aaa9f4347b16a66fdcf1295b5a87f10fb60531bdf4b0e1bae320c0adca7f2d46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " Update Article ";
        
        $__internal_aaa9f4347b16a66fdcf1295b5a87f10fb60531bdf4b0e1bae320c0adca7f2d46->leave($__internal_aaa9f4347b16a66fdcf1295b5a87f10fb60531bdf4b0e1bae320c0adca7f2d46_prof);

        
        $__internal_6dbb650fd1ddaba2ea710750d86eec4e848e027bd186fadf7f62d3fdb869ea7a->leave($__internal_6dbb650fd1ddaba2ea710750d86eec4e848e027bd186fadf7f62d3fdb869ea7a_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_1fbb58d711020efd160296288e654c25230f8166de3cf72a960f4886c37c65e2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1fbb58d711020efd160296288e654c25230f8166de3cf72a960f4886c37c65e2->enter($__internal_1fbb58d711020efd160296288e654c25230f8166de3cf72a960f4886c37c65e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_35f715e7b0f9cd08426a261eff3d08964a8a7969d8ae652eb05b0324e3d5fb66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_35f715e7b0f9cd08426a261eff3d08964a8a7969d8ae652eb05b0324e3d5fb66->enter($__internal_35f715e7b0f9cd08426a261eff3d08964a8a7969d8ae652eb05b0324e3d5fb66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "

 <section id=\"home-section\" class=\"line\">   

<div style=\"padding-right: 150px; padding-left: 150px; margin: 10px\">
    <center>
    <img src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/image/" . twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["Post"]) || array_key_exists("Post", $context) ? $context["Post"] : (function () { throw new Twig_Error_Runtime('Variable "Post" does not exist.', 12, $this->getSourceContext()); })()), "img", array()))), "html", null, true);
        echo "\" style=\"max-height:500px; max-width: 500px;\" />


    <h1 style=\"color: white;\">";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["Post"]) || array_key_exists("Post", $context) ? $context["Post"] : (function () { throw new Twig_Error_Runtime('Variable "Post" does not exist.', 15, $this->getSourceContext()); })()), "title", array()), "html", null, true);
        echo "</h1>
    </center>
    <p>";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["Post"]) || array_key_exists("Post", $context) ? $context["Post"] : (function () { throw new Twig_Error_Runtime('Variable "Post" does not exist.', 17, $this->getSourceContext()); })()), "content", array()), "html", null, true);
        echo "</p>
    Date: ";
        // line 18
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["Post"]) || array_key_exists("Post", $context) ? $context["Post"] : (function () { throw new Twig_Error_Runtime('Variable "Post" does not exist.', 18, $this->getSourceContext()); })()), "date", array()), "M/d/Y"), "html", null, true);
        echo "

</div>
<br/>
<br/>
<br/>
<div style=\"padding-right: 300px; margin: 10px\">
<label>Comments</label>

";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new Twig_Error_Runtime('Variable "comments" does not exist.', 27, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 28
            echo "    <div style=\"margin: 5px;\">
        <div style=\"background-color: #204060;\">
            <label>";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["c"], "name", array()), "html", null, true);
            echo " : </label>
            <p style=\"display: inline;\">";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["c"], "comment", array()), "html", null, true);
            echo "</p>
            ";
            // line 32
            if (((isset($context["checkUser"]) || array_key_exists("checkUser", $context) ? $context["checkUser"] : (function () { throw new Twig_Error_Runtime('Variable "checkUser" does not exist.', 32, $this->getSourceContext()); })()) == "true")) {
                // line 33
                echo "            <div class=\"pull-right\">
                <a class=\"\" style=\"color: white;\" title=\"remove\" href=\"";
                // line 34
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("edit_comment", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["c"], "id", array()))), "html", null, true);
                echo "\">edit</a> 
                <a class=\"\" style=\"color: pink;\"title=\"remove\" href=\"";
                // line 35
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("remove_comment", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["c"], "id", array()))), "html", null, true);
                echo "\">remove</a>
            </div>
                
            ";
            }
            // line 39
            echo "        </div>
    </div>
        
        
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "

<br/>


";
        // line 49
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 49, $this->getSourceContext()); })()), 'form_start');
        echo "
    
    ";
        // line 51
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 51, $this->getSourceContext()); })()), "comment", array()), 'row');
        echo "
    <button class=\"btn btn-primary\" type=\"submit\">Comment</button>
";
        // line 53
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 53, $this->getSourceContext()); })()), 'form_end');
        echo "
</div>
</section>
";
        
        $__internal_35f715e7b0f9cd08426a261eff3d08964a8a7969d8ae652eb05b0324e3d5fb66->leave($__internal_35f715e7b0f9cd08426a261eff3d08964a8a7969d8ae652eb05b0324e3d5fb66_prof);

        
        $__internal_1fbb58d711020efd160296288e654c25230f8166de3cf72a960f4886c37c65e2->leave($__internal_1fbb58d711020efd160296288e654c25230f8166de3cf72a960f4886c37c65e2_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Post:viewPost.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 53,  157 => 51,  152 => 49,  145 => 44,  135 => 39,  128 => 35,  124 => 34,  121 => 33,  119 => 32,  115 => 31,  111 => 30,  107 => 28,  103 => 27,  91 => 18,  87 => 17,  82 => 15,  76 => 12,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::loggedin.html.twig\" %}

{% block title%} Update Article {% endblock %}

{% block body %}


 <section id=\"home-section\" class=\"line\">   

<div style=\"padding-right: 150px; padding-left: 150px; margin: 10px\">
    <center>
    <img src=\"{{asset('uploads/image/' ~ Post.img)}}\" style=\"max-height:500px; max-width: 500px;\" />


    <h1 style=\"color: white;\">{{Post.title}}</h1>
    </center>
    <p>{{Post.content}}</p>
    Date: {{Post.date|date('M/d/Y')}}

</div>
<br/>
<br/>
<br/>
<div style=\"padding-right: 300px; margin: 10px\">
<label>Comments</label>

{% for c in comments %}
    <div style=\"margin: 5px;\">
        <div style=\"background-color: #204060;\">
            <label>{{c.name}} : </label>
            <p style=\"display: inline;\">{{c.comment}}</p>
            {%if(checkUser == \"true\")%}
            <div class=\"pull-right\">
                <a class=\"\" style=\"color: white;\" title=\"remove\" href=\"{{path('edit_comment',{id:c.id})}}\">edit</a> 
                <a class=\"\" style=\"color: pink;\"title=\"remove\" href=\"{{path('remove_comment',{id:c.id})}}\">remove</a>
            </div>
                
            {% endif %}
        </div>
    </div>
        
        
{% endfor %}


<br/>


{{form_start(form)}}
    
    {{form_row(form.comment)}}
    <button class=\"btn btn-primary\" type=\"submit\">Comment</button>
{{form_end(form)}}
</div>
</section>
{% endblock %}", "AppBundle:Post:viewPost.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/Post/viewPost.html.twig");
    }
}
