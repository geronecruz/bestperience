<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_4e3271903e33ece75271e854a17246dc43a169ac90a69a863843468c5ada6dd1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dc59da290b63f53598c01516df11ce82ab281b5bf67ce7a5eb49dd1c8d05a6cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dc59da290b63f53598c01516df11ce82ab281b5bf67ce7a5eb49dd1c8d05a6cb->enter($__internal_dc59da290b63f53598c01516df11ce82ab281b5bf67ce7a5eb49dd1c8d05a6cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_320b0f6b3096a0b52998d894b94c4dac6f8211a998e9db2479c06090aa144098 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_320b0f6b3096a0b52998d894b94c4dac6f8211a998e9db2479c06090aa144098->enter($__internal_320b0f6b3096a0b52998d894b94c4dac6f8211a998e9db2479c06090aa144098_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dc59da290b63f53598c01516df11ce82ab281b5bf67ce7a5eb49dd1c8d05a6cb->leave($__internal_dc59da290b63f53598c01516df11ce82ab281b5bf67ce7a5eb49dd1c8d05a6cb_prof);

        
        $__internal_320b0f6b3096a0b52998d894b94c4dac6f8211a998e9db2479c06090aa144098->leave($__internal_320b0f6b3096a0b52998d894b94c4dac6f8211a998e9db2479c06090aa144098_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_7e66fc376b00abddecd6d716e6c5d88b568cb932ed39937369895aa00a26ddfc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7e66fc376b00abddecd6d716e6c5d88b568cb932ed39937369895aa00a26ddfc->enter($__internal_7e66fc376b00abddecd6d716e6c5d88b568cb932ed39937369895aa00a26ddfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_3ccaa17641c2cd5778616628bdbcfa141785dd679923793f2269ea4bd6358ce6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ccaa17641c2cd5778616628bdbcfa141785dd679923793f2269ea4bd6358ce6->enter($__internal_3ccaa17641c2cd5778616628bdbcfa141785dd679923793f2269ea4bd6358ce6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_3ccaa17641c2cd5778616628bdbcfa141785dd679923793f2269ea4bd6358ce6->leave($__internal_3ccaa17641c2cd5778616628bdbcfa141785dd679923793f2269ea4bd6358ce6_prof);

        
        $__internal_7e66fc376b00abddecd6d716e6c5d88b568cb932ed39937369895aa00a26ddfc->leave($__internal_7e66fc376b00abddecd6d716e6c5d88b568cb932ed39937369895aa00a26ddfc_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_5ff6d1f2109a75607a93483c3bf7a5427ec96f6b5578ec05e776888bc6259706 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5ff6d1f2109a75607a93483c3bf7a5427ec96f6b5578ec05e776888bc6259706->enter($__internal_5ff6d1f2109a75607a93483c3bf7a5427ec96f6b5578ec05e776888bc6259706_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_6485da0f99d134cf62cdefab682e1c35eeccab42d71b0fab269d255157087c23 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6485da0f99d134cf62cdefab682e1c35eeccab42d71b0fab269d255157087c23->enter($__internal_6485da0f99d134cf62cdefab682e1c35eeccab42d71b0fab269d255157087c23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_6485da0f99d134cf62cdefab682e1c35eeccab42d71b0fab269d255157087c23->leave($__internal_6485da0f99d134cf62cdefab682e1c35eeccab42d71b0fab269d255157087c23_prof);

        
        $__internal_5ff6d1f2109a75607a93483c3bf7a5427ec96f6b5578ec05e776888bc6259706->leave($__internal_5ff6d1f2109a75607a93483c3bf7a5427ec96f6b5578ec05e776888bc6259706_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_c6beb36b724582541bf93d978e8293c67b56a7105d6f80ab4a44db65a1deccd6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6beb36b724582541bf93d978e8293c67b56a7105d6f80ab4a44db65a1deccd6->enter($__internal_c6beb36b724582541bf93d978e8293c67b56a7105d6f80ab4a44db65a1deccd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_1b965c30dbe99fa98812d3e7ab9fdea86c12e225565c5d8e700c50e551c47c17 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b965c30dbe99fa98812d3e7ab9fdea86c12e225565c5d8e700c50e551c47c17->enter($__internal_1b965c30dbe99fa98812d3e7ab9fdea86c12e225565c5d8e700c50e551c47c17_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_1b965c30dbe99fa98812d3e7ab9fdea86c12e225565c5d8e700c50e551c47c17->leave($__internal_1b965c30dbe99fa98812d3e7ab9fdea86c12e225565c5d8e700c50e551c47c17_prof);

        
        $__internal_c6beb36b724582541bf93d978e8293c67b56a7105d6f80ab4a44db65a1deccd6->leave($__internal_c6beb36b724582541bf93d978e8293c67b56a7105d6f80ab4a44db65a1deccd6_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/ron/bestperience/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
