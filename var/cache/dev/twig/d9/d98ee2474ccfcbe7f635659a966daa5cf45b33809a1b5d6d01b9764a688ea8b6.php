<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_4fe0126cea834c2021a568bc22188a6a6babe007ed6f1aff9514e87b8ba11595 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e06370f769e34827af9c34920ef9d29a542418c930e9d633489e3a307b455f75 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e06370f769e34827af9c34920ef9d29a542418c930e9d633489e3a307b455f75->enter($__internal_e06370f769e34827af9c34920ef9d29a542418c930e9d633489e3a307b455f75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_fcb5d91ed5b15425a15392afc43334fe32affcc42feda5b2b7fb770dc991277d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fcb5d91ed5b15425a15392afc43334fe32affcc42feda5b2b7fb770dc991277d->enter($__internal_fcb5d91ed5b15425a15392afc43334fe32affcc42feda5b2b7fb770dc991277d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e06370f769e34827af9c34920ef9d29a542418c930e9d633489e3a307b455f75->leave($__internal_e06370f769e34827af9c34920ef9d29a542418c930e9d633489e3a307b455f75_prof);

        
        $__internal_fcb5d91ed5b15425a15392afc43334fe32affcc42feda5b2b7fb770dc991277d->leave($__internal_fcb5d91ed5b15425a15392afc43334fe32affcc42feda5b2b7fb770dc991277d_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_0d9a23dfc565e7807c508cd9256dd4ef94c5775fcb9e67a62ad618a64b24b65d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0d9a23dfc565e7807c508cd9256dd4ef94c5775fcb9e67a62ad618a64b24b65d->enter($__internal_0d9a23dfc565e7807c508cd9256dd4ef94c5775fcb9e67a62ad618a64b24b65d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_56aad3bca5715933aaf44deed019b74443ba9750680c16afa76c433f14070038 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56aad3bca5715933aaf44deed019b74443ba9750680c16afa76c433f14070038->enter($__internal_56aad3bca5715933aaf44deed019b74443ba9750680c16afa76c433f14070038_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_56aad3bca5715933aaf44deed019b74443ba9750680c16afa76c433f14070038->leave($__internal_56aad3bca5715933aaf44deed019b74443ba9750680c16afa76c433f14070038_prof);

        
        $__internal_0d9a23dfc565e7807c508cd9256dd4ef94c5775fcb9e67a62ad618a64b24b65d->leave($__internal_0d9a23dfc565e7807c508cd9256dd4ef94c5775fcb9e67a62ad618a64b24b65d_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_b761edec25e0b462966080d7a30c8b12abe130887ead6fc4952ad3e27badf476 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b761edec25e0b462966080d7a30c8b12abe130887ead6fc4952ad3e27badf476->enter($__internal_b761edec25e0b462966080d7a30c8b12abe130887ead6fc4952ad3e27badf476_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_b40f9ce95e6f02f1e5ead0fa08d632c55e645575d1b04909cbf2e89e8c22c807 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b40f9ce95e6f02f1e5ead0fa08d632c55e645575d1b04909cbf2e89e8c22c807->enter($__internal_b40f9ce95e6f02f1e5ead0fa08d632c55e645575d1b04909cbf2e89e8c22c807_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 137, $this->getSourceContext()); })()), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo ")
";
        
        $__internal_b40f9ce95e6f02f1e5ead0fa08d632c55e645575d1b04909cbf2e89e8c22c807->leave($__internal_b40f9ce95e6f02f1e5ead0fa08d632c55e645575d1b04909cbf2e89e8c22c807_prof);

        
        $__internal_b761edec25e0b462966080d7a30c8b12abe130887ead6fc4952ad3e27badf476->leave($__internal_b761edec25e0b462966080d7a30c8b12abe130887ead6fc4952ad3e27badf476_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_8064ce4bc5c90c7760d2c15fe05bd1995e861f8a42fcb54860f6078fd438d5f5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8064ce4bc5c90c7760d2c15fe05bd1995e861f8a42fcb54860f6078fd438d5f5->enter($__internal_8064ce4bc5c90c7760d2c15fe05bd1995e861f8a42fcb54860f6078fd438d5f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ff0908d6c6f22547a7b103a713cc7ed9a5edb62040854f7ce1f9df178943b926 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff0908d6c6f22547a7b103a713cc7ed9a5edb62040854f7ce1f9df178943b926->enter($__internal_ff0908d6c6f22547a7b103a713cc7ed9a5edb62040854f7ce1f9df178943b926_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_ff0908d6c6f22547a7b103a713cc7ed9a5edb62040854f7ce1f9df178943b926->leave($__internal_ff0908d6c6f22547a7b103a713cc7ed9a5edb62040854f7ce1f9df178943b926_prof);

        
        $__internal_8064ce4bc5c90c7760d2c15fe05bd1995e861f8a42fcb54860f6078fd438d5f5->leave($__internal_8064ce4bc5c90c7760d2c15fe05bd1995e861f8a42fcb54860f6078fd438d5f5_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/ron/bestperience/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
