<?php

/* AppBundle:User:editprofile.html.twig */
class __TwigTemplate_9d8231d937cc977437d46ba90d696aa904fea4a5828e9174cb8042723ac92615 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::loggedin.html.twig", "AppBundle:User:editprofile.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::loggedin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5fdea71983008501eadcc2efa0cf9b7b751fe77db77fa6c944f0c5f9c58d6f8b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5fdea71983008501eadcc2efa0cf9b7b751fe77db77fa6c944f0c5f9c58d6f8b->enter($__internal_5fdea71983008501eadcc2efa0cf9b7b751fe77db77fa6c944f0c5f9c58d6f8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:editprofile.html.twig"));

        $__internal_26742933f98a515475d2e0ffab60342ca9b32ccfeb514b6cb0f60ade54eec85a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_26742933f98a515475d2e0ffab60342ca9b32ccfeb514b6cb0f60ade54eec85a->enter($__internal_26742933f98a515475d2e0ffab60342ca9b32ccfeb514b6cb0f60ade54eec85a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:editprofile.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5fdea71983008501eadcc2efa0cf9b7b751fe77db77fa6c944f0c5f9c58d6f8b->leave($__internal_5fdea71983008501eadcc2efa0cf9b7b751fe77db77fa6c944f0c5f9c58d6f8b_prof);

        
        $__internal_26742933f98a515475d2e0ffab60342ca9b32ccfeb514b6cb0f60ade54eec85a->leave($__internal_26742933f98a515475d2e0ffab60342ca9b32ccfeb514b6cb0f60ade54eec85a_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_8fb2a9eeaba58bf43fea0cbc040d6eb00f757f816bb16372b86b4aed418bbc9c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8fb2a9eeaba58bf43fea0cbc040d6eb00f757f816bb16372b86b4aed418bbc9c->enter($__internal_8fb2a9eeaba58bf43fea0cbc040d6eb00f757f816bb16372b86b4aed418bbc9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_ddf41fc9274d82cab6186b952d3c9f05dea03859798b08a2a9a42156e79d1616 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ddf41fc9274d82cab6186b952d3c9f05dea03859798b08a2a9a42156e79d1616->enter($__internal_ddf41fc9274d82cab6186b952d3c9f05dea03859798b08a2a9a42156e79d1616_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Register";
        
        $__internal_ddf41fc9274d82cab6186b952d3c9f05dea03859798b08a2a9a42156e79d1616->leave($__internal_ddf41fc9274d82cab6186b952d3c9f05dea03859798b08a2a9a42156e79d1616_prof);

        
        $__internal_8fb2a9eeaba58bf43fea0cbc040d6eb00f757f816bb16372b86b4aed418bbc9c->leave($__internal_8fb2a9eeaba58bf43fea0cbc040d6eb00f757f816bb16372b86b4aed418bbc9c_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_ed56464c9948eca98e064c7dc15e40e793347c0f9bde64a794f4d3501e90f7b7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed56464c9948eca98e064c7dc15e40e793347c0f9bde64a794f4d3501e90f7b7->enter($__internal_ed56464c9948eca98e064c7dc15e40e793347c0f9bde64a794f4d3501e90f7b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f227d6edb7fde8425503f9031e535bc327ea64d3faf0aecf25a963b114d6e517 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f227d6edb7fde8425503f9031e535bc327ea64d3faf0aecf25a963b114d6e517->enter($__internal_f227d6edb7fde8425503f9031e535bc327ea64d3faf0aecf25a963b114d6e517_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "


";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 9, $this->getSourceContext()); })()), 'form_start');
        echo "
  
    ";
        // line 15
        echo "
    <div class=\"row \">
        <div class = \"\" style=\"margin-left: 300px; margin-right:300px; margin-top:50; background-color: #19334d; padding: 10px\">
            <center><h2 style=\"color: white;\">Edit Profile</h2></center>
           
                    <img src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("uploads/image/"), "html", null, true);
        echo twig_escape_filter($this->env, (isset($context["userimg"]) || array_key_exists("userimg", $context) ? $context["userimg"] : (function () { throw new Twig_Error_Runtime('Variable "userimg" does not exist.', 20, $this->getSourceContext()); })()), "html", null, true);
        echo "\" style=\"max-height:100px; max-width:100px;\"/>
                    ";
        // line 21
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 21, $this->getSourceContext()); })()), "img", array()), 'row', array("attr" => array("requirsed" => "false")));
        echo "
                    ";
        // line 22
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 22, $this->getSourceContext()); })()), "name", array()), 'row', array("attr" => array("requidred" => "false")));
        echo "
                    
                    ";
        // line 24
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 24, $this->getSourceContext()); })()), "birthdate", array()), 'row', array("attr" => array("requdired" => "false")));
        echo "
                    ";
        // line 25
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 25, $this->getSourceContext()); })()), "address", array()), 'row', array("attr" => array("requidred" => "false")));
        echo "
                    ";
        // line 26
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 26, $this->getSourceContext()); })()), "description", array()), 'row', array("attr" => array("requdired" => "false")));
        echo "
                    <button class=\"btn btn-primary\" type=\"submit\">Update!</button>
        </div>
    </div>
   
";
        // line 31
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 31, $this->getSourceContext()); })()), 'form_end');
        echo "
    



";
        
        $__internal_f227d6edb7fde8425503f9031e535bc327ea64d3faf0aecf25a963b114d6e517->leave($__internal_f227d6edb7fde8425503f9031e535bc327ea64d3faf0aecf25a963b114d6e517_prof);

        
        $__internal_ed56464c9948eca98e064c7dc15e40e793347c0f9bde64a794f4d3501e90f7b7->leave($__internal_ed56464c9948eca98e064c7dc15e40e793347c0f9bde64a794f4d3501e90f7b7_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:User:editprofile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 31,  107 => 26,  103 => 25,  99 => 24,  94 => 22,  90 => 21,  85 => 20,  78 => 15,  73 => 9,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::loggedin.html.twig\" %}

{% block title %}Register{% endblock %}

{% block body %}



{{ form_start(form) }}
  
    {#{{ form_row(form.username) }}
    {{ form_row(form.plainPassword.first) }}
    {{ form_row(form.plainPassword.second) }}
    {{ form_row(form.email) }}#}

    <div class=\"row \">
        <div class = \"\" style=\"margin-left: 300px; margin-right:300px; margin-top:50; background-color: #19334d; padding: 10px\">
            <center><h2 style=\"color: white;\">Edit Profile</h2></center>
           
                    <img src=\"{{asset('uploads/image/')}}{{userimg}}\" style=\"max-height:100px; max-width:100px;\"/>
                    {{ form_row(form.img,  {'attr': {'requirsed': 'false'}}) }}
                    {{ form_row(form.name,  {'attr': {'requidred': 'false'}}) }}
                    
                    {{ form_row(form.birthdate,  {'attr': {'requdired': 'false'}}) }}
                    {{ form_row(form.address,  {'attr': {'requidred': 'false'}}) }}
                    {{ form_row(form.description,  {'attr': {'requdired': 'false'}}) }}
                    <button class=\"btn btn-primary\" type=\"submit\">Update!</button>
        </div>
    </div>
   
{{ form_end(form) }}
    



{% endblock %}", "AppBundle:User:editprofile.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/User/editprofile.html.twig");
    }
}
