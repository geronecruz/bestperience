<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_42ad1cda4129fb7dd270a17c73e9d777ce83a5593fdd83dab77160909243549d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0d8d5efe47315df37928ac7a4a55a5337eb3c10e0391f7d91dd9a8534e2d2f67 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0d8d5efe47315df37928ac7a4a55a5337eb3c10e0391f7d91dd9a8534e2d2f67->enter($__internal_0d8d5efe47315df37928ac7a4a55a5337eb3c10e0391f7d91dd9a8534e2d2f67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_6fe676fcbf25ff5cb1f4bc1f2dc2d84a8f04bb2949b2ff2e7d0dc9e357724288 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6fe676fcbf25ff5cb1f4bc1f2dc2d84a8f04bb2949b2ff2e7d0dc9e357724288->enter($__internal_6fe676fcbf25ff5cb1f4bc1f2dc2d84a8f04bb2949b2ff2e7d0dc9e357724288_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_0d8d5efe47315df37928ac7a4a55a5337eb3c10e0391f7d91dd9a8534e2d2f67->leave($__internal_0d8d5efe47315df37928ac7a4a55a5337eb3c10e0391f7d91dd9a8534e2d2f67_prof);

        
        $__internal_6fe676fcbf25ff5cb1f4bc1f2dc2d84a8f04bb2949b2ff2e7d0dc9e357724288->leave($__internal_6fe676fcbf25ff5cb1f4bc1f2dc2d84a8f04bb2949b2ff2e7d0dc9e357724288_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_2dc0965287f75440319422642640cefdf89643591bede75306d4b64866684a5f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2dc0965287f75440319422642640cefdf89643591bede75306d4b64866684a5f->enter($__internal_2dc0965287f75440319422642640cefdf89643591bede75306d4b64866684a5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_e1096fa73b2340b899042f0a2a0b8dda7097cda54c8643f5387bd0347027ee6f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1096fa73b2340b899042f0a2a0b8dda7097cda54c8643f5387bd0347027ee6f->enter($__internal_e1096fa73b2340b899042f0a2a0b8dda7097cda54c8643f5387bd0347027ee6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_e1096fa73b2340b899042f0a2a0b8dda7097cda54c8643f5387bd0347027ee6f->leave($__internal_e1096fa73b2340b899042f0a2a0b8dda7097cda54c8643f5387bd0347027ee6f_prof);

        
        $__internal_2dc0965287f75440319422642640cefdf89643591bede75306d4b64866684a5f->leave($__internal_2dc0965287f75440319422642640cefdf89643591bede75306d4b64866684a5f_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_fa9275caa6fbd42cf0035efdcf0783e9df0b14ab7cd8e23f708880e5dacc4913 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa9275caa6fbd42cf0035efdcf0783e9df0b14ab7cd8e23f708880e5dacc4913->enter($__internal_fa9275caa6fbd42cf0035efdcf0783e9df0b14ab7cd8e23f708880e5dacc4913_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_3d2e76743610f9e31c21aa90de60c361e4edf4f02a3719e8e3690c6e1e190951 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3d2e76743610f9e31c21aa90de60c361e4edf4f02a3719e8e3690c6e1e190951->enter($__internal_3d2e76743610f9e31c21aa90de60c361e4edf4f02a3719e8e3690c6e1e190951_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_3d2e76743610f9e31c21aa90de60c361e4edf4f02a3719e8e3690c6e1e190951->leave($__internal_3d2e76743610f9e31c21aa90de60c361e4edf4f02a3719e8e3690c6e1e190951_prof);

        
        $__internal_fa9275caa6fbd42cf0035efdcf0783e9df0b14ab7cd8e23f708880e5dacc4913->leave($__internal_fa9275caa6fbd42cf0035efdcf0783e9df0b14ab7cd8e23f708880e5dacc4913_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_43f084cefddbec7fed3aed74171574ddcf80bc0868ac391a5f34d354a25b28ac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_43f084cefddbec7fed3aed74171574ddcf80bc0868ac391a5f34d354a25b28ac->enter($__internal_43f084cefddbec7fed3aed74171574ddcf80bc0868ac391a5f34d354a25b28ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_eb6ff3ac60b1f665c70d664363c6bd283c457199ea58c0b0f07dfad13e1385d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb6ff3ac60b1f665c70d664363c6bd283c457199ea58c0b0f07dfad13e1385d4->enter($__internal_eb6ff3ac60b1f665c70d664363c6bd283c457199ea58c0b0f07dfad13e1385d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_eb6ff3ac60b1f665c70d664363c6bd283c457199ea58c0b0f07dfad13e1385d4->leave($__internal_eb6ff3ac60b1f665c70d664363c6bd283c457199ea58c0b0f07dfad13e1385d4_prof);

        
        $__internal_43f084cefddbec7fed3aed74171574ddcf80bc0868ac391a5f34d354a25b28ac->leave($__internal_43f084cefddbec7fed3aed74171574ddcf80bc0868ac391a5f34d354a25b28ac_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "/home/ron/bestperience/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
