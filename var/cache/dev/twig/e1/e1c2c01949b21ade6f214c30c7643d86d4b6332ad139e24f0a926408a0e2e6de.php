<?php

/* AppBundle:User:myAccount.html.twig */
class __TwigTemplate_6781c79a5c7ed03a4f9fc4576c55344254f5c60502a36da71cbfafc7f451aef3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("::loggedin.html.twig", "AppBundle:User:myAccount.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'style' => array($this, 'block_style'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::loggedin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ffd37202e02a08969c537f2f65762bc30be964c1af269031681c0270f31024be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ffd37202e02a08969c537f2f65762bc30be964c1af269031681c0270f31024be->enter($__internal_ffd37202e02a08969c537f2f65762bc30be964c1af269031681c0270f31024be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:myAccount.html.twig"));

        $__internal_15d3fdd275b53032c533bf8b623fcc63a5b3c921fde62ad832a72e8c0ecafede = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15d3fdd275b53032c533bf8b623fcc63a5b3c921fde62ad832a72e8c0ecafede->enter($__internal_15d3fdd275b53032c533bf8b623fcc63a5b3c921fde62ad832a72e8c0ecafede_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:myAccount.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ffd37202e02a08969c537f2f65762bc30be964c1af269031681c0270f31024be->leave($__internal_ffd37202e02a08969c537f2f65762bc30be964c1af269031681c0270f31024be_prof);

        
        $__internal_15d3fdd275b53032c533bf8b623fcc63a5b3c921fde62ad832a72e8c0ecafede->leave($__internal_15d3fdd275b53032c533bf8b623fcc63a5b3c921fde62ad832a72e8c0ecafede_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_6577fedb9a4455b6938ac9a7be4091f8772f84a94da85e1cc71e3127904a93bf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6577fedb9a4455b6938ac9a7be4091f8772f84a94da85e1cc71e3127904a93bf->enter($__internal_6577fedb9a4455b6938ac9a7be4091f8772f84a94da85e1cc71e3127904a93bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_3bd46968d1f6cab6208735951abc7bd7ddcd2ecf07b5e8a2a20346fc15c2efba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3bd46968d1f6cab6208735951abc7bd7ddcd2ecf07b5e8a2a20346fc15c2efba->enter($__internal_3bd46968d1f6cab6208735951abc7bd7ddcd2ecf07b5e8a2a20346fc15c2efba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Articles";
        
        $__internal_3bd46968d1f6cab6208735951abc7bd7ddcd2ecf07b5e8a2a20346fc15c2efba->leave($__internal_3bd46968d1f6cab6208735951abc7bd7ddcd2ecf07b5e8a2a20346fc15c2efba_prof);

        
        $__internal_6577fedb9a4455b6938ac9a7be4091f8772f84a94da85e1cc71e3127904a93bf->leave($__internal_6577fedb9a4455b6938ac9a7be4091f8772f84a94da85e1cc71e3127904a93bf_prof);

    }

    // line 5
    public function block_style($context, array $blocks = array())
    {
        $__internal_6a4c09e896d2e257746a1c1dc0161a1e57fb71a9deefa4b01c3f7f04e10c593f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6a4c09e896d2e257746a1c1dc0161a1e57fb71a9deefa4b01c3f7f04e10c593f->enter($__internal_6a4c09e896d2e257746a1c1dc0161a1e57fb71a9deefa4b01c3f7f04e10c593f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        $__internal_f2a2b77da407663897805e08e93155efde223a302a750615da1b359001e86514 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f2a2b77da407663897805e08e93155efde223a302a750615da1b359001e86514->enter($__internal_f2a2b77da407663897805e08e93155efde223a302a750615da1b359001e86514_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/profile.css"), "html", null, true);
        echo "\"> ";
        
        $__internal_f2a2b77da407663897805e08e93155efde223a302a750615da1b359001e86514->leave($__internal_f2a2b77da407663897805e08e93155efde223a302a750615da1b359001e86514_prof);

        
        $__internal_6a4c09e896d2e257746a1c1dc0161a1e57fb71a9deefa4b01c3f7f04e10c593f->leave($__internal_6a4c09e896d2e257746a1c1dc0161a1e57fb71a9deefa4b01c3f7f04e10c593f_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_949d36e55b5323fc0df1a44aad9c1d8ba3078708e2f2160c6cd22def5edf3d71 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_949d36e55b5323fc0df1a44aad9c1d8ba3078708e2f2160c6cd22def5edf3d71->enter($__internal_949d36e55b5323fc0df1a44aad9c1d8ba3078708e2f2160c6cd22def5edf3d71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e0bf2e57fa34f8db0b12cfedfc113c86251178f8f38459904e02ae284657d051 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0bf2e57fa34f8db0b12cfedfc113c86251178f8f38459904e02ae284657d051->enter($__internal_e0bf2e57fa34f8db0b12cfedfc113c86251178f8f38459904e02ae284657d051_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "


<div class=\"whole-panel\">

<div class=\"left-panel\">
  <center><img class=\"profilepic \" src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("uploads/image/"), "html", null, true);
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 14, $this->getSourceContext()); })()), "img", array()), "html", null, true);
        echo "\">
  <h1><span class=\"lnr lnr-user\"></span>&nbsp;";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 15, $this->getSourceContext()); })()), "username", array()), "html", null, true);
        echo "</h1>
 <a class=\"editprof-button btn btn-default\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_update_userProfile", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 16, $this->getSourceContext()); })()), "id", array()))), "html", null, true);
        echo "\"><span class=\"lnr lnr-pencil\"></span>&nbsp;Edit Profile</a>
 <a class=\"editprof-button btn btn-default\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_changePassword", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 17, $this->getSourceContext()); })()), "id", array()))), "html", null, true);
        echo "\"><span class=\"lnr lnr-pencil\"></span>&nbsp;Change Password</a>
 
                    
 <div class=\"halfleft-panel\">
 <h2><span class=\"lnr lnr-pushpin\"></span>&nbsp;Profile</h2>

<h3><span class=\"lnr lnr-license\"></span>&nbsp;Name:</h3>
  <p>";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 24, $this->getSourceContext()); })()), "name", array()), "html", null, true);
        echo "</p>
  <h3><span class=\"lnr lnr-calendar-full\"></span>&nbsp;Birthdate:</h3>
  <p>  ";
        // line 26
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 26, $this->getSourceContext()); })()), "birthdate", array()), "M/d/Y"), "html", null, true);
        echo "</p>

  <h3><span class=\"lnr lnr-map-marker\"></span>&nbsp;Address:</h3>
  <p>";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 29, $this->getSourceContext()); })()), "address", array()), "html", null, true);
        echo "</p>

    <div class=\"description\">
        <h3>About me</h3>
        <p>";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 33, $this->getSourceContext()); })()), "description", array()), "html", null, true);
        echo "</p>
    </div>
   

</div>


 </center>
</div>

\t";
        // line 46
        echo "<div class=\"right-panel\">
    <div>
     <h2 > My Articles</h2>
     </div>
    <a class=\"editprof-button btn btn-default pull-right\" href=\"";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_post_add");
        echo "\"><span class=\"lnr lnr-pencil\"></span>&nbsp;Add Article</a>
<div class=\"right-panel-topbar\">

  ";
        // line 54
        echo "   ";
        // line 56
        echo " </div>
    ";
        // line 57
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["userpost"]) || array_key_exists("userpost", $context) ? $context["userpost"] : (function () { throw new Twig_Error_Runtime('Variable "userpost" does not exist.', 57, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["Post"]) {
            // line 58
            echo "
    



          <div class=\"margin\">
            <!-- ARTICLES -->             
            <div class=\"s-12 l-12\">
            
                <!-- ARTICLE 1 -->               
               <article class=\"post-";
            // line 68
            echo twig_escape_filter($this->env, (twig_random($this->env, 4) + 1), "html", null, true);
            echo " line\">
                  <!-- image -->                 
                  <div class=\"s-12 l-6 post-image\"> 
\t\t\t\t  \t<center>                  
                     <a href=\"";
            // line 72
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_post_view", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "id", array()))), "html", null, true);
            echo "\">
                     <img src=\"";
            // line 73
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("uploads/image/"), "html", null, true);
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "img", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "title", array()), "html", null, true);
            echo "\" style=\"max-height:300px; max-width: 200px;\">
                     </a>
\t\t\t\t\t </center>                
                  </div>
                  <!-- text -->                 
                  <div class=\"s-12 l-5 post-text\">
                     <a href=\"";
            // line 79
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_post_view", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "id", array()))), "html", null, true);
            echo "\">
                        <h2 style=\"color: white;\">";
            // line 80
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "title", array()), "html", null, true);
            echo " </h2>
                     </a>
                     <p>";
            // line 82
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "Content", array()), "html", null, true);
            echo "</p>
                  </div>
                  <!-- date -->                 
                  <div class=\"s-12 l-1 post-date\">
\t\t\t\t  \t
                     <p class=\"date\"";
            // line 87
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "date", array()), "d"), "html", null, true);
            echo "</p>
                     <p class=\"month\">mar ";
            // line 88
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "date", array()), "M"), "html", null, true);
            echo "</p>
                  </div>
                  <a class=\"w3-color-white\"   style=\"color: white;\" title=\"edit\" href=\"";
            // line 90
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_post_update", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "id", array()))), "html", null, true);
            echo "\"> edit</i></a>&nbsp; 
\t\t\t\t    <a class=\"text-danger\" style=\"color: white;\" title=\"Remove\" href=\"";
            // line 91
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_post_remove", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "id", array()))), "html", null, true);
            echo "\"> remove</a>
               </article>
               
            </div>
           
         </div>

         ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['Post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 98
        echo " 






    
";
        
        $__internal_e0bf2e57fa34f8db0b12cfedfc113c86251178f8f38459904e02ae284657d051->leave($__internal_e0bf2e57fa34f8db0b12cfedfc113c86251178f8f38459904e02ae284657d051_prof);

        
        $__internal_949d36e55b5323fc0df1a44aad9c1d8ba3078708e2f2160c6cd22def5edf3d71->leave($__internal_949d36e55b5323fc0df1a44aad9c1d8ba3078708e2f2160c6cd22def5edf3d71_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:User:myAccount.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  251 => 98,  237 => 91,  233 => 90,  228 => 88,  224 => 87,  216 => 82,  211 => 80,  207 => 79,  195 => 73,  191 => 72,  184 => 68,  172 => 58,  168 => 57,  165 => 56,  163 => 54,  157 => 50,  151 => 46,  138 => 33,  131 => 29,  125 => 26,  120 => 24,  110 => 17,  106 => 16,  102 => 15,  97 => 14,  89 => 8,  80 => 7,  60 => 5,  42 => 4,  11 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("
{% extends \"::loggedin.html.twig\" %}

{% block title %}Articles{% endblock %}
{% block style%}<link rel=\"stylesheet\" href=\"{{asset('assets/css/profile.css')}}\"> {%endblock%}

{% block body %}



<div class=\"whole-panel\">

<div class=\"left-panel\">
  <center><img class=\"profilepic \" src=\"{{asset('uploads/image/')}}{{users.img}}\">
  <h1><span class=\"lnr lnr-user\"></span>&nbsp;{{users.username}}</h1>
 <a class=\"editprof-button btn btn-default\" href=\"{{path('admin_update_userProfile',{id:users.id})}}\"><span class=\"lnr lnr-pencil\"></span>&nbsp;Edit Profile</a>
 <a class=\"editprof-button btn btn-default\" href=\"{{path('admin_changePassword',{id:users.id})}}\"><span class=\"lnr lnr-pencil\"></span>&nbsp;Change Password</a>
 
                    
 <div class=\"halfleft-panel\">
 <h2><span class=\"lnr lnr-pushpin\"></span>&nbsp;Profile</h2>

<h3><span class=\"lnr lnr-license\"></span>&nbsp;Name:</h3>
  <p>{{users.name}}</p>
  <h3><span class=\"lnr lnr-calendar-full\"></span>&nbsp;Birthdate:</h3>
  <p>  {{users.birthdate|date('M/d/Y')}}</p>

  <h3><span class=\"lnr lnr-map-marker\"></span>&nbsp;Address:</h3>
  <p>{{users.address}}</p>

    <div class=\"description\">
        <h3>About me</h3>
        <p>{{users.description}}</p>
    </div>
   

</div>


 </center>
</div>

\t{# <a class=\"\" title=\"View\" href=\"{{path('user_post_view',{id:Post.id})}}\">view</a> 
\t\t\t\t<a class=\"\" title=\"edit\" href=\"{{path('user_post_update',{id:Post.id})}}\"> edit</i></a> 
\t\t\t\t<a class=\"text-danger\" title=\"Remove\" href=\"{{path('user_post_remove',{id:Post.id})}}\"> emove</a> #}
<div class=\"right-panel\">
    <div>
     <h2 > My Articles</h2>
     </div>
    <a class=\"editprof-button btn btn-default pull-right\" href=\"{{path('user_post_add')}}\"><span class=\"lnr lnr-pencil\"></span>&nbsp;Add Article</a>
<div class=\"right-panel-topbar\">

  {# <button class=\"add-post-btn\" onclick=\"location.href='addpost.php';\"><span class=\"lnr lnr-plus-circle\"></span>&nbsp;Add Post</button> #}
   {# <button class=\"add-post-btn\" onclick=\"location.href='addpost.php';\"><span class=\"lnr lnr-pencil\"></span>&nbsp;Edit Post</button>
    <button class=\"add-post-btn\" onclick=\"location.href='profile.php';\"><span class=\"lnr lnr-trash\"></span>&nbsp;Delete Post</button> #}
 </div>
    {%for Post in userpost%}

    



          <div class=\"margin\">
            <!-- ARTICLES -->             
            <div class=\"s-12 l-12\">
            
                <!-- ARTICLE 1 -->               
               <article class=\"post-{{random(4)+1}} line\">
                  <!-- image -->                 
                  <div class=\"s-12 l-6 post-image\"> 
\t\t\t\t  \t<center>                  
                     <a href=\"{{path('user_post_view',{id:Post.id})}}\">
                     <img src=\"{{asset('uploads/image/')}}{{Post.img}}\" alt=\"{{Post.title}}\" style=\"max-height:300px; max-width: 200px;\">
                     </a>
\t\t\t\t\t </center>                
                  </div>
                  <!-- text -->                 
                  <div class=\"s-12 l-5 post-text\">
                     <a href=\"{{path('user_post_view',{id:Post.id})}}\">
                        <h2 style=\"color: white;\">{{Post.title}} </h2>
                     </a>
                     <p>{{Post.Content}}</p>
                  </div>
                  <!-- date -->                 
                  <div class=\"s-12 l-1 post-date\">
\t\t\t\t  \t
                     <p class=\"date\"{{Post.date|date('d')}}</p>
                     <p class=\"month\">mar {{Post.date|date('M')}}</p>
                  </div>
                  <a class=\"w3-color-white\"   style=\"color: white;\" title=\"edit\" href=\"{{path('user_post_update',{id:Post.id})}}\"> edit</i></a>&nbsp; 
\t\t\t\t    <a class=\"text-danger\" style=\"color: white;\" title=\"Remove\" href=\"{{path('user_post_remove',{id:Post.id})}}\"> remove</a>
               </article>
               
            </div>
           
         </div>

         {%endfor%} 






    
{% endblock %}
", "AppBundle:User:myAccount.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/User/myAccount.html.twig");
    }
}
