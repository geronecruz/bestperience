<?php

/* ::loggedin.html.twig */
class __TwigTemplate_98a09017b4e7c9994d05eaf3d36133c89ed9fa346e25a5332a2999b105e9e637 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2c5cb297bae875c38443b882df5db2ea6bf3b405be6f604ebe6d90b6603e6f37 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2c5cb297bae875c38443b882df5db2ea6bf3b405be6f604ebe6d90b6603e6f37->enter($__internal_2c5cb297bae875c38443b882df5db2ea6bf3b405be6f604ebe6d90b6603e6f37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::loggedin.html.twig"));

        $__internal_06fb34e98966ece8fbd619d1d30980eb76eff3b93bf4dda30421697264efcbe7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_06fb34e98966ece8fbd619d1d30980eb76eff3b93bf4dda30421697264efcbe7->enter($__internal_06fb34e98966ece8fbd619d1d30980eb76eff3b93bf4dda30421697264efcbe7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::loggedin.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en-US\">
   <head>
      <meta charset=\"UTF-8\">
      <meta name=\"viewport\" content=\"width=device-width\" />
      <title>BestPerience | Share your Best Experience
      </title>
      <meta name=\"description\" content=\"Download free amazing responsive Fashion Blog template.\"/>
      <meta name=\"keywords\" content=\"free, responsive, blog, fashion, web site, template\"/>
\t  <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/bootstrap.css"), "html", null, true);
        echo "\">
      <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/components.css"), "html", null, true);
        echo "\">
      <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/responsee.css"), "html", null, true);
        echo "\">
\t\t";
        // line 13
        $this->displayBlock('style', $context, $blocks);
        // line 15
        echo "      <!-- CUSTOM STYLE -->       
      <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
      <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/template-style.css"), "html", null, true);
        echo "\">
      <script type=\"text/javascript\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery-1.8.3.min.js"), "html", null, true);
        echo "\"></script>
      <script type=\"text/javascript\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>    
      <script type=\"text/javascript\" src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/modernizr.js"), "html", null, true);
        echo "\"></script>
      <script type=\"text/javascript\" src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/responsee.js"), "html", null, true);
        echo "\"></script>          
      <!--[if lt IE 9]> 
      <script src=\"http://html5shiv.googlecode.com/svn/trunk/html5.js\"></script> 
      <![endif]-->  


      <!--My Css-->   
      <link href=\"https://fonts.googleapis.com/css?family=Fjalla+One\" rel=\"stylesheet\">
      <link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\">

      <!--Linear Icon-->
    <script src=\"https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js\"></script>
    <link rel=\"stylesheet\" href=\"https://cdn.linearicons.com/free/1.0.0/icon-font.min.css\">
<!--Fontawesome-->
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">


   </head>
   <body class=\"size-1140\">
      <!-- TOP NAV WITH LOGO -->          
      <header class=\"margin-bottom\">
         <div class=\"line\">
            <nav>
               <div class=\"top-nav\">
                  <p class=\"nav-text\"></p>
                  <a class=\"logo\" href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_post_list");
        echo "\">            
                  Best<span>Perience</span>
                  </a>            
                  <h1>Share your best experience ever!</h1>
                  <ul class=\"top-ul right\">
                     <li>            
                        <a href=\"";
        // line 52
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_post_list");
        echo "\">Home</a>            
                     </li>
                     <li>            
                        <a href=\"";
        // line 55
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_myaccount");
        echo "\">My Profile</a>            
                     </li>
                     <li>            
                        <a href=\"";
        // line 58
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_post_add");
        echo "\">Add Article</a>            
                     </li>
                              <li>            
                        <a href=\"";
        // line 61
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("about");
        echo "\">About</a>            
                     </li>
                     ";
        // line 66
        echo "                     <li>            
                        <a href=\"";
        // line 67
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\">Logout</a>            
                     </li>
                     <div class=\"social right\">\t                 
                     </div>
                  </ul>
               </div>
            </nav>
         </div>
      </header>
      <!-- MAIN SECTION --> 
\t  
 ";
        // line 79
        echo "\t\t\t ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 79, $this->getSourceContext()); })()), "flashes", array()));
        foreach ($context['_seq'] as $context["label"] => $context["messages"]) {
            // line 80
            echo "\t\t\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 81
                echo "\t\t\t\t\t\t\t<div class=\"alert alert-notice\">
\t\t\t\t\t\t\t";
                // line 82
                echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 85
            echo "\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['label'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "\t
\t\t\t\t";
        // line 86
        $this->displayBlock('body', $context, $blocks);
        // line 93
        echo "
\t\t";
        // line 95
        echo "      
   </body>
</html>";
        
        $__internal_2c5cb297bae875c38443b882df5db2ea6bf3b405be6f604ebe6d90b6603e6f37->leave($__internal_2c5cb297bae875c38443b882df5db2ea6bf3b405be6f604ebe6d90b6603e6f37_prof);

        
        $__internal_06fb34e98966ece8fbd619d1d30980eb76eff3b93bf4dda30421697264efcbe7->leave($__internal_06fb34e98966ece8fbd619d1d30980eb76eff3b93bf4dda30421697264efcbe7_prof);

    }

    // line 13
    public function block_style($context, array $blocks = array())
    {
        $__internal_002d0613469d23c63d5c8991aa333c136d8f8671b1300b281ad4f0ad4275159d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_002d0613469d23c63d5c8991aa333c136d8f8671b1300b281ad4f0ad4275159d->enter($__internal_002d0613469d23c63d5c8991aa333c136d8f8671b1300b281ad4f0ad4275159d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        $__internal_11bd458afedae098424dca819a2de885a32732b9e7b231f557f9855546101f5f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_11bd458afedae098424dca819a2de885a32732b9e7b231f557f9855546101f5f->enter($__internal_11bd458afedae098424dca819a2de885a32732b9e7b231f557f9855546101f5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 14
        echo "\t\t";
        
        $__internal_11bd458afedae098424dca819a2de885a32732b9e7b231f557f9855546101f5f->leave($__internal_11bd458afedae098424dca819a2de885a32732b9e7b231f557f9855546101f5f_prof);

        
        $__internal_002d0613469d23c63d5c8991aa333c136d8f8671b1300b281ad4f0ad4275159d->leave($__internal_002d0613469d23c63d5c8991aa333c136d8f8671b1300b281ad4f0ad4275159d_prof);

    }

    // line 86
    public function block_body($context, array $blocks = array())
    {
        $__internal_d13da8f42a8add8970fe69ba8e5ae911e12553d0923b6db27eef7932c99af121 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d13da8f42a8add8970fe69ba8e5ae911e12553d0923b6db27eef7932c99af121->enter($__internal_d13da8f42a8add8970fe69ba8e5ae911e12553d0923b6db27eef7932c99af121_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_00e41fe97268fe0bff8311782165b4bd8bbcfee7e19e1cce44d766e7d55e11e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_00e41fe97268fe0bff8311782165b4bd8bbcfee7e19e1cce44d766e7d55e11e3->enter($__internal_00e41fe97268fe0bff8311782165b4bd8bbcfee7e19e1cce44d766e7d55e11e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 87
        echo "\t
\t\t\t

    \t
  
        \t\t";
        
        $__internal_00e41fe97268fe0bff8311782165b4bd8bbcfee7e19e1cce44d766e7d55e11e3->leave($__internal_00e41fe97268fe0bff8311782165b4bd8bbcfee7e19e1cce44d766e7d55e11e3_prof);

        
        $__internal_d13da8f42a8add8970fe69ba8e5ae911e12553d0923b6db27eef7932c99af121->leave($__internal_d13da8f42a8add8970fe69ba8e5ae911e12553d0923b6db27eef7932c99af121_prof);

    }

    public function getTemplateName()
    {
        return "::loggedin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  224 => 87,  215 => 86,  205 => 14,  196 => 13,  184 => 95,  181 => 93,  179 => 86,  171 => 85,  162 => 82,  159 => 81,  154 => 80,  149 => 79,  135 => 67,  132 => 66,  127 => 61,  121 => 58,  115 => 55,  109 => 52,  100 => 46,  72 => 21,  68 => 20,  64 => 19,  60 => 18,  56 => 17,  52 => 15,  50 => 13,  46 => 12,  42 => 11,  38 => 10,  27 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en-US\">
   <head>
      <meta charset=\"UTF-8\">
      <meta name=\"viewport\" content=\"width=device-width\" />
      <title>BestPerience | Share your Best Experience
      </title>
      <meta name=\"description\" content=\"Download free amazing responsive Fashion Blog template.\"/>
      <meta name=\"keywords\" content=\"free, responsive, blog, fashion, web site, template\"/>
\t  <link rel=\"stylesheet\" href=\"{{asset('assets/css/bootstrap.css')}}\">
      <link rel=\"stylesheet\" href=\"{{asset('assets/css/components.css')}}\">
      <link rel=\"stylesheet\" href=\"{{asset('assets/css/responsee.css')}}\">
\t\t{% block style %}
\t\t{% endblock %}
      <!-- CUSTOM STYLE -->       
      <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
      <link rel=\"stylesheet\" href=\"{{asset('assets/css/template-style.css')}}\">
      <script type=\"text/javascript\" src=\"{{asset('assets/js/jquery-1.8.3.min.js')}}\"></script>
      <script type=\"text/javascript\" src=\"{{asset('assets/js/jquery-ui.min.js')}}\"></script>    
      <script type=\"text/javascript\" src=\"{{asset('assets/js/modernizr.js')}}\"></script>
      <script type=\"text/javascript\" src=\"{{asset('assets/js/responsee.js')}}\"></script>          
      <!--[if lt IE 9]> 
      <script src=\"http://html5shiv.googlecode.com/svn/trunk/html5.js\"></script> 
      <![endif]-->  


      <!--My Css-->   
      <link href=\"https://fonts.googleapis.com/css?family=Fjalla+One\" rel=\"stylesheet\">
      <link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\">

      <!--Linear Icon-->
    <script src=\"https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js\"></script>
    <link rel=\"stylesheet\" href=\"https://cdn.linearicons.com/free/1.0.0/icon-font.min.css\">
<!--Fontawesome-->
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">


   </head>
   <body class=\"size-1140\">
      <!-- TOP NAV WITH LOGO -->          
      <header class=\"margin-bottom\">
         <div class=\"line\">
            <nav>
               <div class=\"top-nav\">
                  <p class=\"nav-text\"></p>
                  <a class=\"logo\" href=\"{{path('user_post_list')}}\">            
                  Best<span>Perience</span>
                  </a>            
                  <h1>Share your best experience ever!</h1>
                  <ul class=\"top-ul right\">
                     <li>            
                        <a href=\"{{path('user_post_list')}}\">Home</a>            
                     </li>
                     <li>            
                        <a href=\"{{path('user_myaccount')}}\">My Profile</a>            
                     </li>
                     <li>            
                        <a href=\"{{path('user_post_add')}}\">Add Article</a>            
                     </li>
                              <li>            
                        <a href=\"{{path('about')}}\">About</a>            
                     </li>
                     {# <li>            
                        <a href=\"{{path('contactus')}}\">Contact</a>            
                     </li> #}
                     <li>            
                        <a href=\"{{path('login')}}\">Logout</a>            
                     </li>
                     <div class=\"social right\">\t                 
                     </div>
                  </ul>
               </div>
            </nav>
         </div>
      </header>
      <!-- MAIN SECTION --> 
\t  
 {# <section id=\"home-section\" class=\"line\">                  #}
\t\t\t {% for label, messages in app.flashes %}
\t\t\t\t\t\t{% for message in messages %}
\t\t\t\t\t\t\t<div class=\"alert alert-notice\">
\t\t\t\t\t\t\t{{ message }}
\t\t\t\t\t\t</div>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t{% endfor %}\t
\t\t\t\t{% block body %}
\t
\t\t\t

    \t
  
        \t\t{% endblock %}

\t\t{# </section> #}
      
   </body>
</html>", "::loggedin.html.twig", "/home/ron/bestperience/app/Resources/views/loggedin.html.twig");
    }
}
