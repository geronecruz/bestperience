<?php

/* ::base.html.twig */
class __TwigTemplate_3698a5049a95d96cd35abee4dbe42043aec1c3ce4b593cfad74f69e93521450f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_94d122a32009873e4e8f6cbdda11549ad0ff4f1c38e1a34a22fd92227bdb85db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_94d122a32009873e4e8f6cbdda11549ad0ff4f1c38e1a34a22fd92227bdb85db->enter($__internal_94d122a32009873e4e8f6cbdda11549ad0ff4f1c38e1a34a22fd92227bdb85db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        $__internal_f78ca66847d2669f39138f9cc45acb2f53354c2bf2b66f36c9e9491ead7a6f15 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f78ca66847d2669f39138f9cc45acb2f53354c2bf2b66f36c9e9491ead7a6f15->enter($__internal_f78ca66847d2669f39138f9cc45acb2f53354c2bf2b66f36c9e9491ead7a6f15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en-US\">
   <head>
      <meta charset=\"UTF-8\">
      <meta name=\"viewport\" content=\"width=device-width\" />
      <title>BestPerience | Share your Best Experience
      </title>
      <meta name=\"description\" content=\"Download free amazing responsive Fashion Blog template.\"/>
      <meta name=\"keywords\" content=\"free, responsive, blog, fashion, web site, template\"/>
\t  <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/bootstrap.css"), "html", null, true);
        echo "\">
      <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/components.css"), "html", null, true);
        echo "\">
      <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/responsee.css"), "html", null, true);
        echo "\">
\t\t";
        // line 13
        $this->displayBlock('style', $context, $blocks);
        // line 15
        echo "      <!-- CUSTOM STYLE -->       
      <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
      <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/template-style.css"), "html", null, true);
        echo "\">
      <script type=\"text/javascript\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery-1.8.3.min.js"), "html", null, true);
        echo "\"></script>
      <script type=\"text/javascript\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>    
      <script type=\"text/javascript\" src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/modernizr.js"), "html", null, true);
        echo "\"></script>
      <script type=\"text/javascript\" src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/responsee.js"), "html", null, true);
        echo "\"></script>          
      <!--[if lt IE 9]> 
      <script src=\"http://html5shiv.googlecode.com/svn/trunk/html5.js\"></script> 
      <![endif]-->  


      <!--My Css-->   
      <link href=\"https://fonts.googleapis.com/css?family=Fjalla+One\" rel=\"stylesheet\">
      <link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\">

      <!--Linear Icon-->
    <script src=\"https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js\"></script>
    <link rel=\"stylesheet\" href=\"https://cdn.linearicons.com/free/1.0.0/icon-font.min.css\">
<!--Fontawesome-->
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">


   </head>
   <body class=\"size-1140\">
      <!-- TOP NAV WITH LOGO -->          
      <header class=\"margin-bottom\">
         <div class=\"line\">
            <nav>
               <div class=\"top-nav\">
                  <p class=\"nav-text\"></p>
                  <a class=\"logo\" href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_post_list");
        echo "\">            
                  Best<span>Perience</span>
                  </a>            
                  <h1>Share your best experience ever!</h1>
                  <ul class=\"top-ul right\">

                              <li>            
                        <a href=\"";
        // line 53
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("about");
        echo "\">About</a>            
                     </li>
                     ";
        // line 58
        echo "                     <li>            
                        <a href=\"";
        // line 59
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\">Login</a>            
                     </li>
                     <div class=\"social right\">\t                 
                     </div>
                  </ul>
               </div>
            </nav>
         </div>
      </header>
      <!-- MAIN SECTION --> 
\t  
 <section id=\"home-section\" class=\"line\">                 
\t\t\t ";
        // line 71
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 71, $this->getSourceContext()); })()), "flashes", array()));
        foreach ($context['_seq'] as $context["label"] => $context["messages"]) {
            // line 72
            echo "\t\t\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 73
                echo "\t\t\t\t\t\t\t<div class=\"alert alert-notice\">
\t\t\t\t\t\t\t";
                // line 74
                echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 77
            echo "\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['label'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "\t
\t\t\t\t";
        // line 78
        $this->displayBlock('body', $context, $blocks);
        // line 85
        echo "
\t\t</section>
      <!-- FOOTER -->       

   </body>
</html>";
        
        $__internal_94d122a32009873e4e8f6cbdda11549ad0ff4f1c38e1a34a22fd92227bdb85db->leave($__internal_94d122a32009873e4e8f6cbdda11549ad0ff4f1c38e1a34a22fd92227bdb85db_prof);

        
        $__internal_f78ca66847d2669f39138f9cc45acb2f53354c2bf2b66f36c9e9491ead7a6f15->leave($__internal_f78ca66847d2669f39138f9cc45acb2f53354c2bf2b66f36c9e9491ead7a6f15_prof);

    }

    // line 13
    public function block_style($context, array $blocks = array())
    {
        $__internal_d8fc8c906f752546ec9aa8b45f87969606cc143e3cb8fde1ffcc3ee544a14488 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d8fc8c906f752546ec9aa8b45f87969606cc143e3cb8fde1ffcc3ee544a14488->enter($__internal_d8fc8c906f752546ec9aa8b45f87969606cc143e3cb8fde1ffcc3ee544a14488_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        $__internal_ce01cc066d8977188e2ad02fb53f8717b8661573e6681d64a7778928cddc4dbd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce01cc066d8977188e2ad02fb53f8717b8661573e6681d64a7778928cddc4dbd->enter($__internal_ce01cc066d8977188e2ad02fb53f8717b8661573e6681d64a7778928cddc4dbd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 14
        echo "\t\t";
        
        $__internal_ce01cc066d8977188e2ad02fb53f8717b8661573e6681d64a7778928cddc4dbd->leave($__internal_ce01cc066d8977188e2ad02fb53f8717b8661573e6681d64a7778928cddc4dbd_prof);

        
        $__internal_d8fc8c906f752546ec9aa8b45f87969606cc143e3cb8fde1ffcc3ee544a14488->leave($__internal_d8fc8c906f752546ec9aa8b45f87969606cc143e3cb8fde1ffcc3ee544a14488_prof);

    }

    // line 78
    public function block_body($context, array $blocks = array())
    {
        $__internal_4102328da92c938add675b696d3a6876960a42911d16776c545d6d9f102c8304 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4102328da92c938add675b696d3a6876960a42911d16776c545d6d9f102c8304->enter($__internal_4102328da92c938add675b696d3a6876960a42911d16776c545d6d9f102c8304_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_94f98a86b4de78a5ec8f3d136deaf9b858f4c31b9179cd1438541c109ecc5490 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_94f98a86b4de78a5ec8f3d136deaf9b858f4c31b9179cd1438541c109ecc5490->enter($__internal_94f98a86b4de78a5ec8f3d136deaf9b858f4c31b9179cd1438541c109ecc5490_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 79
        echo "\t
\t\t\t

    \t
  
        \t\t";
        
        $__internal_94f98a86b4de78a5ec8f3d136deaf9b858f4c31b9179cd1438541c109ecc5490->leave($__internal_94f98a86b4de78a5ec8f3d136deaf9b858f4c31b9179cd1438541c109ecc5490_prof);

        
        $__internal_4102328da92c938add675b696d3a6876960a42911d16776c545d6d9f102c8304->leave($__internal_4102328da92c938add675b696d3a6876960a42911d16776c545d6d9f102c8304_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  207 => 79,  198 => 78,  188 => 14,  179 => 13,  164 => 85,  162 => 78,  154 => 77,  145 => 74,  142 => 73,  137 => 72,  133 => 71,  118 => 59,  115 => 58,  110 => 53,  100 => 46,  72 => 21,  68 => 20,  64 => 19,  60 => 18,  56 => 17,  52 => 15,  50 => 13,  46 => 12,  42 => 11,  38 => 10,  27 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en-US\">
   <head>
      <meta charset=\"UTF-8\">
      <meta name=\"viewport\" content=\"width=device-width\" />
      <title>BestPerience | Share your Best Experience
      </title>
      <meta name=\"description\" content=\"Download free amazing responsive Fashion Blog template.\"/>
      <meta name=\"keywords\" content=\"free, responsive, blog, fashion, web site, template\"/>
\t  <link rel=\"stylesheet\" href=\"{{asset('assets/css/bootstrap.css')}}\">
      <link rel=\"stylesheet\" href=\"{{asset('assets/css/components.css')}}\">
      <link rel=\"stylesheet\" href=\"{{asset('assets/css/responsee.css')}}\">
\t\t{% block style %}
\t\t{% endblock %}
      <!-- CUSTOM STYLE -->       
      <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
      <link rel=\"stylesheet\" href=\"{{asset('assets/css/template-style.css')}}\">
      <script type=\"text/javascript\" src=\"{{asset('assets/js/jquery-1.8.3.min.js')}}\"></script>
      <script type=\"text/javascript\" src=\"{{asset('assets/js/jquery-ui.min.js')}}\"></script>    
      <script type=\"text/javascript\" src=\"{{asset('assets/js/modernizr.js')}}\"></script>
      <script type=\"text/javascript\" src=\"{{asset('assets/js/responsee.js')}}\"></script>          
      <!--[if lt IE 9]> 
      <script src=\"http://html5shiv.googlecode.com/svn/trunk/html5.js\"></script> 
      <![endif]-->  


      <!--My Css-->   
      <link href=\"https://fonts.googleapis.com/css?family=Fjalla+One\" rel=\"stylesheet\">
      <link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\">

      <!--Linear Icon-->
    <script src=\"https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js\"></script>
    <link rel=\"stylesheet\" href=\"https://cdn.linearicons.com/free/1.0.0/icon-font.min.css\">
<!--Fontawesome-->
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">


   </head>
   <body class=\"size-1140\">
      <!-- TOP NAV WITH LOGO -->          
      <header class=\"margin-bottom\">
         <div class=\"line\">
            <nav>
               <div class=\"top-nav\">
                  <p class=\"nav-text\"></p>
                  <a class=\"logo\" href=\"{{path('user_post_list')}}\">            
                  Best<span>Perience</span>
                  </a>            
                  <h1>Share your best experience ever!</h1>
                  <ul class=\"top-ul right\">

                              <li>            
                        <a href=\"{{path('about')}}\">About</a>            
                     </li>
                     {# <li>            
                        <a href=\"{{path('contactus')}}\">Contact</a>            
                     </li> #}
                     <li>            
                        <a href=\"{{path('login')}}\">Login</a>            
                     </li>
                     <div class=\"social right\">\t                 
                     </div>
                  </ul>
               </div>
            </nav>
         </div>
      </header>
      <!-- MAIN SECTION --> 
\t  
 <section id=\"home-section\" class=\"line\">                 
\t\t\t {% for label, messages in app.flashes %}
\t\t\t\t\t\t{% for message in messages %}
\t\t\t\t\t\t\t<div class=\"alert alert-notice\">
\t\t\t\t\t\t\t{{ message }}
\t\t\t\t\t\t</div>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t{% endfor %}\t
\t\t\t\t{% block body %}
\t
\t\t\t

    \t
  
        \t\t{% endblock %}

\t\t</section>
      <!-- FOOTER -->       

   </body>
</html>", "::base.html.twig", "/home/ron/bestperience/app/Resources/views/base.html.twig");
    }
}
