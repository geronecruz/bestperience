<?php

/* AppBundle:Security:login.html.twig */
class __TwigTemplate_8ce222f2504323bfb4bdba79dd1a9d4059e553c1071a9fed533627b1627d7f68 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:Security:login.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'style' => array($this, 'block_style'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_65ab0ae40164978bd64d819bf985a42eb58695d4d2726e84dd38ad63ee764e9d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_65ab0ae40164978bd64d819bf985a42eb58695d4d2726e84dd38ad63ee764e9d->enter($__internal_65ab0ae40164978bd64d819bf985a42eb58695d4d2726e84dd38ad63ee764e9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Security:login.html.twig"));

        $__internal_7a34c36c1e9d44a65eccbf4f75398c2c698dcd48acd9379ea073975347b60e66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a34c36c1e9d44a65eccbf4f75398c2c698dcd48acd9379ea073975347b60e66->enter($__internal_7a34c36c1e9d44a65eccbf4f75398c2c698dcd48acd9379ea073975347b60e66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Security:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_65ab0ae40164978bd64d819bf985a42eb58695d4d2726e84dd38ad63ee764e9d->leave($__internal_65ab0ae40164978bd64d819bf985a42eb58695d4d2726e84dd38ad63ee764e9d_prof);

        
        $__internal_7a34c36c1e9d44a65eccbf4f75398c2c698dcd48acd9379ea073975347b60e66->leave($__internal_7a34c36c1e9d44a65eccbf4f75398c2c698dcd48acd9379ea073975347b60e66_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_581f7da34560a372d5d8c0d20fc161acb3d69a34509722e16739d71691ffdcf6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_581f7da34560a372d5d8c0d20fc161acb3d69a34509722e16739d71691ffdcf6->enter($__internal_581f7da34560a372d5d8c0d20fc161acb3d69a34509722e16739d71691ffdcf6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_5d5e70c40c9845c81416cedc6576642504dd11b0047a9fbba5d68e0cfbe1eb0c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d5e70c40c9845c81416cedc6576642504dd11b0047a9fbba5d68e0cfbe1eb0c->enter($__internal_5d5e70c40c9845c81416cedc6576642504dd11b0047a9fbba5d68e0cfbe1eb0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Login";
        
        $__internal_5d5e70c40c9845c81416cedc6576642504dd11b0047a9fbba5d68e0cfbe1eb0c->leave($__internal_5d5e70c40c9845c81416cedc6576642504dd11b0047a9fbba5d68e0cfbe1eb0c_prof);

        
        $__internal_581f7da34560a372d5d8c0d20fc161acb3d69a34509722e16739d71691ffdcf6->leave($__internal_581f7da34560a372d5d8c0d20fc161acb3d69a34509722e16739d71691ffdcf6_prof);

    }

    // line 5
    public function block_style($context, array $blocks = array())
    {
        $__internal_ed42b994c5736f8ca55ab1458e14f4615f534c2a3d2895b6f03efa3dbd1572e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed42b994c5736f8ca55ab1458e14f4615f534c2a3d2895b6f03efa3dbd1572e4->enter($__internal_ed42b994c5736f8ca55ab1458e14f4615f534c2a3d2895b6f03efa3dbd1572e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        $__internal_bb45a06fdf10bdf922ed05600661b7685584b4cdea4bb60621eb5f88fde92d2e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bb45a06fdf10bdf922ed05600661b7685584b4cdea4bb60621eb5f88fde92d2e->enter($__internal_bb45a06fdf10bdf922ed05600661b7685584b4cdea4bb60621eb5f88fde92d2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/login.css"), "html", null, true);
        echo "\">";
        
        $__internal_bb45a06fdf10bdf922ed05600661b7685584b4cdea4bb60621eb5f88fde92d2e->leave($__internal_bb45a06fdf10bdf922ed05600661b7685584b4cdea4bb60621eb5f88fde92d2e_prof);

        
        $__internal_ed42b994c5736f8ca55ab1458e14f4615f534c2a3d2895b6f03efa3dbd1572e4->leave($__internal_ed42b994c5736f8ca55ab1458e14f4615f534c2a3d2895b6f03efa3dbd1572e4_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_af1ec16a7bfc4ad3c7daf13b58b1782337654e9b13ef6d9820b4b661ba4a5299 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_af1ec16a7bfc4ad3c7daf13b58b1782337654e9b13ef6d9820b4b661ba4a5299->enter($__internal_af1ec16a7bfc4ad3c7daf13b58b1782337654e9b13ef6d9820b4b661ba4a5299_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_0c3822ab7733723709f16cda071c728ed2e44c3b7cad18dae2417cf84ab561a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0c3822ab7733723709f16cda071c728ed2e44c3b7cad18dae2417cf84ab561a1->enter($__internal_0c3822ab7733723709f16cda071c728ed2e44c3b7cad18dae2417cf84ab561a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "



    ";
        // line 12
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 12, $this->getSourceContext()); })())) {
            // line 13
            echo "        <div class=\"alert alert-warning\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 13, $this->getSourceContext()); })()), "messageKey", array()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 13, $this->getSourceContext()); })()), "messageData", array()), "security"), "html", null, true);
            echo "</div>
    ";
        }
        // line 15
        echo "
     
        
<div class=\"login-text\" >
<h1 style=\"color: white;\">
    Bestperience
</h1>
<p>Share your best experience!</p>
</div>

<div class=\"login-box\">
  <center>
<p style=\"font-family: 'Roboto',sans-serif;color: #000;margin-top: 40px\">
  Login with BestPerience
</p>
</center>


 <form action=\"";
        // line 33
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"post\">

<div class=\"login-form\">
 <div class=\"form-group\">
  <input type=\"text\" class=\"form-control login-textbox \" id=\"usr\" placeholder=\"Username\" name=\"_username\" value=\"";
        // line 37
        echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new Twig_Error_Runtime('Variable "last_username" does not exist.', 37, $this->getSourceContext()); })()), "html", null, true);
        echo "\" >
</div>
<div class=\"form-group\">
  <input type=\"password\" class=\"form-control login-textbox\" id=\"pwd\" placeholder=\"password\" name=\"_password\">
  <button class=\"login-button\" onclick=\"location.href='index.php';\"><span class=\"lnr lnr-enter icon\" ></span>&nbsp;Login</button>
<input type=\"hidden\" name=\"_target_path\" value=\"";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("checkRole");
        echo "\" />
  ";
        // line 44
        echo " </form> 

  <p style=\" color:#000; font-size: 15px; margin-top: 10px; \">Don't have an account? <a href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("registration");
        echo "\">Register</a></p>
</div> 
</div>

";
        
        $__internal_0c3822ab7733723709f16cda071c728ed2e44c3b7cad18dae2417cf84ab561a1->leave($__internal_0c3822ab7733723709f16cda071c728ed2e44c3b7cad18dae2417cf84ab561a1_prof);

        
        $__internal_af1ec16a7bfc4ad3c7daf13b58b1782337654e9b13ef6d9820b4b661ba4a5299->leave($__internal_af1ec16a7bfc4ad3c7daf13b58b1782337654e9b13ef6d9820b4b661ba4a5299_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 46,  142 => 44,  138 => 42,  130 => 37,  123 => 33,  103 => 15,  97 => 13,  95 => 12,  89 => 8,  80 => 7,  60 => 5,  42 => 4,  11 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("
{% extends \"::base.html.twig\" %}

{% block title %}Login{% endblock %}
{% block style %}<link rel=\"stylesheet\" href=\"{{asset('assets/css/login.css')}}\">{% endblock %}

{% block body %}




    {% if error %}
        <div class=\"alert alert-warning\">{{ error.messageKey|trans(error.messageData, 'security') }}</div>
    {% endif %}

     
        
<div class=\"login-text\" >
<h1 style=\"color: white;\">
    Bestperience
</h1>
<p>Share your best experience!</p>
</div>

<div class=\"login-box\">
  <center>
<p style=\"font-family: 'Roboto',sans-serif;color: #000;margin-top: 40px\">
  Login with BestPerience
</p>
</center>


 <form action=\"{{ path('login') }}\" method=\"post\">

<div class=\"login-form\">
 <div class=\"form-group\">
  <input type=\"text\" class=\"form-control login-textbox \" id=\"usr\" placeholder=\"Username\" name=\"_username\" value=\"{{ last_username }}\" >
</div>
<div class=\"form-group\">
  <input type=\"password\" class=\"form-control login-textbox\" id=\"pwd\" placeholder=\"password\" name=\"_password\">
  <button class=\"login-button\" onclick=\"location.href='index.php';\"><span class=\"lnr lnr-enter icon\" ></span>&nbsp;Login</button>
<input type=\"hidden\" name=\"_target_path\" value=\"{{path('checkRole')}}\" />
  {# <button class=\"btn btn-default\"type=\"submit\">Login</button> #}
 </form> 

  <p style=\" color:#000; font-size: 15px; margin-top: 10px; \">Don't have an account? <a href=\"{{path('registration')}}\">Register</a></p>
</div> 
</div>

{% endblock %}
", "AppBundle:Security:login.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/Security/login.html.twig");
    }
}
