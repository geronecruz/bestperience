<?php

/* AppBundle:User:changePassword.html.twig */
class __TwigTemplate_d7740816f29559022686acf4dc42eef5472ca4bb89708c8081eaa6f8a7ce5066 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:User:changePassword.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'style' => array($this, 'block_style'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aa66aa134783d9e93461391a5efb5d205f113f97481183d24d497438a5567f1b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa66aa134783d9e93461391a5efb5d205f113f97481183d24d497438a5567f1b->enter($__internal_aa66aa134783d9e93461391a5efb5d205f113f97481183d24d497438a5567f1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:changePassword.html.twig"));

        $__internal_52a17174f11a7590d5f90ee505c43f1ff636643656dce245ebc5754626046331 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_52a17174f11a7590d5f90ee505c43f1ff636643656dce245ebc5754626046331->enter($__internal_52a17174f11a7590d5f90ee505c43f1ff636643656dce245ebc5754626046331_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:changePassword.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_aa66aa134783d9e93461391a5efb5d205f113f97481183d24d497438a5567f1b->leave($__internal_aa66aa134783d9e93461391a5efb5d205f113f97481183d24d497438a5567f1b_prof);

        
        $__internal_52a17174f11a7590d5f90ee505c43f1ff636643656dce245ebc5754626046331->leave($__internal_52a17174f11a7590d5f90ee505c43f1ff636643656dce245ebc5754626046331_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_a273bc46527c40ec86e919718c6fd4a2a04ee618e792592abc5f308bf5929503 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a273bc46527c40ec86e919718c6fd4a2a04ee618e792592abc5f308bf5929503->enter($__internal_a273bc46527c40ec86e919718c6fd4a2a04ee618e792592abc5f308bf5929503_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_e91cc9d97091ed1410399339c513d935d25f999916579e3706553b6e6c539b6a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e91cc9d97091ed1410399339c513d935d25f999916579e3706553b6e6c539b6a->enter($__internal_e91cc9d97091ed1410399339c513d935d25f999916579e3706553b6e6c539b6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Register";
        
        $__internal_e91cc9d97091ed1410399339c513d935d25f999916579e3706553b6e6c539b6a->leave($__internal_e91cc9d97091ed1410399339c513d935d25f999916579e3706553b6e6c539b6a_prof);

        
        $__internal_a273bc46527c40ec86e919718c6fd4a2a04ee618e792592abc5f308bf5929503->leave($__internal_a273bc46527c40ec86e919718c6fd4a2a04ee618e792592abc5f308bf5929503_prof);

    }

    // line 6
    public function block_style($context, array $blocks = array())
    {
        $__internal_de71b335c0d60f872e813b8af4a04159e8d8257e7157cb6ceba5698e9d6620f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_de71b335c0d60f872e813b8af4a04159e8d8257e7157cb6ceba5698e9d6620f7->enter($__internal_de71b335c0d60f872e813b8af4a04159e8d8257e7157cb6ceba5698e9d6620f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        $__internal_a04cc44f1bfeef620cd0aefb145315436ef618d6b02c797b774389af5d9057f2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a04cc44f1bfeef620cd0aefb145315436ef618d6b02c797b774389af5d9057f2->enter($__internal_a04cc44f1bfeef620cd0aefb145315436ef618d6b02c797b774389af5d9057f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/signup.css"), "html", null, true);
        echo "\">";
        
        $__internal_a04cc44f1bfeef620cd0aefb145315436ef618d6b02c797b774389af5d9057f2->leave($__internal_a04cc44f1bfeef620cd0aefb145315436ef618d6b02c797b774389af5d9057f2_prof);

        
        $__internal_de71b335c0d60f872e813b8af4a04159e8d8257e7157cb6ceba5698e9d6620f7->leave($__internal_de71b335c0d60f872e813b8af4a04159e8d8257e7157cb6ceba5698e9d6620f7_prof);

    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        $__internal_f8b7f0be57b35be0e9f1653d79798aa1cfd6f5e699513bd532d221ab8dd2a4f3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f8b7f0be57b35be0e9f1653d79798aa1cfd6f5e699513bd532d221ab8dd2a4f3->enter($__internal_f8b7f0be57b35be0e9f1653d79798aa1cfd6f5e699513bd532d221ab8dd2a4f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f5300fe1625d5c5f71dad043ac27ecdf1cdb14bf2902fb23e7449098c7b9fa06 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5300fe1625d5c5f71dad043ac27ecdf1cdb14bf2902fb23e7449098c7b9fa06->enter($__internal_f5300fe1625d5c5f71dad043ac27ecdf1cdb14bf2902fb23e7449098c7b9fa06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 9
        echo "

<div class=\"signup-text\" >
<h1>
    Bestperience
</h1>
<p>Share your best experience!</p>
</div>

<div class=\"signup-box\" style=\"background-color: \">
  <center>
<p style=\"font-family: 'Roboto',sans-serif;color: white;margin-top: 40px\">
 You man now Change your password
</p>
</center>
";
        // line 24
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 24, $this->getSourceContext()); })()), 'form_start');
        echo "
<div class=\"signup-form\">
 <div class=\"form-group\">
 ";
        // line 27
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 27, $this->getSourceContext()); })()), "username", array()), 'errors');
        echo "
    ";
        // line 28
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 28, $this->getSourceContext()); })()), "username", array()), 'row');
        echo "
</div>
<div class=\"form-group\">
  
    ";
        // line 32
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 32, $this->getSourceContext()); })()), "email", array()), 'errors');
        echo "
    ";
        // line 33
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 33, $this->getSourceContext()); })()), "email", array()), 'row');
        echo "
</div>
<div class=\"form-group\">
  ";
        // line 36
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 36, $this->getSourceContext()); })()), "plainPassword", array()), "first", array()), 'errors');
        echo "
    ";
        // line 37
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 37, $this->getSourceContext()); })()), "plainPassword", array()), "first", array()), 'row');
        echo "
     ";
        // line 38
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 38, $this->getSourceContext()); })()), "plainPassword", array()), "second", array()), 'errors');
        echo "
    ";
        // line 39
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 39, $this->getSourceContext()); })()), "plainPassword", array()), "second", array()), 'row');
        echo "
     <button class=\"btn btn-primary btn-block\" type=\"submit\">Change Account Details</button>


</div> 
</div>






   
    
   
    

";
        // line 56
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 56, $this->getSourceContext()); })()), 'form_end');
        echo "
    
    

";
        
        $__internal_f5300fe1625d5c5f71dad043ac27ecdf1cdb14bf2902fb23e7449098c7b9fa06->leave($__internal_f5300fe1625d5c5f71dad043ac27ecdf1cdb14bf2902fb23e7449098c7b9fa06_prof);

        
        $__internal_f8b7f0be57b35be0e9f1653d79798aa1cfd6f5e699513bd532d221ab8dd2a4f3->leave($__internal_f8b7f0be57b35be0e9f1653d79798aa1cfd6f5e699513bd532d221ab8dd2a4f3_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:User:changePassword.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  165 => 56,  145 => 39,  141 => 38,  137 => 37,  133 => 36,  127 => 33,  123 => 32,  116 => 28,  112 => 27,  106 => 24,  89 => 9,  80 => 8,  60 => 6,  42 => 5,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}



{% block title %}Register{% endblock %}
{% block style %}<link rel=\"stylesheet\" href=\"{{asset('assets/css/signup.css')}}\">{% endblock %}

{% block body %}


<div class=\"signup-text\" >
<h1>
    Bestperience
</h1>
<p>Share your best experience!</p>
</div>

<div class=\"signup-box\" style=\"background-color: \">
  <center>
<p style=\"font-family: 'Roboto',sans-serif;color: white;margin-top: 40px\">
 You man now Change your password
</p>
</center>
{{ form_start(form) }}
<div class=\"signup-form\">
 <div class=\"form-group\">
 {{form_errors(form.username )}}
    {{ form_row(form.username) }}
</div>
<div class=\"form-group\">
  
    {{form_errors(form.email)}}
    {{ form_row(form.email) }}
</div>
<div class=\"form-group\">
  {{form_errors(form.plainPassword.first)}}
    {{ form_row(form.plainPassword.first) }}
     {{form_errors(form.plainPassword.second)}}
    {{ form_row(form.plainPassword.second) }}
     <button class=\"btn btn-primary btn-block\" type=\"submit\">Change Account Details</button>


</div> 
</div>






   
    
   
    

{{ form_end(form) }}
    
    

{% endblock %}
", "AppBundle:User:changePassword.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/User/changePassword.html.twig");
    }
}
