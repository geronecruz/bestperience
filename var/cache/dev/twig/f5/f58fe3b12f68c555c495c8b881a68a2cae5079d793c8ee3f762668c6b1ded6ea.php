<?php

/* AppBundle:Post:listPost.html.twig */
class __TwigTemplate_8132e2e6dd05c6a05c446ecbc6542f1112fb3b3d2c05571cc33c000d0dcfbc04 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::loggedin.html.twig", "AppBundle:Post:listPost.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::loggedin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2e12e017300a4faeb15ff168f0b7311b4957cc8446a3b9142171f96179f3176d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e12e017300a4faeb15ff168f0b7311b4957cc8446a3b9142171f96179f3176d->enter($__internal_2e12e017300a4faeb15ff168f0b7311b4957cc8446a3b9142171f96179f3176d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:listPost.html.twig"));

        $__internal_18a04b3a0144b46eb3e939b208e0deb3e60f96b21d7a5fa5ea75072adc4d12c6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_18a04b3a0144b46eb3e939b208e0deb3e60f96b21d7a5fa5ea75072adc4d12c6->enter($__internal_18a04b3a0144b46eb3e939b208e0deb3e60f96b21d7a5fa5ea75072adc4d12c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Post:listPost.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2e12e017300a4faeb15ff168f0b7311b4957cc8446a3b9142171f96179f3176d->leave($__internal_2e12e017300a4faeb15ff168f0b7311b4957cc8446a3b9142171f96179f3176d_prof);

        
        $__internal_18a04b3a0144b46eb3e939b208e0deb3e60f96b21d7a5fa5ea75072adc4d12c6->leave($__internal_18a04b3a0144b46eb3e939b208e0deb3e60f96b21d7a5fa5ea75072adc4d12c6_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_1cb9dd2012895e74b4df8496a6a56f17613fe9e10d8616ea2f06810dd70cf843 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1cb9dd2012895e74b4df8496a6a56f17613fe9e10d8616ea2f06810dd70cf843->enter($__internal_1cb9dd2012895e74b4df8496a6a56f17613fe9e10d8616ea2f06810dd70cf843_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_54fc1b0fc5b6b3d4a70513aeb62e671a054f0224309c154b7352678990be9610 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_54fc1b0fc5b6b3d4a70513aeb62e671a054f0224309c154b7352678990be9610->enter($__internal_54fc1b0fc5b6b3d4a70513aeb62e671a054f0224309c154b7352678990be9610_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Home";
        
        $__internal_54fc1b0fc5b6b3d4a70513aeb62e671a054f0224309c154b7352678990be9610->leave($__internal_54fc1b0fc5b6b3d4a70513aeb62e671a054f0224309c154b7352678990be9610_prof);

        
        $__internal_1cb9dd2012895e74b4df8496a6a56f17613fe9e10d8616ea2f06810dd70cf843->leave($__internal_1cb9dd2012895e74b4df8496a6a56f17613fe9e10d8616ea2f06810dd70cf843_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_d362b2b145f2cd8085c4b6b9e3f75ba0e0ca37593d8a680ab54837c56e983205 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d362b2b145f2cd8085c4b6b9e3f75ba0e0ca37593d8a680ab54837c56e983205->enter($__internal_d362b2b145f2cd8085c4b6b9e3f75ba0e0ca37593d8a680ab54837c56e983205_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c54a5366c1757811915d464e2c45543979a5b6f71a2671986abf8673c6b38148 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c54a5366c1757811915d464e2c45543979a5b6f71a2671986abf8673c6b38148->enter($__internal_c54a5366c1757811915d464e2c45543979a5b6f71a2671986abf8673c6b38148_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
";
        // line 8
        echo "
 <section id=\"home-section\" class=\"line\">   

 <center><h2 style=\"color:white;\">Articles</h2></center>
";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["Posts"]) || array_key_exists("Posts", $context) ? $context["Posts"] : (function () { throw new Twig_Error_Runtime('Variable "Posts" does not exist.', 12, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["Post"]) {
            // line 13
            echo "\t\t
\t\t\t
\t\t\t
\t\t\t\t";
            // line 19
            echo "\t\t\t
\t\t\t
\t\t\t
\t\t\t";
            // line 23
            echo "\t\t
\t\t

         <div class=\"margin\">
            <!-- ARTICLES -->             
            <div class=\"s-12 l-12\">
            
                <!-- ARTICLE 1 -->               
               <article class=\"post-";
            // line 31
            echo twig_escape_filter($this->env, (twig_random($this->env, 4) + 1), "html", null, true);
            echo " line\">
                  <!-- image -->                 
                  <div class=\"s-12 l-6 post-image\"> 
\t\t\t\t  \t<center>                  
                     <a href=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_post_view", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "id", array()))), "html", null, true);
            echo "\">
                     <img src=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("uploads/image/"), "html", null, true);
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "img", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "title", array()), "html", null, true);
            echo "\" style=\"max-height:300px; max-width: 200px;\">
                     </a>
\t\t\t\t\t </center>                
                  </div>
                  <!-- text -->                 
                  <div class=\"s-12 l-5 post-text\">
                     <a href=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_post_view", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "id", array()))), "html", null, true);
            echo "\">
                        <h2>";
            // line 43
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "title", array()), "html", null, true);
            echo " </h2>
                     </a>
                     <p>";
            // line 45
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "Content", array()), "html", null, true);
            echo "</p>
                  </div>
                  <!-- date -->                 
                  <div class=\"s-12 l-1 post-date\">
\t\t\t\t  \t
                     <p class=\"date\"";
            // line 50
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "date", array()), "d"), "html", null, true);
            echo "</p>
                     <p class=\"month\">mar ";
            // line 51
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["Post"], "date", array()), "M"), "html", null, true);
            echo "</p>
                  </div>
               </article>
               
            </div>
           
         </div>


";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['Post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "</section>

";
        
        $__internal_c54a5366c1757811915d464e2c45543979a5b6f71a2671986abf8673c6b38148->leave($__internal_c54a5366c1757811915d464e2c45543979a5b6f71a2671986abf8673c6b38148_prof);

        
        $__internal_d362b2b145f2cd8085c4b6b9e3f75ba0e0ca37593d8a680ab54837c56e983205->leave($__internal_d362b2b145f2cd8085c4b6b9e3f75ba0e0ca37593d8a680ab54837c56e983205_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Post:listPost.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 61,  145 => 51,  141 => 50,  133 => 45,  128 => 43,  124 => 42,  112 => 36,  108 => 35,  101 => 31,  91 => 23,  86 => 19,  81 => 13,  77 => 12,  71 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::loggedin.html.twig\" %}

{% block title %}Home{% endblock %}

{% block body %}

{# <a class=\"btn btn-success btn-xs\" title=\"View\" href=\"{{path('user_post_add')}}\"><i class=\"fa fa-eye\"></i>add</a>  #}

 <section id=\"home-section\" class=\"line\">   

 <center><h2 style=\"color:white;\">Articles</h2></center>
{%for Post in Posts%}
\t\t
\t\t\t
\t\t\t
\t\t\t\t{# <a class=\"\" title=\"View\" href=\"{{path('user_post_view',{id:Post.id})}}\">view</a> 
\t\t\t\t<a class=\"\" title=\"edit\" href=\"{{path('user_post_update',{id:Post.id})}}\"> edit</i></a> 
\t\t\t\t<a class=\"text-danger\" title=\"Remove\" href=\"{{path('user_post_remove',{id:Post.id})}}\"> emove</a> #}
\t\t\t
\t\t\t
\t\t\t
\t\t\t{#<td> <img src=\"{{asset('uploads/image/')}}{{Post.img}}\" style=\"max-height:100px; max-width: 150px;\"/></td>#}
\t\t
\t\t

         <div class=\"margin\">
            <!-- ARTICLES -->             
            <div class=\"s-12 l-12\">
            
                <!-- ARTICLE 1 -->               
               <article class=\"post-{{random(4)+1}} line\">
                  <!-- image -->                 
                  <div class=\"s-12 l-6 post-image\"> 
\t\t\t\t  \t<center>                  
                     <a href=\"{{path('user_post_view',{id:Post.id})}}\">
                     <img src=\"{{asset('uploads/image/')}}{{Post.img}}\" alt=\"{{Post.title}}\" style=\"max-height:300px; max-width: 200px;\">
                     </a>
\t\t\t\t\t </center>                
                  </div>
                  <!-- text -->                 
                  <div class=\"s-12 l-5 post-text\">
                     <a href=\"{{path('user_post_view',{id:Post.id})}}\">
                        <h2>{{Post.title}} </h2>
                     </a>
                     <p>{{Post.Content}}</p>
                  </div>
                  <!-- date -->                 
                  <div class=\"s-12 l-1 post-date\">
\t\t\t\t  \t
                     <p class=\"date\"{{Post.date|date('d')}}</p>
                     <p class=\"month\">mar {{Post.date|date('M')}}</p>
                  </div>
               </article>
               
            </div>
           
         </div>


{%endfor%}
</section>

{% endblock %}", "AppBundle:Post:listPost.html.twig", "/home/ron/bestperience/src/AppBundle/Resources/views/Post/listPost.html.twig");
    }
}
