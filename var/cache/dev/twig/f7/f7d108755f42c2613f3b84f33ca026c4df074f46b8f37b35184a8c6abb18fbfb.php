<?php

/* @Security/Collector/icon.svg */
class __TwigTemplate_88f9a25b3b4a19f57752d1c88715bdd018071e9f21ac0d4bc99b81f2bba7e8d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_56b983b0f4ceb20ff4dd102c10f03f1139b458a070d096a532de114644c8ff84 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_56b983b0f4ceb20ff4dd102c10f03f1139b458a070d096a532de114644c8ff84->enter($__internal_56b983b0f4ceb20ff4dd102c10f03f1139b458a070d096a532de114644c8ff84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        $__internal_730fefda4891ac814f7aed4f256b95be0ef8da95ddca8f3a3135b5da91e9c958 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_730fefda4891ac814f7aed4f256b95be0ef8da95ddca8f3a3135b5da91e9c958->enter($__internal_730fefda4891ac814f7aed4f256b95be0ef8da95ddca8f3a3135b5da91e9c958_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $__internal_56b983b0f4ceb20ff4dd102c10f03f1139b458a070d096a532de114644c8ff84->leave($__internal_56b983b0f4ceb20ff4dd102c10f03f1139b458a070d096a532de114644c8ff84_prof);

        
        $__internal_730fefda4891ac814f7aed4f256b95be0ef8da95ddca8f3a3135b5da91e9c958->leave($__internal_730fefda4891ac814f7aed4f256b95be0ef8da95ddca8f3a3135b5da91e9c958_prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
", "@Security/Collector/icon.svg", "/home/ron/bestperience/vendor/symfony/symfony/src/Symfony/Bundle/SecurityBundle/Resources/views/Collector/icon.svg");
    }
}
