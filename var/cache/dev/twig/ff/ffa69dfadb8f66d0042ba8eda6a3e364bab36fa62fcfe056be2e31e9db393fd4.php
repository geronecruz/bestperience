<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_5bb381d2016f4fb31ed76213b789caa25a3f81b0f29e559643b0dd7ce9f134c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b1b49177b236cd3a282a0daf249c9f618b6b9a71168b105165916150b4035093 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b1b49177b236cd3a282a0daf249c9f618b6b9a71168b105165916150b4035093->enter($__internal_b1b49177b236cd3a282a0daf249c9f618b6b9a71168b105165916150b4035093_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_e44ead4a28d39afaacf9d114ab4ae425fddb2c09a202fc6a719338cbe41b6ddd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e44ead4a28d39afaacf9d114ab4ae425fddb2c09a202fc6a719338cbe41b6ddd->enter($__internal_e44ead4a28d39afaacf9d114ab4ae425fddb2c09a202fc6a719338cbe41b6ddd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_b1b49177b236cd3a282a0daf249c9f618b6b9a71168b105165916150b4035093->leave($__internal_b1b49177b236cd3a282a0daf249c9f618b6b9a71168b105165916150b4035093_prof);

        
        $__internal_e44ead4a28d39afaacf9d114ab4ae425fddb2c09a202fc6a719338cbe41b6ddd->leave($__internal_e44ead4a28d39afaacf9d114ab4ae425fddb2c09a202fc6a719338cbe41b6ddd_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_5474da1ccf783242108ae8c7dafbb2897a97f79c710c474137dbde0520b01ed1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5474da1ccf783242108ae8c7dafbb2897a97f79c710c474137dbde0520b01ed1->enter($__internal_5474da1ccf783242108ae8c7dafbb2897a97f79c710c474137dbde0520b01ed1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_d80e4903c713c018a32c148c00c1bacec617a2b81df093f1d8381e6cfadea2e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d80e4903c713c018a32c148c00c1bacec617a2b81df093f1d8381e6cfadea2e2->enter($__internal_d80e4903c713c018a32c148c00c1bacec617a2b81df093f1d8381e6cfadea2e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_d80e4903c713c018a32c148c00c1bacec617a2b81df093f1d8381e6cfadea2e2->leave($__internal_d80e4903c713c018a32c148c00c1bacec617a2b81df093f1d8381e6cfadea2e2_prof);

        
        $__internal_5474da1ccf783242108ae8c7dafbb2897a97f79c710c474137dbde0520b01ed1->leave($__internal_5474da1ccf783242108ae8c7dafbb2897a97f79c710c474137dbde0520b01ed1_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_9edec2e7c038c9c05341294a4b5dd50bfb4aa892634972c6286f928fc5858136 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9edec2e7c038c9c05341294a4b5dd50bfb4aa892634972c6286f928fc5858136->enter($__internal_9edec2e7c038c9c05341294a4b5dd50bfb4aa892634972c6286f928fc5858136_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_0a255bbdc9051460d2a6b40d31973474e11b25c73e1dbe5c8a392b12ca6c0d38 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0a255bbdc9051460d2a6b40d31973474e11b25c73e1dbe5c8a392b12ca6c0d38->enter($__internal_0a255bbdc9051460d2a6b40d31973474e11b25c73e1dbe5c8a392b12ca6c0d38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_0a255bbdc9051460d2a6b40d31973474e11b25c73e1dbe5c8a392b12ca6c0d38->leave($__internal_0a255bbdc9051460d2a6b40d31973474e11b25c73e1dbe5c8a392b12ca6c0d38_prof);

        
        $__internal_9edec2e7c038c9c05341294a4b5dd50bfb4aa892634972c6286f928fc5858136->leave($__internal_9edec2e7c038c9c05341294a4b5dd50bfb4aa892634972c6286f928fc5858136_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_87b18443b38b569c9dc091eee6f64bcc5902fcdb53d8b8798faa1fcc62d6c3dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87b18443b38b569c9dc091eee6f64bcc5902fcdb53d8b8798faa1fcc62d6c3dc->enter($__internal_87b18443b38b569c9dc091eee6f64bcc5902fcdb53d8b8798faa1fcc62d6c3dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_8d3dcdddb23dfdc40f0dcf7b5d57a11088c85c72f1e2791abd2b73fca8d784df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d3dcdddb23dfdc40f0dcf7b5d57a11088c85c72f1e2791abd2b73fca8d784df->enter($__internal_8d3dcdddb23dfdc40f0dcf7b5d57a11088c85c72f1e2791abd2b73fca8d784df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_8d3dcdddb23dfdc40f0dcf7b5d57a11088c85c72f1e2791abd2b73fca8d784df->leave($__internal_8d3dcdddb23dfdc40f0dcf7b5d57a11088c85c72f1e2791abd2b73fca8d784df_prof);

        
        $__internal_87b18443b38b569c9dc091eee6f64bcc5902fcdb53d8b8798faa1fcc62d6c3dc->leave($__internal_87b18443b38b569c9dc091eee6f64bcc5902fcdb53d8b8798faa1fcc62d6c3dc_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "/home/ron/bestperience/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
